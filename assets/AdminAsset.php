<?php

namespace app\assets;


use yii\web\AssetBundle;

class AdminAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '//fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic,900,900italic',
        '//fonts.googleapis.com/icon?family=Material+Icons',
        '/css/main.css',
    ];
    public $js = [
        '/js/bundle.js',
        '//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js',
//        '//cdnjs.cloudflare.com/ajax/libs/d3/4.13.0/d3.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment-with-locales.min.js',
        'https://cdn.rawgit.com/davidshimjs/qrcodejs/master/qrcode.min.js',
        '/js/bundle.js',
        '/js/track.js',
//        '/js/chart.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}
