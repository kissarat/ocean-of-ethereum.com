<?php

namespace app\assets;


use yii\web\AssetBundle;

class EmbedAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '/css/embed.css',
    ];
//    public $js = [
//    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
