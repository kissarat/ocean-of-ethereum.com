<?php

namespace app\assets;


use yii\web\AssetBundle;

class MainAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '//fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic,900,900italic',
        '//fonts.googleapis.com/icon?family=Material+Icons',
        '//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/themes/cupertino/jquery-ui.min.css',
        '/css/main.css',
    ];
    public $js = [
        '//cdnjs.cloudflare.com/ajax/libs/vue/2.5.13/vue.min.js',
        '//cdnjs.cloudflare.com/ajax/libs/vue-router/3.0.1/vue-router.min.js',
        '//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js',
//        '//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js',
//        '//cdnjs.cloudflare.com/ajax/libs/d3/4.13.0/d3.min.js',
//        'https://cdn.rawgit.com/davidshimjs/qrcodejs/master/qrcode.min.js',
//        '/js/track.js',
//        '/js/chart.js',
//        '/js/app.js',
//        '/js/main.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
