<?php
/**
 * Last modified: 18.10.02 10:59:53
 * Hash: bdf81fa0a0ee7df72b063d9c91053f4ef7db6564
 */

namespace app\assets;


use yii\web\AssetBundle;

class MinifiedAsset extends AssetBundle
{
    public $sourcePath = '@app/assets';
    public $css = [
    ];
    public $js = [
//        'lib/vue.js',
//        'lib/vue-router.js',
        'lib/bootstrap.js',
        'lib/moment-with-locales.js',
        'lib/qrcode.js',
    ];
    public $depends = [
        'app\assets\MainAsset'
    ];

    public function init()
    {
        if (!YII_DEBUG) {
            foreach (['track.js', 'app.js', 'main.js'] as $filename) {
                $this->js[] = 'js/' . $filename;
            }
        }
        parent::init();
    }
}
