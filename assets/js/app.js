/**
 * Last modified: 18.10.02 11:00:16
 * Hash: 1b6d9607edf5c3f509f11ea005c07da90ceee0b4
 */

$.isObject = function (o) {
    return 'object' === typeof o && o !== null
}

/**
 * @class App
 * @param config
 * @constructor
 */
function App(config) {
    const self = this
    this.time = Date.now();

    this.translation = 'object' === typeof translation && translation ? translation : {}

    const data = {
        languages: {
            en: '🇬🇧',
            ru: '🇷🇺'
        }
    }
    $.extend(data, config)
    this.state = data
    window[this.state.id] = this

    moment.locale(data.locale)

    if (!this.isGuest) {
        Vue.filter('factor', function (value, currency) {
            return value / data.factor[currency || 'USD']
        })
    }

    this._later = [];
    setTimeout(function () {
        self._later.forEach(function (fun) {
            fun.call(self)
        })
    }, 200)
}

App.create = function (state, Extension) {
    Object.assign(App.features, Extension.features)
    Object.assign(App.prototype, Extension.prototype)
    const app = new App(state)
    Extension.call(app)
    return app
}

App.Time = {
    Hour: 3600,
    Day: 24 * 3600,
    Week: 7 * 24 * 3600,
    Month: 30 * 24 * 3600,
    Year: 12 * 30 * 24 * 3600,
}

App.Account = function Account(o, id) {
    this.id = id;
    this.wallet = null;
    this.active = false
    Object.assign(this, o)
}

App.Account.prototype = {
    get label() {
        return this.blockchain ? this.currency : this.name
    },

    createWallet: function () {
        const self = this
        $.getJSON('/wallet/' + this.id, function (r) {
            self.wallet = r.result
        })
    }
}

App.prototype = {
    t: function (text, vars) {
        var out = this.translation[text]
        if (!out) {
            if (this.isDevMode) {
                console.warn('No translation: ' + text)
            }
            out = text
        }
        if (vars && 'object' === typeof vars) {
            for (var name in vars) {
                out = out.replace('{' + name + '}', vars[name])
            }
        }
        return out;
    },

    get id() {
        return this.state.id
    },

    get isGuest() {
        return !this.state.user
    },

    get isDevMode() {
        return 'prod' !== this.mode
    },

    setImmediate: function () {
        const callback = arguments[arguments.length - 1]
        if ('function' === typeof callback) {
            return callback.call(this, [].slice.call(arguments, 0, -1))
        }
        throw new Error('Call to setImmediate without callback')
    },

    debounce: function (callback) {
        const d = function debounced() {
            if (Date.now() - debounced.time > 200) {
                debounced.time = Date.now()
                callback.apply(this, arguments)
            }
        }
        d.time = 0
        return d
    },

    later: function (fun) {
        this._later.push(fun)
    },

    bootstrap: function (options) {
        const self = this
        const results = {}
        const features = App.features
        Object.keys(features).forEach(function (name) {
            const o = options[name]
            if (false === o) {
                return
            }
            const feature = features[name]
            var result
            switch (typeof feature) {
                case 'object':
                    result = self.view(feature)
                    break
                case 'function':
                    if (/^[a-z]/.test(name[0])) {
                        result = feature.apply(self, o || [])
                    }
                    else {
                        result = new feature(self, o)
                        result.appId = self.id
                        Object.defineProperty(result, 'app', {
                            get: function () {
                                return window[this.appId]
                            }
                        })
                        if ('function' === typeof result.init) {
                            self.setImmediate(function () {
                                result.init(self, o)
                            })
                        }
                    }
                    break
            }
            if (result) {
                results[name] = result
            }
        })
        return this.features = results
    },

    normalize: function (money, currency) {
        if ('USD' === currency) {
            return money
        }
        return money / this.state.factor[currency]
    },

    rate: function (money, currency) {
        if (!currency) {
            currency = this.state.currency
        }
        return +(money / this.state.rates[currency]).toFixed(Math.log(this.state.factor[currency]) / Math.log(10))
    },

    get isDashboard() {
        return 0 === location.pathname.indexOf('/dashboard/')
    },

    loadChart: function () {
        this.features.Chart.load()
    },

    unit: function (value, name) {
        return (value / (1024 * 1024)).toFixed(2) + ' ' + name
    },

    get widgets() {
        const self = this
        const widgets = {}
        Object.keys(this.features).forEach(function (name) {
            if (self.features[name]._isVue) {
                widgets[name] = self.features[name]
            }
        })
        return widgets
    },

    get cookies() {
        return this.features.Cookies
    },

    get alerts() {
        return this.features.Alert
    },

    react: function (selector, event, handler) {
        [].forEach.call(document.querySelectorAll(selector), function (element) {
            element.addEventListener(event, handler)
        })
    }
};

App.features = {
    Track: Track,
    // Chart: Chart,

    Alert: {
        el: '.widget-alert',
        data: {
            alerts: []
        },
        methods: {
            add: function (type, message) {
                this.alerts.push({
                    type: type,
                    message: message
                })
                return this.alerts.length - 1
            },

            success: function (message) {
                return this.add('success', message)
            },

            error: function (message) {
                return this.add('error', message)
            },

            remove: function (i) {
                this.alerts.splice(i)
            },
            clear: function () {
                this.alerts.splice(0, this.alerts.length)
            }
        }
    },

    Cookies: (function () {
        function Cookies() {
            this.age = App.Time.Year
        }

        Cookies.prototype = {
            get: function (key) {
                const m = new RegExp(key + '=([^;]+)')
                const v = m.exec(document.cookie)
                if (v) {
                    return v[1]
                }
            },

            set: function (key, value, age) {
                if (!age) {
                    age = this.age
                }
                document.cookie = key + '=' + value + '; path=/; max-age=' + age
            }
        }
        return Cookies
    })(),

    comma: function () {
        this.react('[name*="[amount]"]', 'change', function (e) {
            const input = e.originalEvent.target
            if ('string' === typeof input.value && input.value.indexOf(',') >= 0) {
                input.value = input.value.replace(',', '.')
            }
        })
    },
/*
    avatar: function () {
        const self = this
        this.later(function () {
            this.react('#upload_avatar button', 'click', function () {
                $('#upload_avatar [type=file]').click()
            })
            this.react('[type=file]', 'change', function (e) {
                const input = e.target
                const file = input.files[0]
                if (file) {
                    const isTooLarge = file.size > appConfig.fileSize
                    if (isTooLarge) {
                        input.value = ''
                        self.alerts.error(self.t('File size of "{name}" is larger than {size}', {
                            name: file.name,
                            size: self.unit(appConfig.fileSize, 'MB')
                        }))
                    }
                    else {
                        self.alerts.clear()
                    }
                    $('#upload_avatar .filename').html(isTooLarge ? '' : file.name)
                }
            })
        })
    },
*/
    changeLanguage: function () {
        const self = this
        this.react('#language .dropdown-item', 'click', function (e) {
            self.cookies.set('locale', e.target.querySelector('.name').innerHTML.toLowerCase())
            location.reload()
        })
    },

    reflink: function () {
        // const epoch = 1524000000000;
        $('[name="reflink"]')
        // .each(function (i, input) {
        //     input.value = input.value + '?t=' + Math.floor((Date.now() - epoch) / (3600 * 1000)).toString(36)
        // })
            .click(function () {
                if (window.clipboardData && clipboardData.setData instanceof Function) {
                    clipboardData.setData('URL', this)
                }
                else if (document.execCommand instanceof Function) {
                    this.select()
                    document.execCommand('copy')
                }
                $('.referral-copy-text').text('Copied')
            })

        $('.date').each(function (i, date) {
            date.innerHTML = moment(date.innerHTML).toDate().toLocaleDateString()
        })
    },

    structure: function structureChildren(parentSelector, level) {
        // const self = this
        // if (level > 5) {
        //     return
        // }
        setTimeout(function () {
            $(parentSelector + ' tr.has-children').click(function (e) {
                const $el = $(e.originalEvent.currentTarget)
                const $tr = $el.next()
                if ($tr.hasClass('children')) {
                    $tr.toggleClass('hidden')
                }
                else if ($el.attr('data-id')) {
                    const userId = $el.attr('data-id')
                    const $children = $('<td></td>')
                        .attr('colspan', $el.find('td').length)
                    $('<tr></tr>')
                        .addClass('children')
                        .append($children)
                        .insertAfter($el)
                    $children.load('/children/' + userId + '?level=' + (level + 1), function () {
                        const selector = 'tbody > [data-id="' + userId + '"] + tr.children > td > div > div > table > tbody > '
                        console.log(selector)
                        structureChildren(selector, level + 1)
                        // $children.find('.level[data-nick="' + userId + '"]').html(6 - level)
                    })
                }
            })
        }, 50)
    },

    renderUnits: function () {
        const self = this
        $('.money').each(function (i, el) {
            el.className.split(/\s+/).forEach(function (clazz) {
                const factor = self.state.factor[clazz.toUpperCase()]
                if (factor > 0) {
                    el.innerHTML = +(el.innerHTML / factor).toFixed(Math.log(factor))
                    const regex = 'usd' === clazz ? /^(\d+)(\.\d+)?$/ : /^(\d+\.\d{4})?(\d+)?$/;
                    // el.innerHTML = el.innerHTML.replace(regex, '$1<span class="remain">$2</span>')
                }
            })
        })

        $('.percent').each(function (i, el) {
            const v = el.innerHTML * 100
            el.innerHTML = v >= 100 ? Math.round(v) : +v.toFixed(1)
        })
    },

    transferMoment: function () {
        $('[data-time]').each(function (i, td) {
            td.innerHTML = moment(1000 * td.getAttribute('data-time')).format('D MMM Y, HH:mm')
        })
    }
}

App.utils = {
    serialize: function (params) {
        const strings = []
        for (var key in params) {
            strings.push(key + '=' + params[key])
        }
        return strings.join('&')
    },

    jsonToPHParray: function jsonToPHParray(o) {
        switch (typeof o) {
            case 'object':
                if (o) {
                    const strings = []
                    for (var key in o) {
                        strings.push(jsonToPHParray(key) + ' => ' + jsonToPHParray(o[key]))
                    }
                    return '[\n' + strings.join(',\n') + '\n]'
                }
                return 'null'
            case 'string':
                const s = o.replace(/'/g, "\\'")
                return "'" + s + "'"
            default:
                return o
        }
    }
}
