/**
 * Last modified: 18.08.21 21:40:20
 * Hash: b2dfc6bd8ff6ebcd190d0e2fb02df67d31b0d310
 */

function Chart(app) {
    this.app = app
}

Chart.prototype = {
    selector: '#chart > svg',
    top: 20,
    right: 20,
    bottom: 20,
    left: 40,
    splineAlpha: 0.5,
    scaleY: 1.2,
    timeEpsilon: 0.5,

    get isValid() {
        return this.series
            && this.series.length >= 0
            && this.top > 0
            && this.right > 0
            && this.bottom > 0
            && this.left > 0
    },

    get isVuex() {
        return this.app.getters
    },

    get state() {
        return this.isVuex ? this.app.getters : this.app.state
    },

    get currency() {
        return this.state.currency
    },

    get factor() {
        return 'number' === typeof this.state.factor ? this.state.factor : this.state.factor[this.currency]
    },

    debounce: function (callback) {
        const d = function debounced() {
            if (Date.now() - debounced.time > 200) {
                debounced.time = Date.now()
                callback.apply(this, arguments)
            }
        }
        d.time = 0
        return d
    },

    serialize: function (params) {
        const strings = []
        for (var key in params) {
            strings.push(key + '=' + params[key])
        }
        return strings.join('&')
    },

    render: function (selector) {
        const self = this
        const el = document.querySelector(selector)
        if (!(el && this.isValid)) {
            throw new Error('Invalid chart configuration ' + selector)
        }
        el.innerHTML = ''
        const box = el.getBoundingClientRect()
        const svg = d3.select(selector)
        const width = +box.width - this.left - this.right
        const height = +box.height - this.top - this.bottom
        var g = svg.select('g.area')
        const isEmpty = g.empty()
        if (isEmpty) {
            g = svg.append('g')
        }
        g.attr('transform', 'translate(' + this.left + ', ' + this.top + ')')
            .attr('class', 'area')
        const axisX = d3.scaleLinear().range([0, width])
        const axisY = d3.scaleLinear().range([height, 0])

        const area = d3.line()
            .x(function (d) {
                return axisX(d.day)
            })
            .y(function (d) {
                return axisY(d.amount)
            })
            .curve(d3.curveCatmullRom.alpha(this.splineAlpha))

        const heightY = d3.max(this.series, function (t) {
            return t.amount * self.scaleY
        }) || 20000 / this.factor

        axisX.domain(d3.extent(this.series, function (t) {
            return t.day
        }))
        axisY.domain([0, heightY])
        // area.y0(axisY(0))

        g.append("g")
            .attr("transform", 'translate(0,' + height + ')')
            .call(d3.axisBottom(axisX));
        g.append("g")
            .call(d3.axisLeft(axisY))

        // add the X gridlines
        g.append("g")
            .attr("class", "grid grid-x")
            .attr("transform", 'translate(0,' + height + ')')
            .call(d3.axisBottom(axisX)
                .ticks(this.days)
                .tickSize(-height)
                .tickFormat('')
            )

        // add the Y gridlines
        g.append('g')
            .attr('class', 'grid grid-y')
            .call(d3.axisLeft(axisY)
                .ticks(10)
                .tickSize(-width)
                .tickFormat('')
            )

        g.append('path')
            .datum(this.series)
            .attr("class", "curve")
            .attr("d", area)

        const node = g.append('g')
            .attr('class', 'nodes')
            .selectAll('g')
            .data(this.series)
            .enter()
            .append('g')

        node.append('path')
            .attr('d', 'M0,-5  l-70,0  a5,5 0 0 1 -5,-5' +
                'l0,-30  a5,5 0 0 1 5,-5 l70,0 a5,5 0 0 1 5,5  l0,30 a5,5 0 0 1 -5,5')
            .attr('transform', function (t) {
                const x = axisX(t.day)
                const y = axisY(t.amount)
                return 'translate(' + x + ',' + y + ')'
            })

        node.append('circle')
            .attr('cx', function (t) {
                return axisX(t.day)
            })
            .attr('cy', function (t) {
                return axisY(t.amount)
            })
        node
            .append('text')
            .attr('class', 'month')
            .attr('x', function (t) {
                return axisX(t.day) - 70
            })
            .attr('y', function (t) {
                return axisY(t.amount) - 5
            })
            .text(function (t) {
                return moment(t.date).format('D MMMM')
            })
            .exit()
        node
            .append('text')
            .attr('class', 'amount')
            .attr('font-size', 11)
            .attr('x', function (t) {
                return axisX(t.day) - 70
            })
            .attr('y', function (t) {
                return axisY(t.amount) + 11
            })
            .text(function (t) {
                return t.amount + ' ' + self.state.currency
            })
            .exit()

        svg.on('mousemove', this.debounce(function () {
            const xx = d3.mouse(document.querySelector(self.selector + ' > g.area'))
            const date = axisX.invert(xx[0])

            g.selectAll('g.nodes > g')
                .attr('class', function (t) {
                    return Math.abs(date - t.day) < self.timeEpsilon
                        ? 'active' : ''
                })
                .data(self.series)
        }))
    },

    load: function () {
        this.fetch({
            currency: this.state.currency,
            month: (this.state.month + 1) || (new Date()).getMonth() + 1
        })
    },

    fetch: function (params) {
        const self = this
        const year = (new Date()).getFullYear()
        const month = params.month - 1
        this.days = new Date(year, params.month, 0).getDate()
        $.getJSON('/transfer/histogram?' + this.serialize(params), function (r) {
            if (!(r.result && r.result.series instanceof Array)) {
                return console.error('Invalid response')
            }
            const available = []
            // const month = new Date(year, params.month - 1)
            const factor = self.factor
            const scale = Math.log(factor) / Math.log(10)
            r.result.series.forEach(function (n) {
                available.push(new Chart.Node(n[0], n[1] / factor))
            })
            available.sort(function (a, b) {
                return a.date - b.date
            })
            const series = []
            for (var i = 1; i <= self.days; i++) {
                const node = available.find(function (n) {
                    return n.day === i
                })
                series.push(node || new Chart.Node(new Date(year, month, i)))
            }
            self.series = series
            self.state.monthExpense = r.result.expense / factor
            self.state.monthIncome = +self.sumSeries().toFixed(scale)
            self.render(self.selector)
        })
    },

    sumSeries: function () {
        return this.series.reduce(function (m, n) {
            return n.amount + m
        }, 0)
    },

    toString: function () {
        return this.series.map(function (n) {
            return n.toString()
        }).join('\n')
    }
}

Chart.Node = function Node(time, amount) {
    if (!amount) {
        amount = 0
    }
    this.date = new Date(time)
    this.amount = +amount
}

Chart.Node.prototype = {
    get day() {
        return this.date.getDate()
    },
    toString: function () {
        // return this.date + '=' + this.amount
        return (this.date.getYear() - 100)
            + '-' + ('00' + (this.date.getMonth() + 1)).slice(-2)
            + '-' + ('00' + (this.date.getDate() + 1)).slice(-2)
            + ' ' + this.amount
    }
}
