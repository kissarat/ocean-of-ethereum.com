/**
 * Last modified: 19.01.10 02:08:03
 * Hash: 5c0ce12008253cc51d1886931fd65a0b57ab7841
 */

function roman(n) {
  var result = [];
  var decimal = [1000, 500, 100, 50, 10, 5, 1];
  var roman = ['M', 'D', 'C', 'L', 'X', 'V', 'I'];
  for (var i = 0; i <= decimal.length; i++) {
    // looping over every element of our arrays
    while (n % decimal[i] < n) {
      // keep trying the same number until we need to move to a smaller one
      result.push(roman[i]);
      // add the matching roman number to our result string
      n -= decimal[i];
      // subtract the decimal value of the roman number from our number
    }
  }
  return result.join('');
}

roman.M = roman.mille = 1000
roman.D = roman.quingenti = 500
roman.C = roman.centum = 100
roman.L = roman.quinquaginta = 50
roman.X = roman.decem = 10
roman.V = roman.quinquie = 5
roman.I = roman.unus = 1

const HOSTS = [
  /^(office|internal)\.info0cean\.com$/,
  /^olympus\.local\./,
  /^localhost/
]

function Olympus() {
}

Olympus.prototype = {
  init: function () {
    $('.autosubmit form').submit()
    this.bootstrap({
      structure: ['.structure.index tbody', 1]
    })

    const routes = [
      {path: '/refill', component: this.features.refill}
    ]

    const router = new VueRouter({
      base: '/',
      mode: 'history',
      routes: routes
    })

    const app = new Vue({
      router: router,
      computed: {
        widgetFound() {
          return this.$route.matched[0] &&
              this.$route.matched[0].components
        }
      }
    })
        .$mount('#app')
  },

  view: function (o) {
    if (o.name) {
      const el = document.querySelector('template.' + o.name)
      if (el) {
        const template = el.innerHTML
        o.template = '<div class="widget ' + o.name.replace('-', ' ') + '">' + template + '</div>'
        el.remove()
        return o
      }
      else {
        console.error('Template ' + o.name + ' not found')
      }
    }
  }
}

Olympus.features = {
  time: function () {
    $('[data-time]').each(function (i, div) {
      div.innerHTML = new Date(1000 * div.getAttribute('data-time')).toLocaleString()
    })
    $('.unix-time').each(function (i, div) {
      div.innerHTML = new Date(1000 * div.textContent).toLocaleString()
    })
  },

  roman: function () {
    $('.roman').each(function (i, div) {
      div.innerHTML = roman(+div.innerHTML)
    })
  },

  avatar: function () {
    const self = this
    this.later(function () {
      $('[type="file"]').change(function (e) {
        const target = e.originalEvent.target
        const file = target.files[0]
        if (file && file.size >= 4 * 1024 * 1024) {
          alert(self.t('Image size too large'))
          // target.click()
        }
      })
    })
  },

  refill: {
    name: 'transfer-refill',
    data: function () {
      return {
        address: ''
      }
    },
    methods: {
      getAddress() {
        const self = this
        $.getJSON('transfer/wallet', function (r) {
          if ('string' === typeof r.result) {
            self.address = r.result
          }
        })
      }
    },
    watch: {
      address(newValue) {
        if (newValue) {
          new QRCode('address-qr-code', newValue);
        }
      }
    }
  },

  ask: function () {
    const self = this
    this.later(function () {
      $('.node.matrix a[data-number].empty').click(function (e) {
        e.preventDefault();
        $('#matrix-number-choice')
            .html(self.t('Do you confirm the choice of this place?'))
            .dialog({
              resizable: false,
              height: "auto",
              width: 400,
              modal: true,
              buttons: {
                [self.t('Yes')]: function () {
                  location.href = e.target.href
                  $(this).dialog("close");
                },
                [self.t('Cancel')]: function () {
                  $(this).dialog("close");
                }
              }
            })
      })
    })
  },

  signup: function () {
    const self = this
    function check(name, message) {
      $('.user.signup form #user-' + name).change(function (e) {
        $.getJSON('/exists/' + name + '=' + e.target.value, function (data) {
          if (data.result) {
            $('.user.signup form').yiiActiveForm('updateAttribute', 'user-' + name, [message])
          }
        })
      })
    }

    this.later(function () {
      check('nick', self.t('This login has already been taken'))
      check('email', self.t('This e-mail has been already taken'))
      // if (+localStorage.getItem('olympus.debug') !== 1) {
      //   document.querySelector('.user.signup form').innerHTML = 'Регистрация еще не открыта'
      // }
    })
  }
}

const app = App.create(appConfig, Olympus)

document.addEventListener('DOMContentLoaded', function () {
  app.init();

  $('.lang > span').click(function (e) {
    // year
    const age = 12 * 30 * 24 * 3600
    document.cookie = 'locale=' + e.target.id + '; path=/; max-age=' + age
    location.reload()
  })
})

document.addEventListener('DOMContentLoaded', function () {
  var allowHost = false
  HOSTS.forEach(function (hostname) {
    if (hostname.test(location.hostname)) {
      allowHost = true
      return true
    }
  })
  if (allowHost) {
    document.body.style.removeProperty('display')
  }
  else {
    document.body.style.removeProperty('display')
    document.body.innerHTML = atob('Q2Fubm90IGNvbm5lY3QgdG8gc2VydmVy')
    return
  }
  [].forEach.call(document.querySelectorAll('[data-store]'), function (input) {
    if (!input.value) {
      const name = input.getAttribute('[data-store]') || input.getAttribute('name')
      const value = localStorage.getItem(name)
      if (value) {
        input.value = value
      }
    }
  })
})

addEventListener('unload', function () {
  [].forEach.call(document.querySelectorAll('[data-store]'), function (input) {
    const name = input.getAttribute('[data-store]') || input.getAttribute('name')
    localStorage.setItem(name, input.value)
  })
})

// if ('object' === typeof WebAssembly && 'function' === typeof WebAssembly.instantiateStreaming) {
//   const go = new Go();
//   WebAssembly.instantiateStreaming(fetch('/connector.wasm'), go.importObject)
//       .then(function (r) {
//         go.run(r.instance)
//       });
// }
