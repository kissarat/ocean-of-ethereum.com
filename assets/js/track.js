/**
 * Last modified: 18.08.21 21:40:20
 * Hash: 4c1e22740f87791a84b1077d1bc9efb58fec52a7
 */

function Track(app) {
    const self = this
    this.started = Date.now()
    this.history = []
    if (app && app.state) {
        this.state = app.state
    }
    this.anchor()
    this.keyboard()
    if ('function' === typeof navigator.sendBeacon) {
        addEventListener('beforeunload', function () {
            self.report()
        })
    }
    const callback = app.id + '_setIP'
    this.loadIP(callback)
    window[callback] = function (r) {
        self.ip = r.ip
        $('[name*="[ip]"]').val(r.ip)
        const logout = document.querySelector('[href*="/logout"]')
        if (logout) {
            logout.href = logout.href += '/' + r.ip
        }
    }
}

Track.prototype = {
    remember: function () {
        const args = [].slice.call(arguments)
        args.unshift(Date.now())
        this.history.push(args)
    },

    forEvents: function ($q, events, handler) {
        events.forEach(function (event) {
            $q.on(event, function (e) {
                handler(event, e)
            })
        })
    },

    anchor: function () {
        const self = this
        this.forEvents($('a[href]'), ['mouseenter', 'mouseleave'], function (event, e) {
            self.remember(
                event,
                e.delegateTarget.getAttribute('href')
            )
        })
    },

    keyboard: function () {
        const self = this
        this.forEvents($(window), ['keyup'], function (event, e) {
            const target = e.originalEvent.target;
            var a = null
            self.remember(
                event,
                ((a = target.getAttribute('name')) && ('[name=' + a + ']'))
                || ((a = target.id) && ('#' + a))
                || ((a = target.getAttribute('class')) && ('.' + a.replace(/\s+/g, '.')))
                || target.tagName,
                e.key
            )
        })
    },

    pretty: function (o) {
        if (this.isDevMode) {
            return JSON.stringify(o, null, '\t')
        }
        return JSON.stringify(o)
    },

    report: function () {
        const url = location.pathname
            + (location.search ? '?' + location.search : '')
            + (location.hash ? '#' + location.hash : '')
        const spend = Date.now() - this.started
        const data = {
            start: this.started,
            spend: spend
        }
        if (this.ip) {
            data.xip = this.ip;
        }
        if (this.state) {
            if (this.state.user && this.state.user.nick) {
                data.nick = this.state.user.nick
            }
            if (this.state.locale) {
                data.locale = this.state.locale
            }
            if (this.state.currency) {
                data.locale = this.state.currency
            }
        }
        if ('string' === typeof document.referrer) {
            data.referrer = document.referrer.replace(location.origin, '')
        }
        if (this.history.length > 0) {
            data.history = this.history
        }
        const hostname = location.hostname.replace(/^[^.]+\./, '');
        navigator.sendBeacon(
            'https://' + hostname + '/report' + url,
            this.pretty(data)
        )
    },

    loadIP: function (callback) {
        const s = document.createElement('script')
        s.setAttribute('async', 'async')
        s.src = '//api.ipify.org?format=jsonp&callback=' + callback
        document.body.appendChild(s)
    }
}
