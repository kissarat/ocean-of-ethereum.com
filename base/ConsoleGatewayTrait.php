<?php
/**
 * Last modified: 18.10.06 06:30:22
 * Hash: af2915559c4e1657898cabb50f3f1ede8c3dbd96
 */

namespace app\base;


trait ConsoleGatewayTrait
{
    public function actionUnlock()
    {
        $this->getComponent()->lock(false);
    }

    /**
     * @param $nick
     * @throws \Exception
     */
    public function actionPush($nick): void
    {
        $user = $this->getUser($nick);
        $com = $this->getComponent();
        $com->getPaymentQueue()->push($user->getAttribute($com->wallet));
    }

    public function actionPayments(): void
    {
        foreach ($this->getComponent()->getPaymentQueue()->iterate() as $address) {
            echo $address . "\n";
        }
    }

    public function actionReceive($limit = -1)
    {
        $com = $this->getComponent();
        if ($com->isCurrencyEnabled()) {
            $com->lock(true);
            $com->receive($limit);
            $com->lock(false);
        }
    }

    public function actionWallet(string $nick)
    {
        $user = $this->getUser($nick);
        $wallet = $this->getComponent()->getUserAddress($user->id);
        echo $wallet . "\n";
    }
}
