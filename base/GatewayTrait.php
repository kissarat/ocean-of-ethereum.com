<?php
/**
 * Last modified: 18.10.06 06:26:31
 * Hash: 80c8078b2df06419e04472c1dfe6d89e92ebd3f3
 */

namespace app\base;


use app\models\Transfer;
use Yii;

trait GatewayTrait
{
    use LockTrait;
    public $lockTimeout = 20 * 60;
    public $wallet;
    protected $_storage;
    public $_paymentQueue;

    public function getCurrency(): string
    {
        return strtoupper($this->wallet);
    }

    public function getFactor()
    {
        return Transfer::FACTOR[$this->getCurrency()];
    }

    public function getStorage(): Storage
    {
        if (!$this->_storage) {
            $this->_storage = Yii::$app->storage->derive($this->wallet . '-');
        }
        return $this->_storage;
    }

    public function getPaymentQueue(): Storage
    {
        if (!$this->_paymentQueue) {
            $this->_paymentQueue = $this->getStorage()->derive('payment');
        }
        return $this->_paymentQueue;
    }

    public function getLast(): int
    {
        return (int)$this->getStorage()->get('last', $this->getDefaultLast());
    }

    public function setLast($value): void
    {
        $this->getStorage()->set((int)$value, 'last');
    }

    public function isCurrencyEnabled($checkEnvironment = true): bool
    {
        if ($checkEnvironment && 'on' === getenv('CURRENCY_ENABLE')) {
            return true;
        }
        return Transfer::isCurrencyEnabled($this->getCurrency());
    }

    public function getSummary()
    {
        return [
            'name' => $this->getNetworkName(),
            'last' => $this->getStorage()->get('last') ?: null,
//            'updated' => Utils::timestamp($this->getLastUpdateTime()),
            'payments' => $this->getPaymentQueue()->iterate()
        ];
    }
}
