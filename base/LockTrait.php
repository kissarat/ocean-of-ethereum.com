<?php
/**
 * Last modified: 18.10.06 06:21:28
 * Hash: 6948758baf826709f76f4fc6d7e820aaa8c4c239
 */

namespace app\base;


trait LockTrait
{
    public function lock($bool = true)
    {
        $s = $this->getStorage();
        $time = $s->get('lock');
        if ($bool && $time > 0) {
            throw new \Exception(get_class($this) . ' locked');
        }
        $s->set(($bool ? 1 : -1) * time(), 'lock', $this->lockTimeout);
    }
}
