<?php

namespace app\base;


interface Storage
{
    public function push($value, $key = ''): int;

    public function pop($key = '');

    public function shift($key = '');

    public function unshift($value, $key = ''): int;

    public function del($value, $key = ''): int;

    public function get($key = '', $default = null);

    public function set($value, $key = '', int $expires = 0): void;

    public function iterate($key = ''): array;

    public function derive($prefix = ''): Storage;

    public function equals(Storage $storage): bool;
}
