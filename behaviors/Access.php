<?php
/**
 * Last modified: 18.07.03 05:38:12
 * Hash: a3cdb852909e7f520a8c45c0aa2f607c5f0fc529
 */

namespace app\behaviors;


use app\models\Log;
use Yii;
use yii\base\ActionEvent;
use yii\base\Behavior;
use yii\base\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\UnauthorizedHttpException;

class Access extends Behavior
{
    public $admin;
    public $plain;
    public $guest;

    public function events()
    {
        return [
            Controller::EVENT_BEFORE_ACTION => 'beforeAction'
        ];
    }

    /**
     * @param ActionEvent $event
     * @throws UnauthorizedHttpException
     */
    public function beforeAction(ActionEvent $event): void
    {
        $nick = Yii::$app->request->get('nick');
        if ($nick && !Yii::$app->user->getIsGuest() && $nick !== Yii::$app->user->identity->nick) {
            Log::log('user', 'mismatch', ['nick' => $nick]);
        }
        $event->handled = !$this->check($event->action->id);
        if ($event->handled) {
            throw new UnauthorizedHttpException();
        }
    }

    public function check($action): bool
    {
        $user = Yii::$app->user;
        $roles = ['admin', 'plain', 'guest'];
        $role = null;

        foreach ($roles as $role) {
            $actions = $this->$role;
            if (is_array($actions) && in_array($action, $actions, true)) {
                break;
            }
        }

        if ('guest' === $role) {
            return true;
        }
        if (!$user->identity || !$user->identity->nick) {
            return false;
        }
        if ('plain' === $role) {
            return true;
        }
        if ($user->identity->isAdmin()) {
            return true;
        }
        return false;
    }

    /**
     * @throws ForbiddenHttpException
     */
    public static function verifyNick(): void
    {
        $nick = Yii::$app->request->get('nick');
        if ($nick !== Yii::$app->user->identity->nick) {
            Log::log('deny', Yii::$app->controller->id . '/' . Yii::$app->controller->action->id, ['nick' => $nick]);
            throw new ForbiddenHttpException();
        }
    }

    /**
     * @throws ForbiddenHttpException
     */
    public static function verifyUser(): void
    {
        $user = Yii::$app->request->get('user');
        if ($user !== Yii::$app->user->identity->nick) {
            Log::log('deny', Yii::$app->controller->id . '/' . Yii::$app->controller->action->id, ['user' => $user]);
            throw new ForbiddenHttpException();
        }
    }
}
