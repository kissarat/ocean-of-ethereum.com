<?php
/**
 * Last modified: 18.07.03 05:38:12
 * Hash: 9c2a2196ee8485403e51ef4a0395ed34dd4b65e7
 */

namespace app\behaviors;


use Yii;
use yii\base\ActionFilter;

class NoTokenValidation extends ActionFilter
{
    public function beforeAction($action)
    {
        if (\in_array(Yii::$app->controller->action->id, $this->only, true)) {
            Yii::$app->controller->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }
}
