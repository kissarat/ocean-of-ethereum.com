module.exports = async function cat(s = process.stdin) {
  return new Promise(function (resolve, reject) {
    const list = []
    s.on('data', function (chuck) {
      list.push(chuck)
    })
    s.on('end', function () {
      resolve((1 === list.length ? list[0] : Buffer.concat(list)).toString('utf-8'))
    })
    s.on('error', reject)
  })
}
