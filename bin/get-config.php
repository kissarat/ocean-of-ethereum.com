<?php
/**
 * Last modified: 18.11.20 23:42:00
 * Hash: 986511c368dfd98a09dc4089c3f49fade9fb5253
 */

define('ROOT', getenv('ROOT') ?: dirname(__DIR__));

$data = json_encode(require ROOT . '/config/console.php', JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
//if (!CONSOLE) {
//    header('content-type: application/json');
//}
echo $data;
