const cat = require('./cat')

function json2php(o) {
  switch (typeof o) {
    case 'object':
      if (o) {
        const strings = []
        for (const key in o) {
          strings.push(JSON.stringify(key) + ` => ` + json2php(o[key]))
        }
        return '[\n' + strings.join(',\n') + '\n]'
      }
      return 'null'
    default:
      return JSON.stringify(o)
  }
}

async function main() {
  console.log(json2php(JSON.parse(await cat())))
}

main()
