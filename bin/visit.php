<?php
$data = json_decode(file_get_contents('php://input'), true);
$data['url'] = $_GET['url'] ?? $_SERVER['HTTP_REFERER'];
$data['ip'] = $_SERVER['REMOTE_ADDR'];
$time = dechex(floor(microtime(true) * 10000));

file_put_contents("/var/visit/$time.json", json_encode($data, JSON_UNESCAPED_UNICODE));
