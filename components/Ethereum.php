<?php
/**
 * Last modified: 18.10.07 14:05:18
 * Hash: 71f471195dda0be53ebfaf99b56941ba7b954da0
 */

namespace app\components;


use app\helpers\Utils;
use Yii;

class Ethereum extends Parity
{
    public function getUserAddress(int $id = null, string $password = null): string
    {
        $address = parent::getUserAddress($id, $password);
        $this->getPaymentQueue()->push($address);
        return $address;
    }

    public function receive(int $limit = -1): void
    {
        if ('eth' === $this->wallet) {
            $txs = [];
            foreach ($this->getPaymentQueue()->iterate() as $address) {
                $r = Yii::$app->etherscan->get(null, [
                    'module' => 'account',
                    'action' => 'txlist',
                    'address' => $address
                ]);
                if (!empty($r['status']) && 1 === +$r['status'] && \is_array($r['result'])) {
                    foreach ($r['result'] as $t) {
                        $t['number'] = +$t['blockNumber'];
                        $t['created'] = Utils::timestamp(+$t['timeStamp']);
                        $txs[$t['to']] = $t;
                    }
                }
            }
            $this->loadPayments($txs);
        }
        parent::receive($limit);
    }
}
