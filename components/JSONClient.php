<?php
/**
 * Last modified: 18.10.07 13:56:22
 * Hash: f7aa4d468451d2bd99c90b4aeadec3787e16b99a
 */

namespace app\components;


use yii\base\Component;

class JSONClient extends Component
{
    public $origin = '';
    /**
     * @param string $method
     * @param string $uri
     * @param array $params
     * @param array $content
     * @return mixed
     */
    public function request(string $method, string $uri = null, array $params = null, array $content = null) {
        $options = [
            'method' => $method,
            'header' => 'content-type: application/json',
        ];
        $url = $this->origin;
        if ($uri) {
            $url .= $uri;
        }
        if (!empty($params)) {
            $url .= '?' . http_build_query($params);
        }
        if (!empty($content)) {
            $options['content'] = \json_encode($content, JSON_UNESCAPED_UNICODE);
        }
        $m = null;
        \preg_match('/^([^:]+):/', $url, $m);
        if ($m) {
            $protocol = $m[1];
        }
        else {
            throw new \InvalidArgumentException('Invalid protocol');
        }
        $context = [
            $protocol => $options
        ];
        $rtime = time();
        file_put_contents("/tmp/$rtime-response.json", json_encode($context, JSON_PRETTY_PRINT));
        $data = file_get_contents($url, false, stream_context_create($context));
        $time = time();
        file_put_contents("/tmp/$time-$rtime-response.json", $data);
        return \json_decode($data, JSON_OBJECT_AS_ARRAY);
    }

    public function get(string $uri = null, array $params = null) {
        return $this->request('GET', $uri, $params);
    }
}
