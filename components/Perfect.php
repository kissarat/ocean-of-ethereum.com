<?php
/**
 * Last modified: 18.12.10 23:40:22
 * Hash: 0cbb44475cb48cbbe621e29ab81f5c64d8c914a9
 */

namespace app\components;


use app\base\PaymentSystem;
use Yii;
use yii\base\Component;
use yii\web\BadRequestHttpException;

class Perfect extends Component implements PaymentSystem
{
    public const WALLET_REGEX = '/^U\d{7,8}$/';
    public $wallet;
    public $secret;
    public $method;

    public function getSecretHash()
    {
        return strtoupper(md5($this->secret));
    }

    /**
     * @param $data
     * @return bool
     * @throws BadRequestHttpException
     */
    public function verify($data)
    {
        $fields = [
            'PAYMENT_ID' => '/^\w{8}$/',
            'PAYEE_ACCOUNT' => static::WALLET_REGEX,
            'PAYMENT_AMOUNT' => '/^\d+(\.\d{2})?$/',
            'PAYMENT_UNITS' => '/^USD$/',
            'PAYMENT_BATCH_NUM' => '/^\d+$/',
            'PAYER_ACCOUNT' => '/^U\d{7,8}$/',
            'TIMESTAMPGMT' => '/^\d+$/',
            'V2_HASH' => '/^[0-9A-Z]+$/',
        ];
        foreach ($fields as $name => $pattern) {
            if (!empty($data[$name]) && !preg_match($pattern, $data[$name])) {
                throw new BadRequestHttpException(Yii::t('app', 'Invalid parameter {name}', [
                    'name' => $name
                ]));
            }
        }

        if ($this->wallet != $data['PAYEE_ACCOUNT']) {
            throw new BadRequestHttpException(Yii::t('app', 'Invalid payee'));
        }

        $string = implode(':', [
            $data['PAYMENT_ID'],
            $data['PAYEE_ACCOUNT'],
            $data['PAYMENT_AMOUNT'],
            $data['PAYMENT_UNITS'],
            $data['PAYMENT_BATCH_NUM'],
            $data['PAYER_ACCOUNT'],
            $this->getSecretHash(),
            $data['TIMESTAMPGMT']
        ]);

        if ($data['V2_HASH'] != strtoupper(md5($string))) {
            throw new BadRequestHttpException(Yii::t('app', 'Invalid hash'));
        }
        return true;
    }
}
