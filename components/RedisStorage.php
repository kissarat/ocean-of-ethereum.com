<?php

namespace app\components;


use app\base\Storage;
use yii\base\Component;

class RedisStorage extends Component implements Storage
{
    public $redis;
    public $hostname = 'localhost';
    public $port = 6379;
    public $db = 0;
    public $prefix = '';

    public function init()
    {
        parent::init();
        if (!$this->redis) {
            $this->redis = new \Redis();
            $this->redis->open($this->hostname, $this->port);
            $this->redis->select($this->db);
        }
    }

    public function push($value, $key = ''): int
    {
        return $this->redis->rPush($this->prefix . $key, $value);
    }

    public function del($value, $key = ''): int
    {
        return $this->redis->lRem($this->prefix . $key, $value, 1);
    }

    public function get($key = '', $default = null)
    {
        return $this->redis->get($this->prefix . $key) ?: $default;
    }

    public function set($value, $key = '', int $expires = 0): void
    {
        $key = $this->prefix . $key;
        $this->redis->set($key, $value);
        if ($expires > 0) {
            $this->redis->expire($key, $expires);
        }
    }

    public function pop($key = '')
    {
        return $this->redis->rPop($this->prefix . $key);
    }

    public function shift($key = '')
    {
        return $this->redis->lPop($this->prefix . $key);
    }

    public function unshift($value, $key = ''): int
    {
        return $this->redis->lPush($this->prefix . $key, $value);
    }

    public function iterate($key = ''): array
    {
        return $this->redis->lRange($this->prefix . $key, 0, -1);
    }

    public function derive($prefix = ''): Storage
    {
        return new static([
            'redis' => $this->redis,
            'prefix' => $this->prefix . $prefix
        ]);
    }

    public function equals(Storage $storage): bool
    {
        return $storage instanceof static && $storage->prefix;
    }
}
