<?php
/**
 * Last modified: 18.10.06 06:25:42
 * Hash: 1c21ff4647c7b9432f3c4a15a90c841b0fec1b3f
 */

namespace app\components;


use app\base\Storage;
use Yii;
use yii\base\Component;
use yii\base\Exception;

class Settings extends Component
{
    public $defaults = [];
    public $data = [];
    public const LOCALES = 'locales';
    protected $storages = [];

    public function getStorage(): Storage
    {
        return Yii::$app->storage;
    }

    /**
     * @param $name
     * @throws Exception
     */
    public function checkDefault($name): void
    {
        if (!isset($this->defaults[$name])) {
            throw new Exception("Settings $name not found");
        }
    }

    public function getDefault($name)
    {
        return $this->defaults[$name];
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getSummary(): array
    {
        $summary = [];
        if (YII_DEBUG) {
            $summary['n'] = Yii::$app->storage->db;
//            $summary['hostname'] = Yii::$app->storage->hostname;
            $summary['keys'] = Yii::$app->storage->redis->keys('*');
        }
        foreach ($this->defaults as $name => $_) {
            $summary[$name] = $this->getList($name);
        }
        return $summary;
    }

    public function get($key, $default = null) {
        return $this->getStorage()->get($key, $default);
    }

    /**
     * @param $name
     * @return array
     * @throws Exception
     */
    public function getList($name): array
    {
        if (!isset($this->data[$name])) {
            $this->checkDefault($name);
            $s = $this->getStorage();
            $list = $s->iterate($name);
            if (empty($list)) {
                $list = $this->getDefault($name);
                foreach ($list as $item) {
                    $s->push($item, $name);
                }
            }
            $this->data[$name] = $list;
        }
        return $this->data[$name];
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getLocales(): array
    {
        return $this->getList(static::LOCALES);
    }
}
