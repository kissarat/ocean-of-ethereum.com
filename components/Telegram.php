<?php

namespace app\components;


use app\base\Storage;
use Yii;

class Telegram extends JSONClient
{
    public $token;
    public $wait = 90;
    public $chat_id;
    protected $client;
    protected $_storage;

    public function getStorage(): Storage
    {
        if (!$this->_storage) {
            $this->_storage = Yii::$app->storage->derive('telegram-');
        }
        return $this->_storage;
    }

    public function init()
    {
        if (empty($this->token) || empty($this->chat_id)) {
            throw new \InvalidArgumentException('Invalid configuration');
        }
        $this->origin = "https://api.telegram.org/bot$this->token/";
        parent::init();
    }

    public function invoke($method, $params = null)
    {
        return $this->get($method, $params);
    }

    public function getUpdates()
    {
        return $this->invoke('getUpdates', [
            'timeout' => $this->wait,
            'allowed_updates' => 'message'
        ]);
    }

    public function send($chat_id, $text)
    {
        return $this->invoke('sendMessage', [
            'chat_id' => $chat_id,
            'text' => $text
        ]);
    }

    public function sendText($text)
    {
//        if (!YII_TEST) {
            return $this->invoke('sendMessage', [
                'chat_id' => $this->chat_id,
                'text' => $text
            ]);
//        }
    }
}
