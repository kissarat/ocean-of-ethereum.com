<?php
/**
 * Last modified: 18.06.13 16:34:59
 * Hash: 190507b00896e24f509958b9767724adbd720a7b
 */

require_once 'functions.php';

return local('class', [
//    'yii\data\Pagination' => [
//        'defaultPageSize' => 10,
//        'pageSizeParam' => 'limit',
//    ],
//    'yii\data\Sort' => ['defaultOrder' => ['id' => SORT_DESC]],
    'yii\grid\GridView' => [
        'tableOptions' => ['class' => '']
    ]
]);
