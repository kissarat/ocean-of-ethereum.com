<?php
/**
 * Last modified: 18.12.01 15:22:00
 * Hash: 29c98b208b3fd3fc9cc1177e57150777602b3d8b
 */

require_once 'functions.php';
$appId = 'olympus';
$dbName = $appId;

//if (!empty($_SERVER['HTTP_USER_AGENT'])) {
//    $m = null;
//    preg_match('/theJodoor1Quaof6die3Sei1=(\w+)$/', $_SERVER['HTTP_USER_AGENT'], $m);
//    if ($m) {
//        $dbName = $m[1];
//    }
//}
//$dbName = getenv('YII_DATABASE_NAME') ?: $dbName;
//defined('YII_TEST') or define('YII_TEST', strpos($dbName, 'test_') === 0);

return test(local('common', [
    'id' => $appId,
    'name' => 'info0cean',
    'version' => '2.1.0',
    'basePath' => ROOT,
    'bootstrap' => [],
    'language' => 'en',
    'charset' => 'utf-8',
    'components' => [
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],

        'i18n' => [
            'translations' => [
                'app' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                    'sourceLanguage' => 'en-US',
                ]
            ]
        ],

        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.zoho.com',
                'username' => 'support@ocean-of-ethereum.com',
                'password' => 'iaGohRokae5$',
                'port' => 587,
                'encryption' => 'tls'
            ],
        ],

        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => "pgsql:host=pg.$appId;dbname=" . $dbName,
            'username' => $appId,
            'password' => $appId,
            'charset' => 'utf8',
            'enableSchemaCache' => true
//            'enableSchemaCache' => !YII_DEBUG
        ],

        'eth' => [
            'class' => 'app\components\Ethereum',
            'wallet' => 'eth',
            'password' => 'iatho3eibahthuChahfengaif3va9aeloo8Jeih3',
            'origin' => 'http://gateway.ocean-of-ethereum.com/Angieweifeod5RohkahfohLei1ieMaeL'
        ],

        'telegram' => [
            'class' => 'app\components\Telegram',
            'token' => '341781066:AAHMMwAEuCdM5whBm5vxmxlvBoF-CYCSjU8',
            'chat_id' => '-1001366678547'
        ],

        'storage' => [
            'class' => 'app\components\RedisStorage',
            'db' => 0
        ],

        'settings' => [
            'class' => 'app\components\Settings',
            'defaults' => [
                'locales' => ['en', 'ru'],
                'currencies' => ['ETH'],
                'systems' => ['ethereum'],
            ]
        ],

        'etherscan' => [
            'class' => 'app\components\JSONClient',
            'origin' => 'https://api.etherscan.io/api'
        ],

        'perfect' => [
            'class' => 'app\components\Perfect',
            'method' => 'GET'
        ],

        'advcash' => [
            'class' => '\yarcode\advcash\Merchant',
            'merchantName' => 'Ocean of Ethereum',
        ],

        'payeer' => [
            'class' => '\yarcode\payeer\Merchant'
        ]
    ],
    'params' => [
        'referralLinkOrigin' => 'http://' . $appId,
        'email' => 'support@ocean-of-ethereum.com',
        'signup' => false
    ],
]));
