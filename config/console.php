<?php
/**
 * Last modified: 18.06.13 16:34:59
 * Hash: 65289c7069ccc9eeefbbf9f55493a4be7b9dbac2
 */

require_once 'functions.php';

return extend('console', 'common', [
    'controllerNamespace' => 'app\console\controllers',
    'controllerMap' => [
        'migrate' => [
            'class' => 'yii\console\controllers\MigrateController',
            'migrationPath' => '@app/console/migrations',
//            'interactive' => true,
            'migrationNamespaces' => [
//                'yii\log\migrations',
                'app\console\migrations'
            ]
        ],
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache'
        ],
    ]
]);
