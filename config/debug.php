<?php
/**
 * Last modified: 18.06.13 16:34:59
 * Hash: 26c8cb8ee803474aec4282e272d8f96ff5c17202
 */

$config['modules']['debug'] = [
    'class' => 'yii\debug\Module',
    'allowedIPs' => ['127.0.0.1', '193.169.80.152', '185.53.170.57', '::1'],
];
//$config['modules']['gii'] = 'yii\gii\Module';

$config['bootstrap'][] = 'debug';
