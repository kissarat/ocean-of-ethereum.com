<?php
/**
 * Last modified: 18.06.13 16:34:59
 * Hash: a5b02a097988a093271f490962ca4fd7c1c04d49
 */

require_once 'local/define.php';
defined('ROOT') or define('ROOT', realpath(__DIR__ . '/..'));

function local($name, array $defaults) {
    $filename = __DIR__ . "/local/$name.php";
    if (file_exists($filename)) {
        return array_replace_recursive($defaults, require $filename);
    }
    return $defaults;
}

function extend($name, $baseName, array $defaults) {
    return local($name, array_replace_recursive(require __DIR__ . "/$baseName.php", $defaults));
}

function test($config) {
    if (YII_TEST) {
        $config['params']['hostname'] = 'local.ocean-of-ethereum.com';
        $config['params']['database_name'] = 'test_olympus';
    }
    $config['params']['test'] = YII_TEST;
    return $config;
}
