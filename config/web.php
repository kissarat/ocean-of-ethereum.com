<?php
/**
 * Last modified: 18.12.11 00:31:51
 * Hash: cf321543521140248b77b3d831bbd5f134a1b807
 */

require_once 'functions.php';

$webConfig = [
    'bootstrap' => ['log'],
    'defaultRoute' => 'home/default',
    'controllerMap' => [
        'generate' => 'app\controllers\admin\GenerateController',
    ],
    'components' => [
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                '~c' => 'home/coordinate',
                '~v/<uid>/<url>/<spend>' => 'home/visit',
                '~<nick>/profile' => 'settings/profile',
                '~<nick>/password' => 'settings/password',
                '~<nick>/history' => 'transfer/index',
                '~<nick>/referrals' => 'structure/index',
                '~<nick>/offers' => 'offer/index',
                '~<nick>/claims' => 'claim/index',
                '~<nick>/programs' => 'program/index',
                '~<nick>/swaps' => 'swap/index',
                '~<nick>/test' => 'generate/user',
                '~<nick>/activities' => 'activity/index',
                '~<sponsor:[\w_]+>/sign-in' => 'user/login',
                'sign-in' => 'user/login',
                '~<sponsor:[\w_]+>/signup' => 'user/signup',
                'signup' => 'user/signup',
                'agreement' => 'home/agreement',
                'exists/<name>=<value>' => 'user/exists',
                '~<nick>' => 'user/profile',
                '~<nick>/withdraw' => 'invoice/withdraw',
                '~<nick>/pay' => 'invoice/payment',
                '~<nick>/internal' => 'transfer/internal',
                'children/<id>' => 'structure/index',
                'logout/<nick>' => 'user/logout',
                'matrix/<nick>/<id>' => 'node/matrix',
                'matrix/<id>' => 'node/matrix',
                'claim-request/<id>' => 'claim/index',
                'swap-request/<id>' => 'swap/index',
                'request' => 'user/request',
                'history' => 'transfer/index',
                'refill' => 'transfer/index',
                'newbie/<nick>' => 'home/newbie',
                'under-construction' => 'home/construction',
                'reset/<code>' => 'user/reset',
                'statistics' => 'olympus/index',
                'logout/<nick>/<ip>' => 'user/logout',
                'logout/<ip>' => 'user/logout',
                'logout' => 'user/logout',
                'sitemap.xml' => 'home/sitemap',
                'programs' => 'program/index',
                'choose-matrix-<id>' => 'program/sponsor',
                'random-<program_id>/<nick>' => 'node/random',
                'courses/<program_id>' => 'course/index',
                'download/<id>' => 'course/download',
                'currency/<currency>' => 'transfer/currency',
                'transfers' => 'money/index',
                'pay-pm/<amount>' => 'perfect/pay',
                'pay-payeer/<amount>' => 'payeer/pay',
                'pay-advcash/<amount>' => 'advcash/pay',
                'success-pm' => 'perfect/success',
                'fail-pm' => 'perfect/fail',
                'status-pm' => 'perfect/status',
                'success-payeer' => 'payeer/success',
                'fail-payeer' => 'payeer/fail',
                'status-payeer' => 'payeer/status',
                'request-reset' => 'home/request',
                'success-advcash' => 'advcash/success',
                'status-advcash' => 'advcash/status',
                'fail-advcash' => 'advcash/fail'
            ],
        ],

        'request' => [
            'class' => 'app\base\Request',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],

        'cache' => [
            'class' => 'yii\caching\ApcCache',
            'useApcu' => true,
            'defaultDuration' => 3 * 3600
        ],

        'errorHandler' => [
            'errorAction' => 'home/error',
        ],

        'session' => [
            'class' => 'yii\web\CacheSession'
        ],

        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'enableSession' => true,
            'idParam' => 'id',
//            'autoRenewCookie' => false,
            'loginUrl' => ['user/login'],
            'authTimeout' => 10800
        ],

        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'himiklab\yii2\recaptcha\ReCaptcha',
            'siteKey' => '6LeQ-mYUAAAAALc8miFyqk1GPxxI8WnWbnLE0A5x',
            'secret' => '6LeQ-mYUAAAAALTcvnYv7y9gGp0_vgCLm2wC6wP8'
        ],

        'view' => [
            'class' => '\rmrevin\yii\minify\View',
            'enableMinify' => true,
            'concatCss' => true, // concatenate css
            'minifyCss' => true, // minificate css
            'concatJs' => true, // concatenate js
            'minifyJs' => true, // minificate js
            'minifyOutput' => true, // minificate result html page
            'webPath' => '@web', // path alias to web base
            'basePath' => '@webroot', // path alias to web base
            'minifyPath' => '@webroot/assets', // path alias to save minify result
            'forceCharset' => 'UTF-8', // charset forcibly assign, otherwise will use all of the files found charset
            'expandImports' => true, // whether to change @import on content
            'compressOptions' => ['extra' => true], // options for compress
            'excludeFiles' => [
                '\.min\.(js|css)$'
            ],
            'excludeBundles' => [
                'yii\web\JqueryAsset'
            ],
        ]
    ]
];

return extend('web', 'common', $webConfig);
