<?php
/**
 * Last modified: 18.07.03 05:38:12
 * Hash: 3cf2c44b6fee5d6e137324fab7574c16179af596
 */

namespace app\console\controllers;


use Yii;
use yii\console\Controller;

class BlockioController extends Controller
{
    public function actionAddress(): void
    {
        $address = Yii::$app->blockio->getAddress('oliver');
        echo $address;
    }
}
