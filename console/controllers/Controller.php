<?php
/**
 * Last modified: 18.11.20 23:42:00
 * Hash: 96972c9ed96ada7583ef27ce8080f4fa06caced0
 */

namespace app\console\controllers;


use app\helpers\SQL;
use app\models\User;
use Yii;
use yii\db\ActiveRecord;

class Controller extends \yii\console\Controller
{
    /**
     * @var ActiveRecord
     */
    public static $modelClass;

    /**
     * @return false|null|string
     * @throws \yii\db\Exception
     */
    public function actionId()
    {
        return SQL::getSequenceValue(static::tableName());
    }

    public static function tableName(): string
    {
        return static::$modelClass::tableName();
    }

    /**
     * @throws \yii\db\Exception
     */
    public function actionClear()
    {
        if (YII_DEBUG) {
            static::$modelClass::deleteAll();
            SQL::restartSequence(static::tableName());
        } else {
            die("Operation available in debug mode\n");
        }
    }

    public function dump($key, $value)
    {
        echo "$key=$value\n";
    }

    public function echoJSON($o)
    {
        echo json_encode($o, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT) . "\n";
    }

    public function actionSave()
    {
        $array = [];
        foreach (static::$modelClass::find()->all() as $link) {
            $array[] = $link->attributes;
        }
        echo json_encode($array, JSON_UNESCAPED_UNICODE) . "\n";
    }

    /**
     * @param null|string $filename
     */
    public function actionLoad(?string $filename = null)
    {
        $array = json_decode(file_get_contents($filename ? $filename : 'php://stdin'));
        foreach ($array as $data) {
            /** @var ActiveRecord $model */
            $model = new static::$modelClass($data);
            if (!$model->save()) {
                echo json_encode($model->getErrors(), JSON_PRETTY_PRINT) . "\n";
            }
        }
    }

    /**
     * @param $nick
     * @return User
     * @throws \Exception
     */
    protected function getUser($nick): User
    {
        $user = User::findOne(['nick' => $nick]);
        if (!$user) {
            throw new \Exception('User not found: ' . $nick);
        }
        return $user;
    }

    protected function report($message)
    {
        Yii::$app->telegram->sendText($message);
    }

    /**
     * @param $message
     * @param $actual
     * @param $expected
     * @throws \Exception
     */
    protected function validate($message, $actual, $expected)
    {
        if ($actual !== $expected) {
            throw new \Exception("$message is $actual but expected $expected");
        }
    }

    /**
     * @param $prefix
     * @param $actual
     * @param $expected
     * @throws \Exception
     */
    protected function validateByArray($prefix, $actual, $expected)
    {
        foreach ($expected as $key => $value) {
            if ($expected[$key] !== $actual[$key]) {
                $this->validate($prefix . ' :' . $key, $actual[$key], $expected[$key]);
            }
        }
    }
}
