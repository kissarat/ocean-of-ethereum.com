<?php
/**
 * Last modified: 18.10.06 06:30:22
 * Hash: da8f19dd2fa6f9e7ac05e6bc5af4f195e045514b
 */

namespace app\console\controllers;


use app\base\ConsoleGatewayTrait;
use app\components\Parity;
use Yii;

/**
 *
 * @property \app\components\Parity $component
 */
class EthereumController extends Controller
{
    use ConsoleGatewayTrait;

    /**
     * @return Parity
     * @throws \InvalidArgumentException
     * @throws \yii\base\InvalidConfigException
     */
    protected function getComponent(): Parity
    {
        $system = getenv('system');
        if (empty($system)) {
            throw new \InvalidArgumentException('No "system" environment variable');
        }
        return Yii::$app->get($system);
    }

    public function actionList(bool $all = false)
    {
        $c = $this->getComponent();
        $accounts = [];
        foreach ($c->getAccountList() as $account) {
            $balance = hexdec($c->getBalance($account));
            if ($all || $balance > 0) {
                $accounts[$account] = number_format($balance / Parity::WEI, 8, '.', '');
            }
        }
        arsort($accounts);
        echo json_encode($accounts, JSON_PRETTY_PRINT) . "\n";
    }

    public function actionNumber()
    {
        echo $this->getComponent()->getBlockNumber() . "\n";
    }

    public function actionBlock(string $number = 'latest')
    {
        $com = $this->getComponent();
        if (strpos($number, '0x') === 0) {
            $number = dechex($number);
        } elseif (is_numeric($number)) {
            $number = +$number;
        } elseif ('latest' === $number) {
            $number = $com->getBlockNumber();
        }
        echo json_encode($com->getBlock($number), JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
    }

    public function actionSummary()
    {
        $com = $this->getComponent();
        echo json_encode($com->getSummary(), JSON_PRETTY_PRINT);
    }
}
