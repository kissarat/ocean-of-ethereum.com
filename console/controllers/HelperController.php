<?php
/**
 * Last modified: 18.07.03 05:38:11
 * Hash: 56e7efc4d16708ca5ca9ad9087b699151a3a9c7d
 */

namespace app\console\controllers;


use app\helpers\Utils;
use yii\console\Controller;

class HelperController extends Controller
{
    public function actionWidget(): void
    {
        Utils::generateWidget();
    }
}
