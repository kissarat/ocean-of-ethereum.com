<?php
/**
 * Last modified: 18.10.04 17:11:36
 * Hash: 5b7fe371407ea016cac6025ab06433f49b10c8c8
 */

namespace app\console\controllers;


use app\helpers\SQL;
use app\models\Node;
use Yii;

/** @noinspection LongInheritanceChainInspection */

class NodeController extends Controller
{
    /**
     * @throws \yii\db\Exception
     */
    public function actionSeed(): void
    {
        $nodes = [];
        for ($i = 1; $i <= 6; $i++) {
            $nodes[] = [$i, $i, 1];
        }
        Yii::$app->db
            ->createCommand()
            ->batchInsert('node', ['id', 'program', 'user'], $nodes)
            ->execute();
    }

    /**
     * @param int $program_id
     * @param int $user_id
     * @throws \yii\db\Exception
     */
    public function actionPut(int $program_id = 1, int $user_id = 1): void
    {
        $parent_id = SQL::scalar('select id from need where program = :program_id', [':program_id' => $program_id]);
        SQL::execute('insert into node ("user", "parent", "program") values (:user_id, :parent_id, :program_id)', [
            ':user_id' => $user_id,
            ':parent_id' => $parent_id,
            ':program_id' => $program_id,
        ]);
    }

    /**
     * @param int $user_id
     * @param int $amount
     * @param int $program_id
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionAdd(int $user_id = 1, int $amount = 2500, int $program_id = 11): void
    {
        $node = new Node(['user' => $user_id, 'program' => $program_id]);
        if ($node->insert()) {
            SQL::execute('insert into transfer ("from", "amount", "type", "status", "node") values (:user_id, :amount, :type, :status, :node)', [
                ':user_id' => $user_id,
                ':amount' => $amount,
                ':type' => 'buy',
                ':status' => 'success',
                ':node' => $node->id
            ]);
        }
    }

    /**
     * @param string $nick
     * @throws \Exception
     */
    public function actionUser(string $nick)
    {
        $user = $this->getUser($nick);
        foreach (Node::find()->where(['user' => $user->id])->orderBy(['id' => SORT_ASC])->all() as $node) {
            echo $node->id . "\t{$node->program}\n";
        }
    }
}
