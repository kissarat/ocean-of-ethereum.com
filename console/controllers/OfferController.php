<?php

namespace app\console\controllers;


use app\models\Offer;

class OfferController extends Controller
{
    public function actionCreate(string $nick, int $program_id, int $origin)
    {
        $user = $this->getUser($nick);
        $offer = new Offer([
            'origin' => $origin,
            'user' => $user->id,
            'program' => $program_id
        ]);
        $offer->save(false);
    }
}
