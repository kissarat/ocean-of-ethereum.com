<?php
/**
 * Last modified: 18.10.07 15:06:32
 * Hash: ea310788c5249730a11407b6910da73dc56be776
 */


namespace app\console\controllers;


use app\helpers\Utils;
use Yii;

class SecurityController extends Controller
{
    protected function loadJSON($url)
    {
        try {
            return Utils::loadJSON($url);
        } catch (\Exception $ex) {
            $m = null;
            echo $ex->getMessage();
            preg_match('/https?:\/\/([^\/]+)/', $url, $m);
            throw new \Exception('Cannot open: ' . ($m ? $m[1] : ''));
        }
    }

    public function actionCheck($url)
    {
        try {
            $r = $this->loadJSON($url);
            if (!(true === $r['success'] && 'utf-8' === $r['charset'])) {
                throw new \InvalidArgumentException('Unavailable');
            }
            if (!$r['ethereum']) {
                throw new \InvalidArgumentException('Ethereum node is down');
            }
            if ($r['block']['delta'] >= 50) {
                throw new \InvalidArgumentException('Ethereum node out of sync ' . $r['block']['delta']);
            }
            if ($r['block']['remain'] >= 50) {
                throw new \InvalidArgumentException('Ethereum payments out of sync ' . $r['block']['remain']);
            }
        } catch (\Exception $ex) {
            echo $ex->getMessage() . "\n";
            $this->report('https://office.ocean-of-ethereum.com ' . $ex->getMessage());
        }
    }

    public function actionWordpress($url)
    {
        try {
            $r = $this->loadJSON($url);
            if (!(true === $r['success'] && 'UTC' === $r['timezone'])) {
                throw new \InvalidArgumentException('Unavailable');
            }
        } catch (\Exception $ex) {
            echo $ex->getMessage() . "\n";
            $this->report('https://ocean-of-ethereum.com ' . $ex->getMessage());
        }
    }

    public function actionId()
    {
        echo Yii::$app->id . "\n";
    }
}
