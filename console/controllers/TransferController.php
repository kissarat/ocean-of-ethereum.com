<?php
/**
 * Last modified: 18.07.03 05:38:12
 * Hash: b6372fd49893881fc80ea5abbd6ec2cfce06159d
 */

namespace app\console\controllers;


use app\helpers\SQL;
use app\models\User;
use Yii;
use yii\console\Controller;

class TransferController extends Controller
{
    /**
     * @param $quantity
     * @throws \yii\db\Exception
     */
    public function actionInvoice($quantity): void
    {
        $types = array_flip(['payment', 'withdraw']);
        $balances = array_flip(User::find()->select('id')->column());
        $currencies = array_flip(['USD', 'BTC', 'ETH']);
        $invoices = [];
        for ($i = 0; $i < $quantity; $i++) {
            $id = array_rand($balances);
            $type = array_rand($types);
            $amount = rand(1, 10 * 1000 * 100);
            $invoices[] = [
                'withdraw' === $type ? $id : null,
                'payment' === $type ? $id : null,
                $amount,
                $type,
                'success',
                array_rand($currencies)
            ];
        }
        $columns = ['from', 'to', 'amount', 'type', 'status', 'currency'];
        Yii::$app->db
            ->createCommand()
            ->batchInsert('transfer', $columns, $invoices)
            ->execute();
        SQL::queryAll('insert into transfer("to", type, status, amount, currency)  '
            . "select \"user\", 'payment', 'success', -amount * 2, currency from balance where amount < 0");
    }

}
