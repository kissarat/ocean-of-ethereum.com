<?php
/**
 * Last modified: 18.10.06 05:36:44
 * Hash: a9fe79f9cb3d69b7bcc6b997cc392bcbaa1021db
 */

namespace app\console\controllers;


use app\models\Transfer;
use app\models\User;
use Faker\Factory;
use Yii;

class UserController extends Controller {
    /**
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\base\Exception
     */
    public function actionSeed(): void
    {
        $array = json_decode(file_get_contents(__DIR__ . '/../seeds/user.json'), JSON_OBJECT_AS_ARRAY);
        $loaded = array_keys($array);
        $found = User::find()
            ->where(['in', 'nick', $loaded])
            ->select(['nick'])
            ->column();
        $news = array_diff($loaded, $found);
        foreach ($news as $nick) {
            $data = $array[$nick];
            $data['nick'] = $nick;
            $data['auth'] = Yii::$app->security->generateRandomString(64);
            $user = new User($data);
            $user->generateSecret();
            if (!$user->save(false)) {
                echo json_encode([
                    'nick' => $nick,
                    'errors' => $user->getErrors()
                ],
                    JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
            }
        }
        $t = new Transfer([
            'to' => 1,
            'type' => 'payment',
            'status' => 'success',
            'amount' => 1000000
        ]);
        $t->insert();
    }

    /**
     * @param $nick string Root user
     * @param int $depth integer Depth of structure
     * @param int $width integer Maximum width
     * @throws \yii\db\Exception
     * @throws \Exception
     */
    public function actionChildren($nick, $depth = 1, $width = 5): void
    {
        $factory = Factory::create();
        $parent = User::findOne(['nick' => $nick]);
        $size = random_int(0, $width);
        $children = [];
        for ($i = 0; $i < $size; $i++) {
            $userName = $factory->userName;
            $children[] = [
                $userName,
                $factory->email,
                $factory->firstName,
                $factory->lastName,
                $userName,
                $parent->id,
                'native'
            ];
        }
        Yii::$app->db
            ->createCommand()
            ->batchInsert('user', ['nick', 'email', 'forename', 'surname', 'skype', 'parent', 'type'], $children)
            ->execute();
        if ($depth > 1) {
            foreach ($children as $child) {
                $this->actionChildren($child[0], $depth - 1);
            }
        }
    }

    /**
     * @param string $nick
     * @param string $password
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionPassword(string $nick, string $password) {
        $user = $this->getUser($nick);
        $user->password = $password;
        $user->generateSecret();
        if (!$user->update(false)) {
            die('Cannot save user');
        }
    }

    /**
     * @param string $nick
     * @param string $pin
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionPin(string $nick, string $pin) {
        $user = $this->getUser($nick);
        $user->pin = empty($pin) ? null : $pin;
        if (!$user->update(true, ['pin'])) {
            die('Cannot save user');
        }
    }
}
