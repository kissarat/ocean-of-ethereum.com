<?php
/**
 * Last modified: 18.07.03 05:38:12
 * Hash: 9abb1c01c02c97405e1bd0aa489e1e6bce67db22
 */

defined('ROOT') or define('ROOT', __DIR__ . '/..');

$config = require ROOT . '/config/console.php';
define('VENDOR', ROOT . '/vendor');

//require_once VENDOR . '/yiisoft/yii2/BaseYii.php';
require VENDOR . '/yiisoft/yii2/Yii.php';
require VENDOR . '/autoload.php';

$app = new yii\console\Application($config);
$app->setVendorPath(VENDOR);
