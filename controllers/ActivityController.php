<?php

namespace app\controllers;


use app\controllers\base\RestController;
use app\helpers\SQL;
use app\models\Activity;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;

class ActivityController extends RestController
{

    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index'],
                'rules' => [
                    'member' => [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * @return array|string
     * @throws \yii\db\Exception
     */
    public function actionIndex()
    {
        $user_id = Yii::$app->user->identity->id;
        $seen = SQL::scalar('select count(*) from activity_seen where "user" = :user', [
            ':user' => $user_id
        ]);
        $actual = Activity::find()->count();
        if ($actual > $seen) {
            SQL::execute('insert into activity_seen (activity, "user") select id, :user from activity where id not in (select activity from activity_seen s where s."user" = :user)', [
                ':user' => $user_id
            ]);
        }
        return $this->render('index', [
            'provider' => new ActiveDataProvider([
                'query' => Activity::find()->orderBy(['id' => SORT_DESC])
            ])
        ]);
    }
}
