<?php
/**
 * Last modified: 18.07.20 07:26:07
 * Hash: f642f2d37c58316dc329eb342a23ec0a45fe16fb
 */

namespace app\controllers;


use app\controllers\base\RestController;
use app\models\Article;
use yii\web\NotFoundHttpException;

class ArticleController extends RestController
{
    /**
     * @param $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        $this->layout = 'article';
        return parent::beforeAction($action);
    }

    /**
     * @return array|string
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'models' => Article::find()
                ->where(['type' => 'news'])
                ->orderBy(['id' => SORT_DESC])
                ->all()
        ]);
    }

    /**
     * @param int $id
     * @return array|string
     * @throws \yii\base\InvalidArgumentException
     * @throws NotFoundHttpException
     */
    public function actionView(int $id)
    {
        /** @var Article $model */
        $model = Article::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException('Article not found');
        }
        return $this->render('view', ['model' => $model]);
    }
}
