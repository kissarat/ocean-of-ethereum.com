<?php
/**
 * Last modified: 18.10.14 02:37:22
 * Hash: d24616662b4bace219adaa7c74920fa2b8d61cb1
 */


namespace app\controllers;


use app\models\Claim;
use app\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Transaction;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\ServerErrorHttpException;

class ClaimController extends Controller
{
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index', 'answer', 'remove'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'answer', 'remove'],
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * @return string
     * @throws \yii\base\InvalidArgumentException
     * @throws \yii\db\Exception
     */
    public function actionIndex(): string
    {
        /** @var User $user */
        $user = Yii::$app->user->identity;
        $model = new Claim();
        $can = Claim::can($user);
        if ($can && $model->load(Yii::$app->request->post())) {
            $t = Yii::$app->db->beginTransaction(Transaction::SERIALIZABLE);
            if (!$model->loadTo()) {
                $model->addError('to_nick', Yii::t('app', 'Not found'));
            } elseif ($user->parent === $model->to) {
                $model->addError('to_nick', Yii::t('app', 'User is the same'));
            } elseif (!Claim::can($model->toObject, 'to')) {
                $model->addError('to_nick', Yii::t('app', 'User already has unapproved request'));
            } elseif ($model->create($user)) {
                $t->commit();
                Yii::$app->session->addFlash('success', Yii::t('app', 'Success'));
                $can = Claim::can($user);
            }
        }
        return $this->render('index', [
            'count' => Claim::count($user->id),
            'model' => $model,
            'can' => $can,
            'provider' => new ActiveDataProvider([
                'query' => Claim::find()
                    ->orWhere(['user' => $user->id])
                    ->orWhere(['to' => $user->id])
                    ->orWhere(['from' => $user->id])
                    ->orderBy(['id' => SORT_DESC])
            ])
        ]);
    }

    /**
     * @param int $id
     * @param string $subject
     * @param bool $approve
     * @return Response
     * @throws \yii\web\ServerErrorHttpException
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     */
    public function actionAnswer(int $id, string $subject, bool $approve): Response
    {
        $t = Yii::$app->db->beginTransaction(Transaction::SERIALIZABLE);
        $claim = $this->findModel($id);
        $user_id = (int)Yii::$app->user->getId();
        switch ($subject) {
            case Claim::FROM_SUBJECT:
                if ($claim->from !== $user_id) {
                    throw new ForbiddenHttpException(__LINE__ . ' Cannot approve claim with id ' . $id);
                }
                break;
            case Claim::TO_SUBJECT:
                if ($claim->to !== $user_id) {
                    throw new ForbiddenHttpException(__LINE__ . ' Cannot approve claim with id ' . $id);
                }
                break;
            default:
                throw new ForbiddenHttpException(__LINE__ . ' Cannot approve claim with id ' . $id);
        }
//        if (!$approve) {
//            if ($claim->delete()) {
//                $t->commit();
//                return $this->redirect(['index', 'id' => $id]);
//            }
//            throw new ServerErrorHttpException('Cannot delete claim with id ' . $id);
//        }
        if ($claim->approve($subject, $approve)) {
            $t->commit();
            return $this->redirect(['index', 'id' => $id]);
        }
        throw new ServerErrorHttpException(__LINE__ . ' Cannot approve claim with id ' . $id);
    }

    /**
     * @param int $id
     * @return Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionRemove(int $id)
    {
        $model = $this->findModel($id);
        if ($model->user !== Yii::$app->user->identity->id) {
            throw new ForbiddenHttpException("You can delete only your requests");
        }
        if ($model->getIsUsed()) {
            throw new ForbiddenHttpException("Request already approved");
        }
        if ($model->delete() > 0) {
            Yii::$app->session->addFlash('success', Yii::t('app', 'Successfully'));
            return $this->redirect(['claim/index', 'nick' => Yii::$app->user->identity->nick]);
        }
        throw new NotFoundHttpException('Claim with id ' . $id);
    }

    /**
     * @param int $id
     * @return Claim
     * @throws NotFoundHttpException
     */
    protected function findModel(int $id): Claim
    {
        /** @var Claim $claim */
        $claim = Claim::findOne($id);
        if ($claim) {
            return $claim;
        }
        throw new NotFoundHttpException('Claim with id ' . $id);
    }
}
