<?php

namespace app\controllers;


use app\controllers\base\RestController;
use app\helpers\SQL;
use app\models\Course;
use app\models\CourseUser;
use app\models\Node;
use app\models\Program;
use Yii;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use yii\web\Response;
use yii\web\ServerErrorHttpException;

class CourseController extends RestController
{
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index', 'download'],
                'rules' => [
                    'member' => [
                        'allow' => true,
                        'actions' => ['index', 'download'],
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * @param int $program
     * @throws ForbiddenHttpException
     */
    protected function check(int $program)
    {
        if (Yii::$app->user->identity->getNodes()->where(['program' => $program])->limit(1)->count() === 0) {
            throw new ForbiddenHttpException();
        }
    }

    /**
     * @param int $program_id
     * @return array|string
     * @throws \yii\db\Exception
     * @throws ForbiddenHttpException
     */
    public function actionIndex(int $program_id)
    {
//        $isVIP = false;
//        if (Program::SHARK === $program_id || Program::WHALE === $program_id || Program::DOLPHIN === $program_id) {
//            Yii::$app->session->setFlash('info', Yii::t('app', 'For access to the VIP channel for crypto trading contact us in Skype or Telegram'));
//            $isVIP = true;
//        }
        $this->check($program_id);
        $program = Program::findOne($program_id);
        $downloaded = CourseUser::getDownloaded(Yii::$app->user->id, $program_id);
        $models = Course::find()
            ->from('course_program_view')
            ->where(['program' => $program_id])
            ->all();
        foreach ($models as $model) {
            $model->downloaded = false !== in_array($model->id, $downloaded, true);
        }
        return $this->render('index', [
            'can' => count($downloaded) < $program->courses,
            'models' => $models,
//            'isVIP' => $isVIP
        ]);
    }

    /**
     * @param int $id
     * @return Response
     * @throws ForbiddenHttpException
     * @throws HttpException
     * @throws ServerErrorHttpException
     * @throws \yii\db\Exception
     */
    public function actionDownload(int $id)
    {
        $course = Course::findOne(['id' => $id]);
        if (!$course) {
            throw new HttpException('Course not found');
        }
        $program_id = SQL::scalar('select program from course_program where course = :course', [
            ':course' => $course->id
        ]);
        $this->check($program_id);
        $where = [
            'course' => $id,
            'user' => Yii::$app->user->id
        ];
        $download = CourseUser::find()->where($where)->one();
        if (!$download) {
            $downloaded = CourseUser::getDownloaded(Yii::$app->user->id, $program_id);
            $program = Program::findOne($program_id);
            if (count($downloaded) >= $program->courses) {
                throw new ForbiddenHttpException('You already exceeded limit of downloads');
            }
            $download = new CourseUser($where);
            if (!$download->save()) {
                throw new ServerErrorHttpException();
            }
        }
        if (empty($course->download)) {
            throw new ForbiddenHttpException('Course has no downloaded link');
        }
        return $this->redirect($course->download);
    }
}
