<?php
/**
 * Last modified: 18.10.07 15:03:55
 * Hash: ed6d0dd105daf12217eadfeffe97f9e313044754
 */

namespace app\controllers;


use app\controllers\base\RestController;
use app\helpers\SQL;
use app\helpers\Utils;
use app\models\Coordinate;
use app\models\Log;
use Yii;
use yii\db\Query;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UnauthorizedHttpException;

class HomeController extends RestController
{
    /**
     * @param $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        Yii::$app->controller->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * @return array|string|Response
     * @throws \yii\db\Exception
     */
    public function actionError()
    {
        if (Yii::$app->user->getIsGuest()) {
            Yii::$app->layout = 'outdoor';
        }
        $exception = Yii::$app->getErrorHandler()->exception;
        if ($exception instanceof HttpException) {
            if ($exception instanceof NotFoundHttpException) {
                $article = SQL::queryByKey('article', substr($_SERVER['REQUEST_URI'], 1), 'path');
                if ($article) {
                    return $this->render('@app/views/article/view', ['model' => $article]);
                }
            }
            if ($exception instanceof UnauthorizedHttpException && !$this->isApiCall()) {
                return $this->redirect(['user/login']);
            }
            $this->view->title = Yii::t('app', $exception->getName());
            return $this->render('error', [
                'status' => $exception->statusCode,
                'error' => [
                    'message' => $exception->getMessage() ?: $exception->getName()
                ]
            ]);
        }
        return $this->render('exception', [
            'exception' => $exception
        ]);
    }

    public function actionConstruction()
    {
        return $this->render('construction');
    }

    public function actionCoordinate($time = null, $latitude, $longitude, $altitude = null, $accuracy = null): array
    {
//        Yii::$app->response->headers->add('Cache-Control', 'max-age=3600, public');
        if (!$time) {
            $time = time();
        }
        $time = Utils::timestamp($time / 1000);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $coordinate = new Coordinate([
            'time' => $time,
            'latitude' => $latitude,
            'longitude' => $longitude,
            'altitude' => $altitude,
            'accuracy' => $accuracy
        ]);
        $coordinate->save();
        return [
            'result' => ['id' => $coordinate->id]
        ];
    }

    public function actionDefault()
    {
        return $this->redirect(Yii::$app->user->getIsGuest()
            ? ['user/login']
            : ['user/profile', 'nick' => Yii::$app->user->identity->nick]);
    }

    /*
    public function actionMail(): array
    {
        Utils::mail($_POST['to'], $_POST['subject'], $_POST['content']);
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['success' => true];
    }
    */
    /**
     * @param $uid
     * @param $url
     * @param $spend
     * @return array
     * @throws \yii\db\Exception
     */
    public function actionVisit($uid, $url, $spend): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        SQL::execute('insert into visit(token, url, spend, ip, agent) values (:uid, :url, :spend, :ip, :agent)', [
            ':uid' => $uid,
            ':url' => base64_decode($url),
            ':spend' => $spend,
            ':ip' => Yii::$app->request->getUserIP(),
            ':agent' => Yii::$app->request->getUserAgent(),
        ]);
        return Utils::jsonp(['success' => true]);
    }

    /**
     * @param string $nick
     * @return array
     * @throws \yii\db\Exception
     */
    public function actionNewbie(string $nick): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $token = null;
        if (isset($_COOKIE[ini_get('session.name')])) {
            $token = $_COOKIE[ini_get('session.name')];
        }
        $last = null;
        if ($token) {
            $last = SQL::scalar('select "created" from visit where "token" = :token limit 1', ['token' => $token]);
        }
        if (!$token || !$last) {
            SQL::execute('insert into visit("token", "url", "ip", "agent") values (:uid, :url, :ip, :agent)', [
                ':uid' => $token,
                ':url' => '/ref/' . $nick,
                ':ip' => Yii::$app->request->getUserIP(),
                ':agent' => Yii::$app->request->getUserAgent(),
            ]);
        }
        return Utils::jsonp([
            'success' => true,
            'last' => $last
        ]);
    }

    public function actionAgreement()
    {
        return $this->redirect('https://info0cean.com/usloviya-ispolzovaniya/?utm_source=office');
    }

    /**
     * @return array|string
     * @throws \yii\db\Exception
     */
    public function actionSitemap()
    {
        Yii::$app->response->format = Response::FORMAT_RAW;
        $articles = SQL::queryAll('select path, created from article where path is not null');
        $this->layout = false;
        Yii::$app->response->getHeaders()->add('Content-Type', 'text/xml; charset=utf-8');
        return $this->render('sitemap', ['articles' => $articles]);
    }

    public function actionFlush($redis = false): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
//        if (YII_DEBUG) {
//            FileHelper::removeDirectory(Yii::getAlias('@app/runtime'));
//            FileHelper::removeDirectory(Yii::getAlias('@app/web/assets'));
//        }
        Yii::$app->cache->flush();
        Yii::$app->db->schema->refresh();
        if ($redis) {
            Yii::$app->storage->redis->flushAll();
        }
        return ['success' => true];
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public function actionWroclaw(): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $logs = (new Query())->from('log')
            ->select('entity, action, count(*) as count')
            ->where(['>', 'id', Log::lastTime()])
            ->groupBy(['entity', 'action'])
            ->all();
        $activity = [];
        foreach ($logs as $log) {
            $activity[$log['entity']][$log['action']] = $log['count'];
        }
        $activity['ip'] = SQL::scalar('select count(*) from (select ip from log where id > :time group by ip) t', [
            ':time' => Log::lastTime()
        ]);

        $gigabyte = 1024 * 1024 * 1024;

        $result = [
            'success' => true,
            'ethereum' => false,
            'mode' => YII_ENV,
            'debug' => YII_DEBUG,
            'time' => date('Y-m-d H:i:s'),
            'name' => Yii::$app->name,
            'language' => Yii::$app->language,
            'charset' => Yii::$app->charset,
            'cache' => Yii::$app->cache instanceof \yii\caching\ApcCache,
            'mirror' => Yii::$app->request->mirror,
            'ip' => Yii::$app->request->getUserIP(),
            'timezone' => SQL::scalar('SHOW timezone'),
            'agent' => Yii::$app->request->getUserAgent(),
            'accept_language' => Yii::$app->request->getHeaders()->get('accept-language'),
//            'settings' => Yii::$app->settings->getSummary(),
            'space' => [
                disk_free_space('/') / $gigabyte
            ],
            'activity' => $activity
        ];

        try {
            $etherscanBlock = hexdec(Yii::$app->etherscan->get(null, ['module' => 'proxy', 'action' => 'eth_blockNumber'])['result']);
            $block = Yii::$app->eth->getBlockNumber();
            $processed = Yii::$app->eth->getLast();
            $result['block'] = [
                'etherscan' => $etherscanBlock,
                'local' => $block,
                'delta' => $etherscanBlock - $block,
                'processed' => $processed,
                'remain' => $block - $processed
            ];
            $result['ethereum'] = true;
        } catch (\Exception $exception) {

        }

        return $result;
    }

    public function actionBanner()
    {
        $this->view->title = Yii::t('app', 'Banners');
        return $this->render('banner');
    }

//    public function actionNotify(string $text) {
//        Yii::$app->telegram->sendText($text);
//    }
}
