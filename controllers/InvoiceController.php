<?php

namespace app\controllers;


use app\models\form\Payment;
use app\models\form\Withdraw;
use app\models\Log;
use app\models\Node;
use app\models\Transfer;
use app\models\User;
use Yii;
use yii\web\Controller;

class InvoiceController extends Controller
{
    public function actionPayment() {
        $model = new Payment();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            return $this->redirect([$model->system . '/pay', 'amount' => $model->amount * Transfer::RATE]);
        }
        return $this->render('payment', ['model' => $model]);
    }

    /**
     * @return string|\yii\web\Response
     * @throws \yii\db\Exception
     */
    public function actionWithdraw()
    {
        /** @var User $user */
        $user = Yii::$app->user->identity;
        if (Node::find()->where(['user' => $user->id])->limit(1)->count() === 0) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'To be able to withdraw money, you need to buy a place in the matrix'));
            return $this->redirect(['user/profile', 'nick' => $user->nick]);
        }
        if (empty($user->pin)) {
            Yii::$app->session->setFlash('error',
                Yii::t('app', 'User has no financial password'));
            return $this->redirect(['user/profile', 'nick' => $user->nick]);
        }
        $transfer = Transfer::findOne([
            'from' => $user->id,
            'type' => 'withdraw',
            'status' => 'created'
        ]);
        if ($transfer) {
            Yii::$app->session->setFlash('error',
                Yii::t('app', 'You cannot create more than one withdrawal request'));
            Log::log('withdraw', 'second');
            return $this->redirect(['transfer/index', 'id' => $transfer->id]);
        }
        $model = new Withdraw();

//        if ('GET' === Yii::$app->request->method) {
//        Yii::$app->session->setFlash('warning',
//            Yii::t('app', 'You will be charged 5% for the website maintenance after the withdrawal'));
//        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->checkPin($user)) {
            $amount = $model->amount * Transfer::RATE;
            $t = Yii::$app->db->beginTransaction();
//            if ($amount >= Withdraw::$minimal) {
//                $model->addError('amount', Yii::t('app', 'Minimal amount is {amount}', [
//                    'amount' => ' ETH'
//                ]));
//            }

            $currency = Transfer::BASE_CURRENCY;
            $balance = User::getBalance($user->id, $currency);
            if ($balance - $amount >= 0) {
                $transfer = new Transfer([
                    'type' => 'withdraw',
                    'status' => 'created',
                    'currency' => $currency,
                    'from' => $user->id,
                    'amount' => $amount,
                    'wallet' => $model->wallet,
                    'text' => empty($model->text) ? null : $model->text
                ]);
                if ($transfer->save(false)) {
                    Yii::$app->session->setFlash('success',
                        Yii::t('app', 'Withdrawal request created'));
                    $t->commit();
                    return $this->redirect(['transfer/index', 'id' => $transfer->id]);
                }
            } else {
                Log::log('withdraw', 'insufficient', [
                    'balance' => $balance,
                    'amount' => $amount,
                    'wallet' => $model->wallet,
                    'currency' => Transfer::CURRENCY
                ]);
                $model->addError('amount', Yii::t('app', 'Insufficient funds'));
            }
            $t->rollBack();
        }
        return $this->render('withdraw', [
            'model' => $model
        ]);
    }

}