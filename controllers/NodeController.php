<?php
/**
 * Last modified: 18.09.25 03:46:23
 * Hash: b5146d49a8bb49978592b2badcb15f988c55295e
 */

namespace app\controllers;


use app\controllers\base\RestController;
use app\helpers\SQL;
use app\models\Log;
use app\models\Node;
use app\models\Offer;
use app\models\Program;
use app\models\Swap;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\QueryParamAuth;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\ServerErrorHttpException;

class NodeController extends RestController
{
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ],
            'authenticator' => [
                'class' => CompositeAuth::class,
                'only' => ['view'],
                'authMethods' => [
                    QueryParamAuth::class,

                ]
            ]
        ];
    }

    /**
     * @param int $id
     * @param int|null $offer
     * @return array|string|Response
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     * @throws ForbiddenHttpException
     */
    public function actionView(int $id, int $offer = null)
    {
        $this->layout = 'embed';
        return $this->actionMatrix($id, $offer);
    }

    /**
     * @param int $id
     * @param int $offer
     * @param bool $direct
     * @return array|string|Response
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     * @throws ForbiddenHttpException
     */
    public function actionMatrix(int $id, ?int $offer = null, bool $direct = false)
    {
        Offer::checkAvailability($offer);
        $ref_node = Node::findOne($id);
        if (!$ref_node) {
            throw new NotFoundHttpException('Node not found');
        }
        $root = $ref_node->getRoot();
        if ($direct) {
            $this->layout = 'embed';
        } else {
            if ($root->id !== $id) {
                return $this->redirect(['node/matrix',
                    'id' => $root->id,
                    'offer' => $offer
                ]);
            }
        }
        $models = Node::getMatrix($id);
        $nodes = [];
        $level = 0;
        foreach ($models as $model) {
            $level = max($level, $model['level']);
            $nodes[$model['number']] = $model;
        }
        $program = null;
        if (\count($models) > 0) {
            $program = Program::findOne($root['program']);
        } else {
            throw new NotFoundHttpException('Matrix not found');
        }
        return $this->render('matrix', [
            'offer' => $offer,
            'buy' => $offer !== null && $offer !== 0,
            'program' => $program,
            'nodes' => $nodes,
            'root' => $root
        ]);
    }

    /**
     * @param $program
     * @param int $offer
     * @return Response
     * @throws \yii\db\Exception
     * @throws ForbiddenHttpException
     */
    public function actionRandom(int $program, ?int $offer = null)
    {
        Offer::checkAvailability($offer);
        $id = SQL::scalar('select id from matrix_count
          where opened and not closed and "program" = :program
          order by filled, random() limit 1', [
            ':program' => $program
        ]);
        if (null === $id) {
            $id = SQL::scalar('select id from matrix_count
              where not closed and "program" = :program
              order by filled, random() limit 1', [
                ':program' => $program
            ]);
            Log::debug('matrix', 'not_found', ['found' => $id]);
        }
        return $this->redirect(['matrix', 'id' => $id, 'offer' => $offer]);
    }

    /**
     * @param $id
     * @param $number
     * @param $offer
     * @return Response
     * @throws HttpException
     * @throws \Error
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     */
    public function actionBuy(int $id, int $number, ?int $offer = null): ?Response
    {
        /** @var User $user */
        $user = Yii::$app->user->identity;
        $tx = Yii::$app->db->beginTransaction();
        try {
//            $node_id = 0;
            if ($offer > 0) {
                /**
                 * @var Offer $o
                 */
                $o = Offer::findOne($offer);
                if ($o) {
                    if ($user->id === $o->user && !$o->used) {
//                        header('OPEN: ' . $id);
                        $node_id = $o->open($id, $number);
                    } else {
                        throw new ForbiddenHttpException('It is not your offer');
                    }
                } else {
                    throw new NotFoundHttpException('Offer not found');
                }
            } else {
//                header('BUY: ' . $id);
                $node_id = Program::buy($id, $user->id, $number);
            }
            if ($node_id > 0) {
                $tx->commit();
                return $this->redirect(['node/matrix', 'id' => $node_id]);
            }

            throw new ServerErrorHttpException('Invalid node_id: ' . $node_id);
        } catch (HttpException $ex) {
            if (402 === $ex->statusCode) {
                return $this->redirect(['transfer/pay']);
            }

            throw $ex;
        }
    }

    /**
     * @param int $id
     * @return array
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionRoot(int $id): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $node = Node::findOne($id);
        if ($node) {
            return [
                'count' => $node->getMemberCount(),
                'result' => $node->attributes
            ];
        }
        throw new NotFoundHttpException();
    }
}
