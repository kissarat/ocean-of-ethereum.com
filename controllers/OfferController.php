<?php
/**
 * Last modified: 18.07.28 03:53:53
 * Hash: 80b4c5d7cc12f9c0ced4d786ddf578b3a5c59301
 */

namespace app\controllers;


use app\models\Offer;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;

class OfferController extends Controller
{
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['@']
                    ]
                ]
            ],
        ];
    }

    /**
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'provider' => new ActiveDataProvider([
                'query' => Offer::find()
                    ->innerJoin('program', '"program"."id" = "offer"."program"')
                    ->where(['user' => (int)Yii::$app->user->getId()])
                    ->orderBy(['id' => SORT_DESC])
            ])
        ]);
    }
}
