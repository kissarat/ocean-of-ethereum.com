<?php
/**
 * Last modified: 18.05.09 23:08:43
 * Hash: 7ad2245f329a3832c4e683cf2f7ae55dc1719bcb
 */

namespace app\controllers;


use app\controllers\base\YarcodeController;
use app\helpers\Utils;
use app\models\Log;
use app\models\Transfer;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

class PayeerController extends YarcodeController
{
    public $enableCsrfValidation = false;

    protected function getPaymentSystemId(): string
    {
        return 'payeer';
    }

    protected function checkSign(array $data)
    {
        $parts = [
            ArrayHelper::getValue($data, 'm_operation_id'),
            ArrayHelper::getValue($data, 'm_operation_ps'),
            ArrayHelper::getValue($data, 'm_operation_date'),
            ArrayHelper::getValue($data, 'm_operation_pay_date'),
            ArrayHelper::getValue($data, 'm_shop'),
            ArrayHelper::getValue($data, 'm_orderid'),
            ArrayHelper::getValue($data, 'm_amount'),
            ArrayHelper::getValue($data, 'm_curr'),
            ArrayHelper::getValue($data, 'm_desc'),
            ArrayHelper::getValue($data, 'm_status'),
            $this->getComponent()->secret,
        ];

        $sign = strtoupper(hash('sha256', implode(':', $parts)));
        return ArrayHelper::getValue($data, 'm_sign') === $sign;
    }

    protected function handleRequest(array $data): Transfer
    {
        $transfer = Transfer::findOne([
            'guid' => $data['m_orderid'],
            'type' => Transfer::TYPE_PAYMENT,
            'system' => 'payeer',
            'currency' => 'USD',
        ]);
        if (!$transfer) {
            Log::log('payeer', 'not_found', $data);
            throw new NotFoundHttpException();
        }
        if (Transfer::STATUS_SUCCESS === $transfer->status) {
            return $transfer;
        }
//        if (Transfer::STATUS_CREATED !== $transfer->status) {
//            throw new BadRequestHttpException('Invalid status');
//        }
        if (!$this->checkSign($data)) {
            Log::log('payeer', 'invalid', $data);
            throw new BadRequestHttpException('Invalid signature');
        }
        $transfer->amount = floor($data['m_amount'] * Transfer::FACTOR['USD']);
        $transfer->status = 'success' === $data['m_status'] ? Transfer::STATUS_SUCCESS : 'fail';
        $transfer->txid = $data['m_operation_id'];
//            $transfer->wallet = Utils::timestamp();
        $transfer->time = Utils::timestamp();
        unset($data['m_orderid'], $data['m_amount'], $data['m_operation_id']);
        $transfer->vars = $data;
        if ($transfer->update(false)) {
            return $transfer;
        }
        Log::log('payeer', 'invalid', $data);
        return null;
    }
}
