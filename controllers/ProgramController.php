<?php
/**
 * Last modified: 18.11.16 04:03:18
 * Hash: c934debf160f1d56ec2126c4793387811a85d184
 */

namespace app\controllers;


use app\behaviors\Access;
use app\controllers\base\RestController;
use app\helpers\Utils;
use app\models\form\Username;
use app\models\Node;
use app\models\Offer;
use app\models\Program;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ProgramController extends RestController
{
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index', 'open', 'sponsor'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'open', 'sponsor'],
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * @return array|string
     * @throws \yii\base\InvalidArgumentException
     * @throws \yii\db\Exception
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'models' => $this->getUser()->getProgramNodes()
        ]);
    }

    /**
     * @param int $program
     * @param User|null $user
     * @param int|null $offer
     * @return Response
     * @throws \yii\db\Exception
     */
    protected function openNode(int $program, ?User $user, ?int $offer = null): Response
    {
        if ($node = Program::tryOpen($program, $user)) {
            return $this->redirect(['node/matrix',
                'nick' => $this->getUser()->nick,
                'id' => $node->id,
                'offer' => $offer
            ]);
        }
        return $this->redirect(['program/sponsor',
            'id' => $program,
            'offer' => $offer
        ]);
    }

    /**
     * @param int $id
     * @param int $offer
     * @param int $level
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     * @throws \yii\web\ForbiddenHttpException
     * @throws BadRequestHttpException
     */
    public function actionOpen(int $id, int $offer = -1, int $level = 1): Response
    {
        if (!(1 === $level || 2 === $level)) {
            throw new BadRequestHttpException('Invalid level in program.open');
        }
        Offer::checkAvailability($offer);
        if ($offer > 0 || Program::canBuy($id)) {
            $parent = $this->getUser();
            for ($i = 0; $i < $level; $i++) {
                if (!($parent = $parent->parentObject)) {
                    throw new NotFoundHttpException('Parent user not found in program.open');
                }
            }
            return $this->openNode($id, $parent, $offer);
        }
        return $this->redirect(['invoice/payment', 'nick' => Yii::$app->user->identity->nick]);
    }

    /**
     * @param int $id
     * @param int $offer
     * @return array|string|Response
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionSponsor(int $id, int $offer = -1)
    {
        Offer::checkAvailability($offer);
        /** @var Node $node */
        $node = Node::findOne($id);
        if (!$node) {
            throw new NotFoundHttpException("Matrix #$id not found (choose-sponsor)");
        }
        $program = $node->programObject;
        $model = new Username();
        /** @var Node $sponsor_node */
        $sponsor_node = Node::find()
            ->where(['user' => Yii::$app->user->identity->parent, 'program' => $node->program])
            ->one();
        if ($sponsor_node && !$sponsor_node->isMatrixActive()) {
            /** @var Node $sponsor_next_node */
            $sponsor_next_node = Node::find()
                ->where(['user' => Yii::$app->user->identity->parent, 'program' => $node->program + 1])
                ->one();
            if ($sponsor_next_node) {
                Yii::$app->session->addFlash('warning', Yii::t('app', 'Your sponsor has moved to the matrix {name}', [
                    'name' => $sponsor_next_node->programObject->name
                ]));
            } else {
                Yii::$app->session->addFlash('warning', Yii::t('app', 'Your sponsor has closed the matrix {name}', [
                    'name' => $program->name
                ]));
            }
        }
//        else {
//            Yii::$app->session->addFlash('warning', Yii::t('app', 'You have no sponsor in {name}', [
//                'name' => $program->name
//            ]));
//        }
        if ($model->load(Yii::$app->request->post()) && $model->validate(['nick'])) {
            /** @var User $found */
            $found = User::find()
                ->where(['nick' => $model->nick])
                ->select(['id'])
                ->one();
            if ($found) {
                /** @var Node $target_node */
                $target_node = Node::find()
                    ->where(['user' => $found->id, 'program' => $node->program])
                    ->one();
                if ($target_node) {
                    return $this->openNode($id, $found, $offer);
                }
                $model->addError('nick', Yii::t('app', 'This user is not in the matrix {name}', [
                    'name' => $program->name
                ]));
            } else {
                $model->addError('nick', Yii::t('app', 'Not found'));
            }
        }
        return $this->render('sponsor', [
            'id' => $id,
            'offer' => $offer,
            'model' => $model,
            'program' => $program
        ]);
    }
}
