<?php
/**
 * Last modified: 18.10.06 17:49:43
 * Hash: 4cd3c2b2e162692909b2bfebaebb6c33936d8818
 */

namespace app\controllers;


use app\models\form\Password;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\UploadedFile;

class SettingsController extends Controller
{
    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index', 'profile', 'sponsor'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'profile', 'sponsor'],
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionProfile()
    {
        $model = User::findOne(['id' => Yii::$app->user->identity->id]);
        /** @var User $model */
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $file = UploadedFile::getInstance($model, 'avatar_file');
            if ($file) {
                $nick = Yii::$app->user->identity->nick;
                if ($model->getOldAttribute('avatar')) {
                    $old = Yii::getAlias('@app/web' . $model->getOldAttribute('avatar'));
                    if (file_exists($old)) {
                        unlink($old);
                    }
                }
                $md5 = md5_file($file->tempName);
                $url = "/avatars/$nick-$md5." . $file->getExtension();
                move_uploaded_file($file->tempName, Yii::getAlias('@app/web' . $url));
                $model->avatar = $url;
            }
            $model->save(false);
            Yii::$app->session->setFlash('success', Yii::t('app', 'Profile saved successfully'));
            return $this->redirect(['user/profile', 'nick' => $model->nick]);
        }
        return $this->render('profile', [
            'model' => $model
        ]);
    }

    /**
     * @return string
     */
    public function actionPassword()
    {
        $model = new Password();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            /** @var User $user */
            $user = User::findOne(['id' => Yii::$app->user->identity->id]);
            if ($user->validatePassword($model->current)) {
                $user->password = $model->new;
                $user->generateSecret();
                $user->save(false);
                Yii::$app->session->setFlash('success', Yii::t('app', 'Successfully'));
            } else {
                $model->addError('current', Yii::t('app', 'Invalid password'));
            }
        }
        return $this->render('password', [
            'model' => $model
        ]);
    }
}
