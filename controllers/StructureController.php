<?php
/**
 * Last modified: 18.07.26 12:34:32
 * Hash: 848e00dd180c24c77b08b02b985b0381718e56f1
 */

namespace app\controllers;


use app\controllers\base\RestController;
use app\models\Referral;
use app\models\Visit;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\web\Response;

class StructureController extends RestController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * @param int $id
     * @param int $level
     * @return array|string
     */
    public function actionIndex(int $id = 0, int $level = 1)
    {
        $isChildren = $id > 0;
        if ($isChildren) {
            $this->layout = false;
        } else {
            $id = Yii::$app->user->identity->id;
        }
        return $this->render($isChildren ? 'children' : 'index', [
            'id' => $id,
            'level' => $level,
            'provider' => new ArrayDataProvider([
                'allModels' => Referral::find()
                    ->where(['parent' => $id])
                    ->orderBy(['id' => SORT_DESC])
                    ->all()
            ])
        ]);
    }

    public function actionVisit(): array
    {
        Yii::$app->response->format = Response::FORMAT_JSONP;
        $visit = new Visit([
            'url' => '/ref/' . Yii::$app->request->get('r'),
            'ip' => Yii::$app->request->getUserIP()
        ]);
        $visit->save();
        return [
            'callback' => Yii::$app->request->get('callback') ?: 'callback',
            'data' => ['ok' => 1]
        ];
    }
}
