<?php
/**
 * Last modified: 18.10.18 23:54:18
 * Hash: 719f5cc844f2cd897fc5baae19a68afe7f569dd6
 */

namespace app\controllers;

use app\helpers\SQL;
use app\helpers\Utils;
use app\models\form\Username;
use app\models\Node;
use app\models\Swap;
use app\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\ServerErrorHttpException;

class SwapController extends Controller
{
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index', 'create', 'approve'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'create', 'approve'],
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * @param int $from
     * @return Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function actionCreate(int $from)
    {
        /**
         * @var User $found_user
         * @var Node $from_node
         * @var Node $to_node
         * @var Swap $swap
         */
        $from_level = 0;
        $t = Yii::$app->db->beginTransaction();
        $from_node = Node::findOne($from);
        if (!$from_node) {
            throw new NotFoundHttpException('Node not found');
        }
        if (!Swap::can($from_node->userObject, $from_node->program)) {
            throw new ForbiddenHttpException('Cannot create swap');
        }
        $swap = Swap::find()
            ->andWhere(['from' => $from])
            ->andWhere('approved is null')
            ->one();
        $model = new Username();
        if ($swap) {
            $model->addError('nick', Yii::t('app', 'You already sent request'));
        } else {
            if ($from_node->user !== Yii::$app->user->identity->id) {
                throw new ForbiddenHttpException('You are not an owner of node with id: ' . $from);
            }
            $from_root = $from_node->getRoot($from_level);
            $program = $from_node->programObject;
//            \assert($program, 'Program not found');
            if ($from_root->id === $from_node->id) {
                throw new ForbiddenHttpException('You are in the head of matrix with id: ' . $from);
            }
//            \assert($from_level < $program->level, 'Invalid root');
            if ($model->load(Yii::$app->request->post()) && $model->validate(['nick'])) {
                $found_user = User::find()
                    ->where(['nick' => $model->nick])
                    ->select(['id', 'swap_count'])
                    ->one();
                if ($found_user && $found_user->id !== $from_node->user) {
                    $to_node = $found_user->getActiveMatrix($from_node->program);
                    if ($to_node) {
                        if (Swap::can($found_user, $from_node->program)) {
                            $to_level = 0;
                            $to_root = $to_node->getRoot($to_level);
                            if ($from_level === $to_level) {
                                $swap = new Swap([
                                    'from' => $from_node->id,
                                    'from_user' => $from_node->user,
                                    'to' => $to_node->id,
                                    'to_user' => $to_node->user,
                                    'root' => $to_root->id,
                                    'level' => $to_level
                                ]);
                                if ($swap->insert(false)) {
                                    $t->commit();
                                    return $this->redirect(['swap/index', 'id' => $swap->id]);
                                }
                            } else {
                                $model->addError('nick', Yii::t('app', 'You are at the different level'));
                            }
                        } else {
                            $model->addError('nick', Yii::t('app', 'The user has already used his limits of swaps', [
                                'nick' => $model->nick
                            ]));
                        }
                    } else {
                        $model->addError('nick', Yii::t('app', 'This user is not in the matrix {name}', [
                            'name' => $program->name
                        ]));
                    }
                } else {
                    $model->addError('nick', Yii::t('app', 'User not found'));
                }
            }
        }
        return $this->render('create', ['model' => $model]);
    }

    /**
     * @param int $id
     * @param bool $approve
     * @return \yii\web\Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws ServerErrorHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     */
    public function actionApprove(int $id, bool $approve)
    {
        $t = Yii::$app->db->beginTransaction();
        $user_id = Yii::$app->user->identity->id;
        $swap = $this->findModel($id);
        $to_node = $swap->toObject;
        if (!$to_node) {
            throw new NotFoundHttpException('Receiver node not found');
        }
        if ($user_id !== $to_node->user) {
            throw new ForbiddenHttpException("It's not request for you");
        }
        $from_node = $swap->fromObject;
        if (!$from_node) {
            throw new NotFoundHttpException('Sender node not found');
        }
        $level = 0;
        $root = $to_node->getRoot($level);
        if ($root->id === $swap->root && $level === $swap->level) {
            $commit = true;
            if ($approve) {
                $parent_id = $to_node->parent;
                $from_children = Node::findAll(['parent' => $from_node->id]);
                $to_children = Node::findAll(['parent' => $to_node->id]);
                $cell = $to_node->cell;
                $to_node->parent = $from_node->parent;
                $to_node->cell = $from_node->cell;
                $from_node->parent = $parent_id;
                $from_node->cell = $cell;
                $commit = $from_node->update(false)
                    && $to_node->update(false)
                    && Swap::updateParent($from_children, $to_node->id)
                    && Swap::updateParent($to_children, $from_node->id);
            }
            $swap->approved = $approve;
            $swap->time = Utils::timestamp();
            $commit = $commit && $swap->update(false);
            if ($commit) {
                $t->commit();
                return $this->redirect(['swap/index', 'id' => $id]);
            }
            throw new ServerErrorHttpException('Cannot save swap');
        }
        $swap->delete();
        $t->commit();
        throw new ForbiddenHttpException("Matrix #$root->id changed");
    }

    public function actionIndex()
    {
        /** @var User $user */
        $user = Yii::$app->user->identity;
        $count = Swap::countForUser($user->id);
        $number = $user->swap_count - $count;
        Yii::$app->session->addFlash($number > 0 ? 'info' : 'warning', Yii::t('app', 'Number of swaps is <span data-number="{number}">{number}</span>', [
            'number' => $number
        ]));
        return $this->render('index', [
            'provider' => new ActiveDataProvider([
                'query' => Swap::forUser($user->id)->orderBy(['id' => SORT_DESC])
            ])
        ]);
    }

    /**
     * @param int $id
     * @return Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionRemove(int $id)
    {
        $model = $this->findModel($id);
        if ($model->from_user !== Yii::$app->user->identity->id) {
            throw new ForbiddenHttpException("You can delete only your requests");
        }
        if (!$model->getIsPending()) {
            throw new ForbiddenHttpException("Request already approved");
        }
        if ($model->delete() > 0) {
            Yii::$app->session->addFlash('success', Yii::t('app', 'Successfully'));
            return $this->redirect(['swap/index', 'nick' => Yii::$app->user->identity->nick]);
        }
        throw new NotFoundHttpException('Swap with id ' . $id);
    }

    /**
     * @param int $id
     * @return Swap
     * @throws NotFoundHttpException
     */
    protected function findModel(int $id): Swap
    {
        /** @var Swap $claim */
        $claim = Swap::findOne($id);
        if ($claim) {
            return $claim;
        }
        throw new NotFoundHttpException('Swap with id ' . $id);
    }
}
