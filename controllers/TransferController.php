<?php
/**
 * Last modified: 18.11.21 00:02:30
 * Hash: db0e4d83a77a3bf85c04707f6972b8e4a68bb457
 */

namespace app\controllers;


use app\base\Request;
use app\controllers\base\RestController;
use app\helpers\Utils;
use app\models\form\Internal;
use app\models\form\Withdraw;
use app\models\Log;
use app\models\Node;
use app\models\Transfer;
use app\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Transaction;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\web\ServerErrorHttpException;

class TransferController extends RestController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index', 'withdraw', 'wallet', 'internal'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'withdraw', 'wallet', 'internal'],
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    public function actionIndex()
    {
        $user = Yii::$app->user->identity->id;
        $query = Transfer::find()
            ->where(['or', ['from' => $user], ['to' => $user]])
            ->andFilterWhere(Utils::pick($_GET, ['currency', 'type']))
            ->orderBy(['id' => SORT_DESC]);
        $provider = new ActiveDataProvider([
            'query' => $query
        ]);

        return $this->render('index', [
            'provider' => $provider
        ]);
    }

    /**
     * @return array
     * @throws ServerErrorHttpException
     */
    public function actionWallet(): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (!Yii::$app->has('eth')) {
            throw new ServerErrorHttpException();
        }
        return ['result' => Yii::$app->eth->getUserAddress(Yii::$app->user->identity->id)];
    }

    /**
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public function actionInternal()
    {
        /** @var User $from */
        $from = Yii::$app->user->identity;
        if (empty($from->pin)) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'User has no financial password'));
            return $this->redirect(['user/profile', 'nick' => $from->nick]);
        }
        $model = new Internal();
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->checkPin($from)) {
            if ($from->nick === $model->nick) {
                $model->addError('nick', Yii::t('app', 'You cannot send money to yourself'));
            } else {
                $t = Yii::$app->db->beginTransaction(Transaction::SERIALIZABLE);
                if ($model->can($from->id)) {
                    $to = $model->getRecipient();
                    $internal = new Transfer([
                        'type' => 'internal',
                        'status' => 'success',
                        'amount' => $model->getCent(),
                        'from' => $from->id,
                        'to' => $to->id,
                        'text' => $model->text ?: null,
                        'vars' => [
                            'from_nick' => $from->nick,
                            'to_nick' => $to->nick
                        ]
                    ]);
                    if ($internal->insert()) {
                        $t->commit();
                        $array = [$model->nick, $model->amount];
                        if ($model->text) {
                            $array[] = $model->text;
                        }
                        Yii::$app->response->headers->add('Hash', md5(implode(':', $array)));
                        Yii::$app->session->addFlash('success', Yii::t('app', 'Success'));
                        return $this->redirect(['index', 'id' => $internal->id]);
                    }
                    throw new ServerErrorHttpException('Cannot create internal transfer');
                }
                $model->addError('amount', Yii::t('app', 'Insufficient funds'));
            }
        }
        return $this->render('internal', [
            'model' => $model
        ]);
    }

    /**
     * @param string $currency
     * @param null|string $back
     * @return Response
     * @throws BadRequestHttpException
     */
//    public function actionCurrency(string $currency = Transfer::BASE_CURRENCY, ?string $back = null) {
//        if (!\in_array($currency, Transfer::getCurrencies(),true)) {
//            throw new BadRequestHttpException('Invalid currency ' . $currency);
//        }
//        setcookie('currency', $currency, time() + 3600 * 24 * 365, '/');
//        if ($back) {
//            return $this->redirect($back);
//        }
//        return $this->redirect(['user/profile', 'nick' => Yii::$app->user->identity->nick]);
//    }
}
