<?php
/**
 * Last modified: 18.12.11 01:19:19
 * Hash: a31022880624333a9ee669d21af8a9ed013bb744
 */

namespace app\controllers;

use app\controllers\base\RestController;
use app\models\Email;
use app\models\form\Login;
use app\models\form\PasswordReset;
use app\models\form\RequestPasswordReset;
use app\models\Log;
use app\models\Token;
use app\models\Transfer;
use app\models\User;
use Yii;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\filters\HttpCache;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\ServerErrorHttpException;

class UserController extends RestController
{
    public function behaviors(): array
    {
        return [
            'cache' => [
                'class' => HttpCache::class,
                'enabled' => !YII_DEBUG,
                'only' => ['login', 'signup', 'request'],
                'lastModified' => function () {
                    $time = time();
                    return $time - $time % 3600;
                }
            ],
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout', 'request', 'reset', 'profile'],
                'rules' => [
                    'guest' => [
                        'allow' => true,
                        'actions' => ['request', 'reset'],
                        'roles' => ['?']
                    ],
                    'member' => [
                        'allow' => true,
                        'actions' => ['profile', 'logout'],
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * @param $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if ('authorize' === $action->id || 'login' === $action->id) {
            Yii::$app->request->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    protected function saveSponsor($nick): string
    {
        $nick = $nick ?: Yii::$app->request->getCookie(User::SPONSOR_COOKIE, User::ROOT_NICK);
        if (User::ROOT_NICK !== $nick) {
            setcookie(User::SPONSOR_COOKIE, $nick, time() + User::SPONSOR_COOKIE_EXPIRES, '/');
        }
        return $nick;
    }

    public function actionSignup($sponsor = null)
    {
        if (!Yii::$app->user->getIsGuest()) {
            return $this->redirect(['user/profile', 'nick' => Yii::$app->user->identity->nick]);
        }

        $this->layout = 'outdoor';
        $sponsor = $this->saveSponsor($sponsor);
        $model = new User(['scenario' => User::SCENARIO_REGISTER]);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->parent = User::getIdByNick($model->sponsor ?: $sponsor ?: User::ROOT_NICK);
            $model->generateSecret();
            $model->save(false);
            if (Yii::$app->user->login($model)) {
//                Email::send($model->email,
//                    Yii::t('app', '{name}: Welcome', ['name' => Yii::$app->name]),
//                    Yii::t('app', "Dear, {nick}\nThank you for registration at {name}", [
//                        'nick' => $model->nick,
//                        'name' => Yii::$app->name
//                    ]));
                Log::log('user', 'signup', ['ip' => $model->ip]);
                return $this->redirect(['user/profile', 'nick' => $model->nick]);
            }
        } else {
            $model->sponsor = $sponsor;
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function actionLogin($sponsor = null)
    {
        if (!Yii::$app->user->getIsGuest()) {
            return $this->redirect(['user/profile', 'nick' => Yii::$app->user->identity->nick]);
        }

        $this->layout = 'outdoor';
        $this->saveSponsor($sponsor);
        $model = new Login();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $data = ['ip' => $model->ip];
            $user = null;
            if (Login::checkLoginAttempt(['ip' => Yii::$app->request->getUserIP()])) {
                $user = $model->getUser();
                if ($user) {
                    if (Login::checkLoginAttempt(['user' => $user->id])) {
                        if ($user->validatePassword($model->password)) {
                            Yii::$app->user->login($user);
                            Log::log('auth', 'login', $data);
                            $url = Yii::$app->user->getReturnUrl();
                            return $this->redirect(empty($url) ? ['user/profile', 'nick' => $user->nick] : $url);
                        }
                        $model->addError('password', Yii::t('app', 'Invalid password'));
                    }
                } else {
                    $model->addError('login', Yii::t('app', 'User does not exists'));
                }
            }
            if (!$user) {
                $data['login'] = $model->login;
            }
            Log::log('auth', 'invalid', $data, $user ? $user->id : null);
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * @param $name
     * @param $value
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionExists($name, $value) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ('nick' === $name || 'email' === $name) {
            return ['result' => User::find()->where([$name => $value])->limit(1)->count() > 0];
        }
        throw new BadRequestHttpException('Invalid name ' . $name);
    }

    public function actionLogout()
    {
        $id = $this->getUser()->id;
        Yii::$app->user->logout(false);
        Log::log('auth', 'logout', null, $id);
        return $this->redirect(['user/login']);
    }

    /**
     * @param $nick
     * @return array|string
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionProfile($nick)
    {
        $informer = (new Query())
            ->from('informer')
            ->where(['nick' => $nick])
            ->one();
        if (empty($informer)) {
            throw new NotFoundHttpException(Yii::t('app', 'User not found'));
        }
        $informer = array_merge($informer, Transfer::getFinance($informer['id']));

        if ($informer['parent']) {
            $informer['parent'] = json_decode($informer['parent']);
        }

        return $this->render('profile', [
            'model' => $informer,
            'programs' => $this->getUser()->getProgramNodes()
        ]);
    }

    /**
     * @return array|string
     * @throws \yii\base\Exception
     */
    public function actionRequest()
    {
        $this->layout = 'outdoor';
        $model = new RequestPasswordReset();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = $model->getUser();
            $success = $user && ($code = $user->generateCode())
                && Email::send($user->email, Yii::t('app', 'Password Recovery in the "{name}" project', [
                    'name' => Yii::$app->name
                ]),
                    Yii::t('app', "Hello!\nYou requested a password reset from your account.\nFollow the link to reset your password - {url} \n\nSincerely, \"{name}\" Administration", [
                        'url' => Url::to(['user/reset', 'code' => $code], true),
                        'name' => Yii::$app->name
                    ]));
            if ($success) {
                Yii::$app->session->addFlash('success', Yii::t('app', 'Check your email'));
            } else {
                Yii::$app->session->addFlash('error', Yii::t('app', 'User not found'));
            }
        }
        return $this->render('request', ['model' => $model]);
    }

    /**
     * @param $code
     * @return array|string|Response
     * @throws \yii\base\InvalidArgumentException
     * @throws ServerErrorHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionReset($code)
    {
        $this->layout = 'outdoor';
        $model = new PasswordReset();
        $token = Token::findOne(['id' => $code]);
        if ($token) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $user = User::findOne(['id' => $token->user]);
                if (!$user) {
                    throw new ServerErrorHttpException('User not found');
                }
                /** @var User $user */
                $user->password = $model->password;
                $user->generateSecret();
                if ($user->save(false) && $token->delete()) {
                    Yii::$app->session->addFlash('success', Yii::t('app', 'Password changed'));
                }
                return $this->redirect(['user/login']);
            }
        } else {
            Yii::$app->session->addFlash('error', Yii::t('app', 'Password recovery code not found'));
//            return $this->redirect(['user/login']);
        }
        return $this->render('reset', [
            'token' => $token,
            'model' => $model
        ]);
    }

    /**
     * @param $nick
     * @param $token
     * @return Response
     * @throws ForbiddenHttpException
     * @throws ServerErrorHttpException
     */
    public function actionAuthorize($nick, $token): Response
    {
        /** @var Token $auth */
        $auth = Token::findOne(['id' => $token]);
        if ($auth) {
            $user = User::findOne(['nick' => $nick]);
            if ($user) {
                $u = $auth->userObject;
                if (!$u) {
                    throw new ServerErrorHttpException('User not found');
                }
                if ($u->admin || YII_DEBUG) {
//            $allowed = YII_DEBUG ? true : Ip::can(Yii::$app->request->getUserIP());
                    if (Yii::$app->user->login($user)) {
                        Log::log('auth', 'admin', [
                            'nick' => $user->nick,
                            'token' => $auth->id,
                            'agent' => Yii::$app->request->getUserAgent()
                        ]);
                        return $this->redirect(['user/profile', 'nick' => $user->nick]);
                    }
                    throw new ForbiddenHttpException('cannot login ' . $nick);
                }
                throw new ForbiddenHttpException('admin ' . $auth->userObject->nick);
            }
            throw new ForbiddenHttpException('user ' . $nick);
        }
        throw new ForbiddenHttpException('token');
    }
}