<?php
/**
 * Last modified: 18.07.30 10:36:05
 * Hash: 5b9a0417f42e844bb420867359e39453f00bf877
 */

namespace app\controllers\admin;


use app\helpers\SQL;
use app\models\Claim;
use app\models\form\Generator;
use app\models\Node;
use app\models\Offer;
use app\models\Transfer;
use app\models\User;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

class GenerateController extends Controller
{
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['user', 'clear'],
                'rules' => [
                    'member' => [
                        'allow' => true,
                        'actions' => ['user', 'clear'],
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * @param $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     * @throws ForbiddenHttpException
     */
    public function beforeAction($action): bool
    {
        if (YII_TEST) {
            $this->enableCsrfValidation = false;
            return parent::beforeAction($action);
        }
        throw new ForbiddenHttpException('Server is not in test mode');
    }

    /**
     * @return string
     * @throws ForbiddenHttpException
     * @throws \Error
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\HttpException
     */
    public function actionUser(): string
    {
        if (!YII_DEBUG) {
            throw new ForbiddenHttpException();
        }
        $model = new Generator();
        $users = [];
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $users = $model->generate();
        }

        return $this->render('@app/views/generate/user', [
            'model' => $model,
            'provider' => new ArrayDataProvider([
                'allModels' => $users
            ])
        ]);
    }

    /**
     * @return \yii\web\Response
     * @throws \yii\db\Exception
     */
    public function actionClear()
    {
        Claim::deleteAll();
        Node::deleteAll(['>', 'id', 20]);
        Offer::deleteAll();
        Transfer::deleteAll();
//        $users = json_decode(file_get_contents(Yii::getAlias('@app/console/seeds/user.json')), JSON_OBJECT_AS_ARRAY);
//        $user = end($users);
        $id = 6;
        User::deleteAll(['>', 'id', $id]);
        SQL::restartSequence('user', $id + 1);

        return $this->redirect(User::findOne(['nick' => Yii::$app->user->identity->nick]) ? ['generate/user'] : ['user/login']);
    }
}
