<?php
/**
 * Last modified: 18.06.06 13:52:03
 * Hash: 2bbdf3c0afbd5bab9cd7e2be9ba2c5e2911fd0aa
 */

namespace app\controllers\base;


use Yii;
use yii\db\ActiveRecord;
use yii\web\Controller;
use yii\web\Response;

abstract class AdminController extends Controller
{
    /**
     * @var ActiveRecord $modelClass
     */
    public $modelClass;

    /**
     * @param $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if (Yii::$app->request->getIsAjax()) {
            Yii::$app->response->format = Response::FORMAT_JSON;
        }
        else {
            Yii::$app->layout = 'admin';
        }
//        if (YII_TEST) {
            return parent::beforeAction($action);
//        }
//        return false;
    }
}
