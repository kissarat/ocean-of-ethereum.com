<?php
/**
 * Last modified: 18.12.11 01:18:06
 * Hash: bac9c6fb8fd42976d620a1c12302069e67fedb2f
 */

namespace app\controllers\base;


use Yii;
use yii\base\Model;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

/**
 *
 * @property \app\models\User $user
 */
abstract class RestController extends Controller
{
    /**
     * @param $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if (YII_TEST) {
            $this->enableCsrfValidation = false;
        }
//        if (!Yii::$app->user->getIsGuest() && Yii::$app->user->identity->admin) {
//            Yii::$app->layout = 'admin';
//        }
        return parent::beforeAction($action);
    }

    /**
     * @return \app\models\User
     */
    public function getUser(): \app\models\User
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return Yii::$app->user->identity;
    }

    /**
     * @return bool
     * Checks if request is api invocation
     */
    public function isApiCall(): bool
    {
        return isset(Yii::$app->request->getAcceptableContentTypes()['application/json'])
            || 'json' === Yii::$app->request->get('format');
    }

    /**
     * @param string $view
     * @param array $params
     * @return array|string
     * Returns json data if it is api invocation renders view otherwise
     * @throws \yii\base\InvalidArgumentException
     */
    public function render($view, $params = [])
    {
        if (isset($params['search']) && !isset($params['provider'])) {
            $params['provider'] = $params['search']->search(Yii::$app->request->get());
        }
        if ($this->isApiCall()) {
            return $this->json($params);
        }
        return parent::render($view, $params);
    }

    /**
     * @param $params
     * @return mixed
     */
    public function json($params)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (!YII_DEBUG) {
            Yii::$app->response->statusCode = 403;
            return ['message' => 'Nothing is possible!'];
        }
        $r = $params;
//        $r = isset($params['status']) ? $params : [];
//        $r['spend'] = microtime(true) - $_SERVER['REQUEST_TIME_FLOAT'];
        if (isset($params['provider'])) {
            /**
             * @var \yii\data\ActiveDataProvider $q
             */
            $q = $params['provider'];
            $models = $q->getModels();
            $page = $q->getPagination();
            $sort = [];
            foreach ($q->sort->orders as $key => $order) {
                $sort[$key] = $order === SORT_ASC ? 1 : -1;
            }
            if (count($sort) > 0) {
                $r['sort'] = $sort;
            }
            $r['page'] = [
                'offset' => $page->offset,
                'limit' => $page->limit,
                'total' => $page->totalCount
            ];
            $r['result'] = $models;
        } else if (isset($params['model'])) {
            $r['result'] = $params['model'];
            if ($params['model'] instanceof Model && $params['model']->hasErrors()) {
                $r['errors'] = $params['model']->getErrors();
            }
            unset($r['model']);
        } else if (isset($params['models'])) {
            $r['result'] = $params['models'];
            unset($r['models']);
        }
        unset($r['provider']);
//        $r['alerts'] = Yii::$app->session->getAllFlashes(true);
        return $r;
    }
}
