<?php
/**
 * Last modified: 18.10.18 22:53:35
 * Hash: ded89f99b25e9cd825e4305feae1b2fbf138c4d9
 */

$file = file_get_contents(__DIR__ . '/' . $argv[1] . '.sql');
$objects = [];
foreach (preg_split('/\s*;\s*/', $file) as $s) {
    if (empty(trim($s))) {
        continue;
    }
    preg_match('/((materialized)?\s*VIEW|TABLE|TYPE)\s+"([\w_]+)"/i', $s, $m);
    if (!empty($m) && !empty($m[1])) {
//        echo json_encode($m) . "\n";
        if ('materialized' === $m[2]) {
            $d = 'DROP materialized view IF EXISTS "' . $m[3] . '"';
        }
        else {
            $d = 'DROP ' . $m[1] . ' IF EXISTS "' . $m[3] . '"';
        }
        if ('-' === $s[0]) {
            $d = '-- ' . $d;
        }
        $objects[] = $d;
    }
//    else {
//        echo "Invalid view: $s\n";
//        throw new \Exception("Invalid view: $s\n\n\n");
//    }
}

echo "-- Last modified: 18.10.18 22:53:35\n";
echo "-- Hash: ded89f99b25e9cd825e4305feae1b2fbf138c4d9\n";

foreach (array_reverse($objects) as $object) {
    echo "$object;\n";
}
