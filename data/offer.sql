insert into offer ("user", origin, program)
  select
    "user",
    id,
    case when program % 2 = 0
      then program - 1
    else program + 1 end as program
  from matrix_count m
  where closed and id not in (select o.origin
                              from offer o
                              group by o.origin);

insert into offer ("user", origin, "program")
  select
    n."user",
    n.id,
    n.program + 1 as next_program
  from matrix_count n
  where closed and n.program % 2 = 0 and origin is not null
        and (select count(*) = 0
             from node n2
             where n.program + 1 = n2.program and n."user" = n2."user"
             limit 1) and n.program + 1 < (
    select max(id)
    from program
  )
  except
  select
    "user",
    origin  as id,
    program as next_program
  from offer;
