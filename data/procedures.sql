CREATE OR REPLACE FUNCTION get_root(_id INT, _level BIGINT)
  RETURNS INT AS $$
DECLARE
  root_id INT;
BEGIN
  WITH RECURSIVE r(id, parent, level) AS (
    SELECT
      id,
      parent,
      _level AS level
    FROM node
    WHERE id = _id
    UNION
    SELECT
      n.id,
      n.parent,
      r.level - 1 AS level
    FROM node n
      JOIN r ON r.parent = n.id
    WHERE r.level > 0 AND r.parent IS NOT NULL
  )
  SELECT id
  FROM r
  ORDER BY level
  LIMIT 1
  INTO root_id;
  RETURN root_id;
END
$$ LANGUAGE plpgsql STABLE;

CREATE OR REPLACE FUNCTION count_matrix(root_id INT)
  RETURNS INT AS $$
DECLARE
  _count INT;
BEGIN
  WITH RECURSIVE r(id, parent, level, program) AS (
    SELECT
      id,
      parent,
      0 :: INT AS "level",
      program
    FROM node
    WHERE id = root_id
    UNION
    SELECT
      n.id,
      n.parent,
      r.level + 1 AS "level",
      n.program
    FROM node n
      JOIN r ON r.id = n.parent
    WHERE r.level < 3 + (1 - n.program % 2)
  )
  SELECT count(*)
  FROM r INTO _count;
  RETURN _count;
END
$$ LANGUAGE plpgsql STABLE;

