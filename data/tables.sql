-- Last modified: 19.02.05 22:47:23
-- Hash: 7f8b1e79f17b4f36b717ad8a6d9a4dce11545435

create type "token_type" as enum (
  'server',
  'browser',
  'app',
  'code'
);

create type "user_type" as enum ('new', 'native', 'leader', 'special');

create table "user" (
  id            serial primary key                                  not null,
  nick          character varying(24)                               not null,
  email         character varying(48)                               not null,
  type          "user_type" default 'new' :: "user_type"            not null,
  parent        integer,
  secret        character(60),
  pin           char(4),
  admin         boolean default false                               not null,
  surname       character varying(48),
  forename      character varying(48),
  avatar        character varying(192),
  skype         character varying(32),
  phone         character varying(32),
  telegram      character varying(32),
  country       character varying(64),
  settlement    character varying(128),
  address       character varying(128),
  eth           character varying(42),
  site          character varying(192),
  geo           boolean                                             not null default false,
  major         boolean                                             not null default false,
  created       timestamp(0) without time zone                               default now(),
  "time"        timestamp(0) without time zone,
  auth          char(64),
  swap_count    SMALLINT                                            NOT NULL DEFAULT 2,
  claim_count   SMALLINT                                            NOT NULL DEFAULT 2,
  last_activity smallint                                            not null default 0
);

insert into "user" (id, nick, email) values (1, 'ocean', 'ocean@mail.local');
alter sequence user_id_seq restart 2;

create table "token" (
  id        character varying(192)                                                                      not null,
  "user"    integer,
  type      "token_type"                                                                                not null default 'browser',
  expires   timestamp(0) without time zone default '2030-01-01 00:00:00' :: timestamp without time zone not null,
  data      json,
  ip        inet,
  handshake timestamp,
  name      character varying(240),
  created   timestamp(0) without time zone                                                                       default now(),
  "time"    timestamp(0) without time zone
);

create type "article_type" as enum ('cat', 'page', 'news', 'ticket', 'note');

create table "article" (
  id       serial primary key not null,
  path     varchar(96) unique,
  "type"   "article_type"     not null default 'page',
  --   program  SMALLINT REFERENCES program (id) ON UPDATE NO ACTION ON DELETE NO ACTION,
  price    int,
  name     varchar(96)        not null,
  short    varchar(512),
  time     timestamp,
  created  timestamp          not null default CURRENT_TIMESTAMP,
  text     varchar(48000),
  image    varchar(256),
  "user"   int references "user" (id),
  priority smallint           not null default 0,
  active   boolean            not null default true
);

create table "relation" (
  "from" int not null references article (id)
  on delete cascade on update cascade,
  "to"   int not null references article (id)
  on delete cascade on update cascade,
  unique ("from", "to")
);

insert into "article" (path, name) values
  ('faq', 'FAQ'),
  ('marketing', 'Маркетинг'),
  ('contacts', 'Контакты');

create table "session" (
  id     char(40) not null primary key,
  expire integer,
  data   bytea
);

create table "cache" (
  id     char(128) not null primary key,
  expire int,
  data   bytea
);

create table "visit" (
  "id"      serial primary key,
  "token"   varchar(240),
  "url"     varchar(240) not null,
  "spend"   int,
  ip        inet,
  agent     varchar(240),
  "created" timestamp default current_timestamp
);

create type "transfer_type" as enum ('internal', 'payment', 'withdraw', 'accrue', 'buy', 'support',
  'write-off', 'bonus', 'refund', 'qualification', 'fee');
create type "transfer_status" as enum ('created', 'success', 'fail', 'canceled', 'pending', 'virtual');
create type "transfer_system" as enum ('perfect', 'advcash', 'payeer');
create type "currency" as enum ('ETH', 'USD');

create table "transfer" (
  id       serial primary key,
  "from"   int references "user" (id)
  on delete cascade on update cascade,
  "to"     int references "user" (id)
  on delete cascade on update cascade,
  amount   int8              not null,
  "type"   "transfer_type"   not null,
  "status" "transfer_status" not null,
  node     int,
  text     varchar(192),
  vars     json,
  wallet   varchar(64),
  txid     varchar(66),
  system   "transfer_system",
  currency "currency"        not null default 'USD',
  ip       inet,
  guid     char(8),
  created  timestamp         not null default CURRENT_TIMESTAMP,
  time     timestamp
);

create table "program" (
  id       smallint primary key,
  price    int         not null,
  name     varchar(24) not null,
  previous int references program (id),
  active   boolean     not null default true,
  courses  int         not null default 0
);

insert into "program" (id, "name", price, previous, courses) values
  (11, 'CRAB Stream', 1000, null, 3),
  (12, 'CRAB Lagoon', 1000, 11, 3),
  (13, 'FISH Stream', 3000, 12, 5),
  (14, 'FISH Lagoon', 3000, 13, 5),
  (15, 'DOLPHIN Stream', 9000, 14, 5),
  (16, 'DOLPHIN Lagoon', 9000, 15, 5),
  (17, 'SHARK Stream', 27000, 16, 1000000),
  (18, 'SHARK Lagoon', 27000, 17, 1000000),
  (19, 'WHALE Stream', 81000, 18, 1000000),
  (20, 'WHALE Lagoon', 81000, 19, 1000000);

create table "node" (
  id            serial primary key,
  "user"        int references "user" (id)
  on update cascade on delete cascade,
  program       int       not null references program (id),
  parent        int references node (id)
  on update cascade on delete cascade,
  qualification int references node (id)
  on update cascade on delete cascade,
  qualified     smallint,
  cell          smallint  not null default 0,
  created       timestamp not null default CURRENT_TIMESTAMP,
  time          timestamp
);

insert into "node" (id, "user", program)
  select
    id,
    1,
    id
  from program p
  order by p.id;

alter sequence node_id_seq restart 31;

create table "offer" (
  id      serial primary key,
  "user"  int references "user" (id)
  on update cascade on delete cascade,
  origin  int references node (id)
  on update cascade on delete cascade,
  program int       not null references program (id),
  used    int references node (id)
  on update cascade on delete set null,
  created timestamp not null default CURRENT_TIMESTAMP,
  time    timestamp
);

create table "swap" (
  id          serial primary key,
  "from"      int       not null references node (id)
  on update cascade on delete cascade,
  "from_user" int       not null references "user" (id)
  on update cascade on delete cascade,
  "to"        int references node (id)
  on update cascade on delete set null,
  "to_user"   int       not null references "user" (id)
  on update cascade on delete cascade,
  "root"      int       not null references node (id)
  on update cascade on delete cascade,
  level       smallint  not null,
  approved    boolean,
  created     timestamp not null default CURRENT_TIMESTAMP,
  time        timestamp,
  visible     boolean   not null default true
);

create table "claim" (
  id            serial primary key,
  "user"        int       not null references "user" (id),
  "from"        int       not null references "user" (id),
  "to"          int       not null references "user" (id),
  from_approved boolean,
  to_approved   boolean,
  created       timestamp not null default now(),
  "time"        timestamp
);

create type "request_method" as enum ('OPTIONS', 'GET', 'HEAD', 'POST', 'PUT', 'PATCH', 'DELETE', 'TRACE', 'CONNECT');

create table "request" (
  id     bigserial primary key,
  time   timestamp        not null default CURRENT_TIMESTAMP,
  ip     inet,
  url    varchar(4096)    not null,
  method "request_method" not null,
  type   varchar(64),
  agent  varchar(512),
  "data" text,
  guid   char(32),
  token  varchar(240),
  "user" int
);

create table "reward" (
  level   smallint primary key,
  percent float not null
);

insert into "reward" values
  (1, 0.4);

create table "log" (
  id     bigint primary key,
  entity varchar(16) not null,
  action varchar(32) not null,
  ip     inet,
  "user" int references "user" (id)
  on delete cascade on update cascade,
  token  character varying(192),
  data   json
);

create table "coordinate" (
  id                serial primary key,
  time              timestamp not null default CURRENT_TIMESTAMP,
  latitude          float8    not null,
  longitude         float8    not null,
  altitude          float8,
  accuracy          smallint,
  altitude_accuracy smallint,
  heading           float8,
  speed             float8
);

create table "wallet" (
  id      varchar(64) primary key,
  system  "transfer_system" not null,
  amount  int8              not null,
  expires timestamp,
  time    timestamp         not null,
  created timestamp         not null default CURRENT_TIMESTAMP
);

create table "email" (
  id      bigserial primary key,
  "to"    varchar(192),
  subject varchar(192),
  content text,
  created timestamp not null default CURRENT_TIMESTAMP,
  sent    timestamp
);

create table "exchange" (
  "from" currency  not null,
  "to"   currency  not null,
  "rate" float     not null default 1 check ("rate" > 0),
  time   timestamp not null default '1970-01-01 00:00:00',
  unique ("from", "to")
);

insert into "exchange" ("from", "to", rate) values ('ETH', 'ETH', 100000);
insert into "exchange" ("from", "to", rate) values ('ETH', 'USD', 200);
insert into "exchange" ("from", "to", rate) values ('USD', 'USD', 100);

create table "ethereum" (
  id      CHAR(66) PRIMARY KEY,
  "from"  CHAR(42)        NOT NULL,
  "to"    CHAR(42)        NOT NULL,
  value   DECIMAL(24, 18) NOT NULL,
  created timestamp       not null default CURRENT_TIMESTAMP
);

CREATE TABLE ip (
  id      INET PRIMARY KEY,
  text    VARCHAR(80),
  editor  INT       NOT NULL,
  ip      INET      NOT NULL,
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  time    TIMESTAMP
);

create table cat (
  id      smallserial primary key,
  parent  smallint references cat (id),
  name    varchar(192) not null,
  created TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
  time    TIMESTAMP
);

create table course (
  id       smallserial primary key,
  title    varchar(250) not null,
  image    varchar(250),
  about    varchar(250),
  download varchar(250),
  created  TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
  time     TIMESTAMP
);

create table course_program (
  course  smallint references course (id) on delete cascade on update cascade,
  program smallint references program (id) on delete cascade on update cascade,
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

create table course_cat (
  course  smallint references course (id) on delete cascade on update cascade,
  cat     smallint references cat (id) on delete cascade on update cascade,
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

create table course_user (
  course  smallint references course (id) on delete cascade on update cascade,
  "user"  int references "user" (id) on delete cascade on update cascade,
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

create table activity (
  id            smallserial primary key,
  title         varchar(250),
  description   varchar(5000),
  speaker       varchar(100),
  online_url    varchar(250),
  recording_url varchar(250),
  start         timestamp,
  created       TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  time          TIMESTAMP
);

create table activity_seen (
  activity smallint  not null references activity (id),
  "user"   int       not null references "user" (id),
  unique (activity, "user"),
  created  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

insert into activity (title, description, speaker, online_url, recording_url, start) values (
  'Первый общепроектный вебинар',
  'Знакомство с администрацией. О площадке. Доходность. Маркетинг',
  'Алена Авантис',
  null,
  'https://youtu.be/2mPrMmx5DMs',
  '2018-12-06'
);

insert into activity (title, description, speaker, online_url, recording_url, start) values (
  'Вебинар «Инфо Океан» ',
  'Новости. Разбор программ. Маркетинг. Доходность',
  'Алена Авантис',
  null,
  'https://youtu.be/ZT0FY2hW0dY',
  '2018-12-16'
);

insert into activity (title, description, speaker, online_url, recording_url, start) values (
  'Заключительный вебинар 2018 года',
  'О площадке. Маркетинг. Доходность',
  'Алена Авантис',
  null,
  'https://youtu.be/1aKo70oyiS0',
  '2018-12-23'
);

insert into activity (title, description, speaker, online_url, recording_url, start) values (
  'Новогодний розыгрыш от «Инфо Океан»',
  'Условия новогоднего конкурса',
  'Алена Авантис',
  null,
  'https://youtu.be/MibyzSStmW0',
  '2018-12-24'
);

insert into activity (title, description, speaker, online_url, recording_url, start) values (
  'Прямой эфир новогоднего розыгрыша',
  'Выбираем и поздравляем победителей',
  'Алена Авантис',
  null,
  'https://youtu.be/hoHow_isV9U',
  '2018-12-26'
);

insert into activity (title, description, speaker, online_url, recording_url, start) values (
  'Первая инфо-школа от «Инфо Океана»',
  'Секреты эффективности и достижений',
  'Алена Авантис',
  null,
  'https://youtu.be/aPkC6EW7f_s',
  '2019-01-09'
);

insert into activity (title, description, speaker, online_url, recording_url, start) values (
  'Второе занятие инфо-школы',
  'Стратегии рекрутирования',
  'Алена Авантис',
  'https://pruffme.com/landing/u769083/info0cean',
  null,
  '2019-01-16'
);

insert into activity (title, description, speaker, online_url, recording_url, start) values (
  'Третье занятие инфо-школы',
  'Стратегии рекрутирования. Часть 2',
  'Алена Авантис',
  'https://pruffme.com/landing/u769083/info0cean',
  null,
  '2019-01-29'
);

insert into course (title, image, about, download, created) values
  ('Евгений Кузнецов «Интенсив по управлению временем»',
   'https://info0cean.com/wp-content/uploads/2018/10/1-upravlenie-vremenem.jpg',
   'https://info0cean.com/evgenij-kuznetsov-intensiv-po-upravleniyu-vremenem/', 'https://fex.net/332973651941',
   '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Максим Чернов «Успех через связи: видеокурс по нетворкингу»',
   'https://info0cean.com/wp-content/uploads/2018/10/2.-Maksim-CHernov-Uspeh-cherez-svyazi-videokurs-po-netvorkingu-.png',
   'https://info0cean.com/maksim-chernov-uspeh-cherez-svyazi-videokurs-po-netvorkingu/',
   'https://fex.net/838964129092', '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Елена Блиновская «Расширение финансового сознания»',
   'https://info0cean.com/wp-content/uploads/2018/10/3-Elena-Blinovskaya-Marafon-zhelanij-i-rasshirenie-finansovogo-soznaniya-2018-02.jpg',
   'https://info0cean.com/elena-blinovskaya-marafon-zhelanij-i-rasshirenie-finansovogo-soznaniya-2018-audio/',
   'https://fex.net/629153592975', '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values ('PIMEN «Криптотрейдинг для начинающих»',
                                                                    'https://info0cean.com/wp-content/uploads/2018/10/1.-Mihail-Pimenov-Kriptotrejding-dlya-nachinayushhih-1-1.png',
                                                                    'https://info0cean.com/pimen-kriptotrejding-dlya-nachinayushhih/',
                                                                    'https://fex.net/910960108085',
                                                                    '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values ('Школа трейдинга Harvest «»',
                                                                    'https://info0cean.com/wp-content/uploads/2018/10/2.-SHkola-trejdinga-HARVEST.jpg',
                                                                    'https://info0cean.com/shkola-trejdinga-harvest/',
                                                                    'https://fex.net/221408339778',
                                                                    '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Артём Сребный «Курс по трейдингу»', 'https://info0cean.com/wp-content/uploads/2018/10/all-platform-1.png',
   'https://info0cean.com/artyom-srebnyj-kurs-po-trejdingu-dlya-novichka-s-nulya-bazovyj-1-0-2018/',
   'https://fex.net/224783182394', '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values ('Радислав Гандапас «Идеология Лидера»',
                                                                    'https://info0cean.com/wp-content/uploads/2018/10/1-ideologiya-lidera.jpg',
                                                                    'https://info0cean.com/radislav-gandapas-ideologiya-lidera/',
                                                                    'https://fex.net/573211185707',
                                                                    '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values ('Радислав Гандапас «Алгоритмы успеха»',
                                                                    'https://info0cean.com/wp-content/uploads/2018/10/5-kartinka-k-video-Algoritmy-Uspeha-02.jpg',
                                                                    'https://info0cean.com/radislav-gandapas-algoritmy-uspeha/',
                                                                    'https://fex.net/326857245882',
                                                                    '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values ('Радислав Гандапас «Скрипты успеха»',
                                                                    'https://info0cean.com/wp-content/uploads/2018/10/3-Radislav-Gandapas-Skripty-uspeha-02.jpg',
                                                                    'https://info0cean.com/radislav-gandapas-skripty-uspeha/',
                                                                    'https://fex.net/821624238530',
                                                                    '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values ('Радислав Гандапас «Формула мотивации»',
                                                                    'https://info0cean.com/wp-content/uploads/2018/10/6-kartinka-k-video-Formula-motivatsii-02.jpg',
                                                                    'https://info0cean.com/radislav-gandapas-formula-motivatsii/',
                                                                    'https://fex.net/032964308151',
                                                                    '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Радислав Гандапас «Полноценная жизнь как бизнес-проект»',
   'https://info0cean.com/wp-content/uploads/2018/10/5-Radislav-Gandapas-Polnotsennaya-zhizn-kak-biznes-proekt-02.jpg',
   'https://info0cean.com/radislav-gandapas-polnotsennaya-zhizn-kak-biznes-proekt/',
   'https://fex.net/156191309113', '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values ('Армен Геворкян «Интенсив-курс по криптотрейдингу»',
                                                                    'https://info0cean.com/wp-content/uploads/2018/10/1.-Armen-Gevorkyan-Intensiv-kurs-po-profitnomu-kriptotrejdingu-.jpeg',
                                                                    'https://info0cean.com/armen-gevorkyan-intensiv-kurs-po-profitnomu-kriptotrejdingu-2018/',
                                                                    'https://fex.net/825676585585',
                                                                    '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values ('TSI Analytics Group «Трейдинг на криптовалютах»',
                                                                    'https://info0cean.com/wp-content/uploads/2018/10/2.-TSI-Analytics-Group-Trejding-na-kriptovalyutah-volnovoj-podhod-2018.jpg',
                                                                    'https://info0cean.com/tsi-analytics-group-trejding-na-kriptovalyutah-volnovoj-podhod-2018/',
                                                                    'https://fex.net/492985930782',
                                                                    '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values ('Дмитрий Карпиловский «Криптобизнес 3.0»',
                                                                    'https://info0cean.com/wp-content/uploads/2018/10/3.-Dmitrij-Karpilovskij-Kriptobiznes-3.0-2017.jpg',
                                                                    'https://info0cean.com/dmitrij-karpilovskij-kriptobiznes-3-0-2017/',
                                                                    'https://fex.net/162264662825',
                                                                    '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Трейдинг от А до Я с Александром Герчиком за 60 дней «»',
   'https://info0cean.com/wp-content/uploads/2018/10/4.-Trejding-ot-A-do-YA-s-Aleksandrom-Gerchikom-za-60-dnej.jpg',
   'https://info0cean.com/trejding-ot-a-do-ya-s-aleksandrom-gerchikom-za-60-dnej/',
   'https://fex.net/539487774625', '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values ('HAMAHA «Обучение торговле криптовалютами»',
                                                                    'https://info0cean.com/wp-content/uploads/2018/10/5.-HAMAHA-Obuchenie-torgovle-kriptovalyutami.jpg',
                                                                    'https://info0cean.com/hamaha-obuchenie-torgovle-kriptovalyutami/',
                                                                    'https://fex.net/696049335132',
                                                                    '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values ('Мария Солодар «Школа интернет-маркетологов»',
                                                                    'https://info0cean.com/wp-content/uploads/2018/10/1.-Mariya-Solodar-SHkola-Internet-Marketologov.jpg',
                                                                    'https://info0cean.com/mariya-solodar-2-h-mesyachnaya-shkola-internet-marketologov/',
                                                                    '#', '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values ('Алексей Лукьянов «Денежный Ютубер Пакет Практик»',
                                                                    'https://info0cean.com/wp-content/uploads/2018/10/photo_2018-10-21_04-00-48.jpg',
                                                                    'https://info0cean.com/aleksej-lukyanov-denezhnyj-yutuber-paket-praktik/',
                                                                    '#', '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Skillbox + Ingate «Практический интенсивный курс SMM менеджер»',
   'https://info0cean.com/wp-content/uploads/2018/10/3-SkillboxIngate-Prakticheskij-intensivnyj-kurs-SMM-menedzher-2.jpg',
   'https://info0cean.com/skillbox-ingate-prakticheskij-intensivnyj-kurs-smm-menedzher-2018/', '#',
   '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values ('SEOman «SEO-специалист с нуля Пакет Premium»',
                                                                    'https://info0cean.com/wp-content/uploads/2018/10/4.-SEOman-SEO-spetsialist-s-nulya-za-2-mesyatsa-.jpg',
                                                                    'https://info0cean.com/seoman-seo-spetsialist-s-nulya-za-2-mesyatsa-paket-premium-2018/',
                                                                    '#', '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Алексей Князев «Курс по настройке таргетинговой рекламы»',
   'https://info0cean.com/wp-content/uploads/2018/10/5.-Aleksej-Knyazev-Samyj-effektivnyj-kurs-po-nastrojke-targetingovoj-reklamy-18-potok-768x432.jpg',
   'https://info0cean.com/aleksej-knyazev-samyj-effektivnyj-kurs-po-nastrojke-targetingovoj-reklamy-18-potok/',
   '#', '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Cryptominds «Курсы по Блокчейну и Криптоторговле (сборник) 2018»',
   'https://info0cean.com/wp-content/uploads/2018/10/1-Cryptominds-Kursy-po-Blokchejnu-i-Kriptotorgovle-01.png',
   'https://info0cean.com/cryptominds-kursy-po-blokchejnu-i-kriptotorgovle-2018-sbornik/',
   'https://fex.net/523406588211', '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Ethereum Works «Разработка на блокчейне Ethereum Базовый курс 2018»',
   'https://info0cean.com/wp-content/uploads/2018/10/shkola_blokcheyn_razrabotki_ethereumworks.png',
   'https://info0cean.com/ethereum-works-razrabotka-na-blokchejne-ethereum-bazovyj-kurs-2018-2/', '#',
   '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Сергей Атрощенко «Манипуляции. Нарушение теханализа»',
   'https://info0cean.com/wp-content/uploads/2018/10/3.-Sergej-Atroshhenko.-Sekrety-Manipulyatsij.-Narushenie-tehanaliza.jpg',
   'https://info0cean.com/ethereum-works-razrabotka-na-blokchejne-ethereum-bazovyj-kurs-2018/', '#',
   '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Форекс и инвестиции «Техника прибыли Price Action»',
   'https://info0cean.com/wp-content/uploads/2018/10/4.-Foreks-i-investitsii-Price-Action.Tehnika-pribyli-.jpg',
   'https://info0cean.com/foreks-i-investitsii-price-action-tehnika-pribyli/', '#', '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Елена Калашникова (Estrader) «Профиль рынка Price Action»',
   'https://info0cean.com/wp-content/uploads/2018/10/5.jpg',
   'https://info0cean.com/elena-kalashnikova-estrader-profil-rynka-price-action/', '#',
   '2018-11-01T07:52:15.978Z');

insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/evgenij-kuznetsov-intensiv-po-upravleniyu-vremenem/'),
                                                     11);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/maksim-chernov-uspeh-cherez-svyazi-videokurs-po-netvorkingu/'),
                                                     11);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/elena-blinovskaya-marafon-zhelanij-i-rasshirenie-finansovogo-soznaniya-2018-audio/'),
                                                     11);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/pimen-kriptotrejding-dlya-nachinayushhih/'),
                                                     11);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/shkola-trejdinga-harvest/'),
                                                     11);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/artyom-srebnyj-kurs-po-trejdingu-dlya-novichka-s-nulya-bazovyj-1-0-2018/'),
                                                     11);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/radislav-gandapas-ideologiya-lidera/'),
                                                     13);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/radislav-gandapas-algoritmy-uspeha/'),
                                                     13);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/radislav-gandapas-skripty-uspeha/'),
                                                     13);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/radislav-gandapas-formula-motivatsii/'),
                                                     13);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/radislav-gandapas-polnotsennaya-zhizn-kak-biznes-proekt/'),
                                                     13);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/armen-gevorkyan-intensiv-kurs-po-profitnomu-kriptotrejdingu-2018/'),
                                                     13);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/tsi-analytics-group-trejding-na-kriptovalyutah-volnovoj-podhod-2018/'),
                                                     13);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/dmitrij-karpilovskij-kriptobiznes-3-0-2017/'),
                                                     13);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/trejding-ot-a-do-ya-s-aleksandrom-gerchikom-za-60-dnej/'),
                                                     13);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/hamaha-obuchenie-torgovle-kriptovalyutami/'),
                                                     13);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/mariya-solodar-2-h-mesyachnaya-shkola-internet-marketologov/'),
                                                     15);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/aleksej-lukyanov-denezhnyj-yutuber-paket-praktik/'),
                                                     15);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/skillbox-ingate-prakticheskij-intensivnyj-kurs-smm-menedzher-2018/'),
                                                     15);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/seoman-seo-spetsialist-s-nulya-za-2-mesyatsa-paket-premium-2018/'),
                                                     15);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/aleksej-knyazev-samyj-effektivnyj-kurs-po-nastrojke-targetingovoj-reklamy-18-potok/'),
                                                     15);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/cryptominds-kursy-po-blokchejnu-i-kriptotorgovle-2018-sbornik/'),
                                                     15);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/ethereum-works-razrabotka-na-blokchejne-ethereum-bazovyj-kurs-2018-2/'),
                                                     15);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/ethereum-works-razrabotka-na-blokchejne-ethereum-bazovyj-kurs-2018/'),
                                                     15);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/foreks-i-investitsii-price-action-tehnika-pribyli/'),
                                                     15);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/elena-kalashnikova-estrader-profil-rynka-price-action/'),
                                                     15);


insert into course (title, image, about, download, created) values
  ('Евгений Кузнецов «Интенсив по управлению временем»',
   'https://info0cean.com/wp-content/uploads/2018/10/1-upravlenie-vremenem.jpg',
   'https://info0cean.com/evgenij-kuznetsov-intensiv-po-upravleniyu-vremenem/?=shark', 'https://fex.net/332973651941',
   '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Максим Чернов «Успех через связи: видеокурс по нетворкингу»',
   'https://info0cean.com/wp-content/uploads/2018/10/2.-Maksim-CHernov-Uspeh-cherez-svyazi-videokurs-po-netvorkingu-.png',
   'https://info0cean.com/maksim-chernov-uspeh-cherez-svyazi-videokurs-po-netvorkingu/?=shark',
   'https://fex.net/838964129092', '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Елена Блиновская «Расширение финансового сознания»',
   'https://info0cean.com/wp-content/uploads/2018/10/3-Elena-Blinovskaya-Marafon-zhelanij-i-rasshirenie-finansovogo-soznaniya-2018-02.jpg',
   'https://info0cean.com/elena-blinovskaya-marafon-zhelanij-i-rasshirenie-finansovogo-soznaniya-2018-audio/?=shark',
   'https://fex.net/629153592975', '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values ('PIMEN «Криптотрейдинг для начинающих»',
                                                                    'https://info0cean.com/wp-content/uploads/2018/10/1.-Mihail-Pimenov-Kriptotrejding-dlya-nachinayushhih-1-1.png',
                                                                    'https://info0cean.com/pimen-kriptotrejding-dlya-nachinayushhih/?=shark',
                                                                    'https://fex.net/910960108085',
                                                                    '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Школа трейдинга Harvest «»', 'https://info0cean.com/wp-content/uploads/2018/10/2.-SHkola-trejdinga-HARVEST.jpg',
   'https://info0cean.com/shkola-trejdinga-harvest/?=shark', 'https://fex.net/221408339778',
   '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Артём Сребный «Курс по трейдингу»', 'https://info0cean.com/wp-content/uploads/2018/10/all-platform-1.png',
   'https://info0cean.com/artyom-srebnyj-kurs-po-trejdingu-dlya-novichka-s-nulya-bazovyj-1-0-2018/?=shark',
   'https://fex.net/224783182394', '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Радислав Гандапас «Идеология Лидера»', 'https://info0cean.com/wp-content/uploads/2018/10/1-ideologiya-lidera.jpg',
   'https://info0cean.com/radislav-gandapas-ideologiya-lidera/?=shark', 'https://fex.net/573211185707',
   '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values ('Радислав Гандапас «Алгоритмы успеха»',
                                                                    'https://info0cean.com/wp-content/uploads/2018/10/5-kartinka-k-video-Algoritmy-Uspeha-02.jpg',
                                                                    'https://info0cean.com/radislav-gandapas-algoritmy-uspeha/?=shark',
                                                                    'https://fex.net/326857245882',
                                                                    '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values ('Радислав Гандапас «Скрипты успеха»',
                                                                    'https://info0cean.com/wp-content/uploads/2018/10/3-Radislav-Gandapas-Skripty-uspeha-02.jpg',
                                                                    'https://info0cean.com/radislav-gandapas-skripty-uspeha/?=shark',
                                                                    'https://fex.net/821624238530',
                                                                    '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values ('Радислав Гандапас «Формула мотивации»',
                                                                    'https://info0cean.com/wp-content/uploads/2018/10/6-kartinka-k-video-Formula-motivatsii-02.jpg',
                                                                    'https://info0cean.com/radislav-gandapas-formula-motivatsii/?=shark',
                                                                    'https://fex.net/032964308151',
                                                                    '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Радислав Гандапас «Полноценная жизнь как бизнес-проект»',
   'https://info0cean.com/wp-content/uploads/2018/10/5-Radislav-Gandapas-Polnotsennaya-zhizn-kak-biznes-proekt-02.jpg',
   'https://info0cean.com/radislav-gandapas-polnotsennaya-zhizn-kak-biznes-proekt/?=shark',
   'https://fex.net/156191309113', '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values ('Армен Геворкян «Интенсив-курс по криптотрейдингу»',
                                                                    'https://info0cean.com/wp-content/uploads/2018/10/1.-Armen-Gevorkyan-Intensiv-kurs-po-profitnomu-kriptotrejdingu-.jpeg',
                                                                    'https://info0cean.com/armen-gevorkyan-intensiv-kurs-po-profitnomu-kriptotrejdingu-2018/?=shark',
                                                                    'https://fex.net/825676585585',
                                                                    '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values ('TSI Analytics Group «Трейдинг на криптовалютах»',
                                                                    'https://info0cean.com/wp-content/uploads/2018/10/2.-TSI-Analytics-Group-Trejding-na-kriptovalyutah-volnovoj-podhod-2018.jpg',
                                                                    'https://info0cean.com/tsi-analytics-group-trejding-na-kriptovalyutah-volnovoj-podhod-2018/?=shark',
                                                                    'https://fex.net/492985930782',
                                                                    '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values ('Дмитрий Карпиловский «Криптобизнес 3.0»',
                                                                    'https://info0cean.com/wp-content/uploads/2018/10/3.-Dmitrij-Karpilovskij-Kriptobiznes-3.0-2017.jpg',
                                                                    'https://info0cean.com/dmitrij-karpilovskij-kriptobiznes-3-0-2017/?=shark',
                                                                    'https://fex.net/162264662825',
                                                                    '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Трейдинг от А до Я с Александром Герчиком за 60 дней «»',
   'https://info0cean.com/wp-content/uploads/2018/10/4.-Trejding-ot-A-do-YA-s-Aleksandrom-Gerchikom-za-60-dnej.jpg',
   'https://info0cean.com/trejding-ot-a-do-ya-s-aleksandrom-gerchikom-za-60-dnej/?=shark',
   'https://fex.net/539487774625', '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values ('HAMAHA «Обучение торговле криптовалютами»',
                                                                    'https://info0cean.com/wp-content/uploads/2018/10/5.-HAMAHA-Obuchenie-torgovle-kriptovalyutami.jpg',
                                                                    'https://info0cean.com/hamaha-obuchenie-torgovle-kriptovalyutami/?=shark',
                                                                    'https://fex.net/696049335132',
                                                                    '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values ('Мария Солодар «Школа интернет-маркетологов»',
                                                                    'https://info0cean.com/wp-content/uploads/2018/10/1.-Mariya-Solodar-SHkola-Internet-Marketologov.jpg',
                                                                    'https://info0cean.com/mariya-solodar-2-h-mesyachnaya-shkola-internet-marketologov/?=shark',
                                                                    '#', '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values ('Алексей Лукьянов «Денежный Ютубер Пакет Практик»',
                                                                    'https://info0cean.com/wp-content/uploads/2018/10/photo_2018-10-21_04-00-48.jpg',
                                                                    'https://info0cean.com/aleksej-lukyanov-denezhnyj-yutuber-paket-praktik/?=shark',
                                                                    '#', '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Skillbox + Ingate «Практический интенсивный курс SMM менеджер»',
   'https://info0cean.com/wp-content/uploads/2018/10/3-SkillboxIngate-Prakticheskij-intensivnyj-kurs-SMM-menedzher-2.jpg',
   'https://info0cean.com/skillbox-ingate-prakticheskij-intensivnyj-kurs-smm-menedzher-2018/?=shark', '#',
   '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values ('SEOman «SEO-специалист с нуля Пакет Premium»',
                                                                    'https://info0cean.com/wp-content/uploads/2018/10/4.-SEOman-SEO-spetsialist-s-nulya-za-2-mesyatsa-.jpg',
                                                                    'https://info0cean.com/seoman-seo-spetsialist-s-nulya-za-2-mesyatsa-paket-premium-2018/?=shark',
                                                                    '#', '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Алексей Князев «Курс по настройке таргетинговой рекламы»',
   'https://info0cean.com/wp-content/uploads/2018/10/5.-Aleksej-Knyazev-Samyj-effektivnyj-kurs-po-nastrojke-targetingovoj-reklamy-18-potok-768x432.jpg',
   'https://info0cean.com/aleksej-knyazev-samyj-effektivnyj-kurs-po-nastrojke-targetingovoj-reklamy-18-potok/?=shark',
   '#', '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Cryptominds «Курсы по Блокчейну и Криптоторговле (сборник) 2018»',
   'https://info0cean.com/wp-content/uploads/2018/10/1-Cryptominds-Kursy-po-Blokchejnu-i-Kriptotorgovle-01.png',
   'https://info0cean.com/cryptominds-kursy-po-blokchejnu-i-kriptotorgovle-2018-sbornik/?=shark',
   'https://fex.net/523406588211', '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Ethereum Works «Разработка на блокчейне Ethereum Базовый курс 2018»',
   'https://info0cean.com/wp-content/uploads/2018/10/shkola_blokcheyn_razrabotki_ethereumworks.png',
   'https://info0cean.com/ethereum-works-razrabotka-na-blokchejne-ethereum-bazovyj-kurs-2018-2/?=shark', '#',
   '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Сергей Атрощенко «Манипуляции. Нарушение теханализа»',
   'https://info0cean.com/wp-content/uploads/2018/10/3.-Sergej-Atroshhenko.-Sekrety-Manipulyatsij.-Narushenie-tehanaliza.jpg',
   'https://info0cean.com/ethereum-works-razrabotka-na-blokchejne-ethereum-bazovyj-kurs-2018/?=shark', '#',
   '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Форекс и инвестиции «Техника прибыли Price Action»',
   'https://info0cean.com/wp-content/uploads/2018/10/4.-Foreks-i-investitsii-Price-Action.Tehnika-pribyli-.jpg',
   'https://info0cean.com/foreks-i-investitsii-price-action-tehnika-pribyli/?=shark', '#', '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Елена Калашникова (Estrader) «Профиль рынка Price Action»',
   'https://info0cean.com/wp-content/uploads/2018/10/5.jpg',
   'https://info0cean.com/elena-kalashnikova-estrader-profil-rynka-price-action/?=shark', '#',
   '2018-11-01T07:52:15.978Z');


insert into course (title, image, about, download, created) values
  ('Евгений Кузнецов «Интенсив по управлению временем»',
   'https://info0cean.com/wp-content/uploads/2018/10/1-upravlenie-vremenem.jpg',
   'https://info0cean.com/evgenij-kuznetsov-intensiv-po-upravleniyu-vremenem/?=white', 'https://fex.net/332973651941',
   '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Максим Чернов «Успех через связи: видеокурс по нетворкингу»',
   'https://info0cean.com/wp-content/uploads/2018/10/2.-Maksim-CHernov-Uspeh-cherez-svyazi-videokurs-po-netvorkingu-.png',
   'https://info0cean.com/maksim-chernov-uspeh-cherez-svyazi-videokurs-po-netvorkingu/?=white',
   'https://fex.net/838964129092', '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Елена Блиновская «Расширение финансового сознания»',
   'https://info0cean.com/wp-content/uploads/2018/10/3-Elena-Blinovskaya-Marafon-zhelanij-i-rasshirenie-finansovogo-soznaniya-2018-02.jpg',
   'https://info0cean.com/elena-blinovskaya-marafon-zhelanij-i-rasshirenie-finansovogo-soznaniya-2018-audio/?=white',
   'https://fex.net/629153592975', '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values ('PIMEN «Криптотрейдинг для начинающих»',
                                                                    'https://info0cean.com/wp-content/uploads/2018/10/1.-Mihail-Pimenov-Kriptotrejding-dlya-nachinayushhih-1-1.png',
                                                                    'https://info0cean.com/pimen-kriptotrejding-dlya-nachinayushhih/?=white',
                                                                    'https://fex.net/910960108085',
                                                                    '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Школа трейдинга Harvest «»', 'https://info0cean.com/wp-content/uploads/2018/10/2.-SHkola-trejdinga-HARVEST.jpg',
   'https://info0cean.com/shkola-trejdinga-harvest/?=white', 'https://fex.net/221408339778',
   '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Артём Сребный «Курс по трейдингу»', 'https://info0cean.com/wp-content/uploads/2018/10/all-platform-1.png',
   'https://info0cean.com/artyom-srebnyj-kurs-po-trejdingu-dlya-novichka-s-nulya-bazovyj-1-0-2018/?=white',
   'https://fex.net/224783182394', '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Радислав Гандапас «Идеология Лидера»', 'https://info0cean.com/wp-content/uploads/2018/10/1-ideologiya-lidera.jpg',
   'https://info0cean.com/radislav-gandapas-ideologiya-lidera/?=white', 'https://fex.net/573211185707',
   '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values ('Радислав Гандапас «Алгоритмы успеха»',
                                                                    'https://info0cean.com/wp-content/uploads/2018/10/5-kartinka-k-video-Algoritmy-Uspeha-02.jpg',
                                                                    'https://info0cean.com/radislav-gandapas-algoritmy-uspeha/?=white',
                                                                    'https://fex.net/326857245882',
                                                                    '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values ('Радислав Гандапас «Скрипты успеха»',
                                                                    'https://info0cean.com/wp-content/uploads/2018/10/3-Radislav-Gandapas-Skripty-uspeha-02.jpg',
                                                                    'https://info0cean.com/radislav-gandapas-skripty-uspeha/?=white',
                                                                    'https://fex.net/821624238530',
                                                                    '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values ('Радислав Гандапас «Формула мотивации»',
                                                                    'https://info0cean.com/wp-content/uploads/2018/10/6-kartinka-k-video-Formula-motivatsii-02.jpg',
                                                                    'https://info0cean.com/radislav-gandapas-formula-motivatsii/?=white',
                                                                    'https://fex.net/032964308151',
                                                                    '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Радислав Гандапас «Полноценная жизнь как бизнес-проект»',
   'https://info0cean.com/wp-content/uploads/2018/10/5-Radislav-Gandapas-Polnotsennaya-zhizn-kak-biznes-proekt-02.jpg',
   'https://info0cean.com/radislav-gandapas-polnotsennaya-zhizn-kak-biznes-proekt/?=white',
   'https://fex.net/156191309113', '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values ('Армен Геворкян «Интенсив-курс по криптотрейдингу»',
                                                                    'https://info0cean.com/wp-content/uploads/2018/10/1.-Armen-Gevorkyan-Intensiv-kurs-po-profitnomu-kriptotrejdingu-.jpeg',
                                                                    'https://info0cean.com/armen-gevorkyan-intensiv-kurs-po-profitnomu-kriptotrejdingu-2018/?=white',
                                                                    'https://fex.net/825676585585',
                                                                    '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values ('TSI Analytics Group «Трейдинг на криптовалютах»',
                                                                    'https://info0cean.com/wp-content/uploads/2018/10/2.-TSI-Analytics-Group-Trejding-na-kriptovalyutah-volnovoj-podhod-2018.jpg',
                                                                    'https://info0cean.com/tsi-analytics-group-trejding-na-kriptovalyutah-volnovoj-podhod-2018/?=white',
                                                                    'https://fex.net/492985930782',
                                                                    '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values ('Дмитрий Карпиловский «Криптобизнес 3.0»',
                                                                    'https://info0cean.com/wp-content/uploads/2018/10/3.-Dmitrij-Karpilovskij-Kriptobiznes-3.0-2017.jpg',
                                                                    'https://info0cean.com/dmitrij-karpilovskij-kriptobiznes-3-0-2017/?=white',
                                                                    'https://fex.net/162264662825',
                                                                    '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Трейдинг от А до Я с Александром Герчиком за 60 дней «»',
   'https://info0cean.com/wp-content/uploads/2018/10/4.-Trejding-ot-A-do-YA-s-Aleksandrom-Gerchikom-za-60-dnej.jpg',
   'https://info0cean.com/trejding-ot-a-do-ya-s-aleksandrom-gerchikom-za-60-dnej/?=white',
   'https://fex.net/539487774625', '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values ('HAMAHA «Обучение торговле криптовалютами»',
                                                                    'https://info0cean.com/wp-content/uploads/2018/10/5.-HAMAHA-Obuchenie-torgovle-kriptovalyutami.jpg',
                                                                    'https://info0cean.com/hamaha-obuchenie-torgovle-kriptovalyutami/?=white',
                                                                    'https://fex.net/696049335132',
                                                                    '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values ('Мария Солодар «Школа интернет-маркетологов»',
                                                                    'https://info0cean.com/wp-content/uploads/2018/10/1.-Mariya-Solodar-SHkola-Internet-Marketologov.jpg',
                                                                    'https://info0cean.com/mariya-solodar-2-h-mesyachnaya-shkola-internet-marketologov/?=white',
                                                                    '#', '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values ('Алексей Лукьянов «Денежный Ютубер Пакет Практик»',
                                                                    'https://info0cean.com/wp-content/uploads/2018/10/photo_2018-10-21_04-00-48.jpg',
                                                                    'https://info0cean.com/aleksej-lukyanov-denezhnyj-yutuber-paket-praktik/?=white',
                                                                    '#', '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Skillbox + Ingate «Практический интенсивный курс SMM менеджер»',
   'https://info0cean.com/wp-content/uploads/2018/10/3-SkillboxIngate-Prakticheskij-intensivnyj-kurs-SMM-menedzher-2.jpg',
   'https://info0cean.com/skillbox-ingate-prakticheskij-intensivnyj-kurs-smm-menedzher-2018/?=white', '#',
   '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values ('SEOman «SEO-специалист с нуля Пакет Premium»',
                                                                    'https://info0cean.com/wp-content/uploads/2018/10/4.-SEOman-SEO-spetsialist-s-nulya-za-2-mesyatsa-.jpg',
                                                                    'https://info0cean.com/seoman-seo-spetsialist-s-nulya-za-2-mesyatsa-paket-premium-2018/?=white',
                                                                    '#', '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Алексей Князев «Курс по настройке таргетинговой рекламы»',
   'https://info0cean.com/wp-content/uploads/2018/10/5.-Aleksej-Knyazev-Samyj-effektivnyj-kurs-po-nastrojke-targetingovoj-reklamy-18-potok-768x432.jpg',
   'https://info0cean.com/aleksej-knyazev-samyj-effektivnyj-kurs-po-nastrojke-targetingovoj-reklamy-18-potok/?=white',
   '#', '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Cryptominds «Курсы по Блокчейну и Криптоторговле (сборник) 2018»',
   'https://info0cean.com/wp-content/uploads/2018/10/1-Cryptominds-Kursy-po-Blokchejnu-i-Kriptotorgovle-01.png',
   'https://info0cean.com/cryptominds-kursy-po-blokchejnu-i-kriptotorgovle-2018-sbornik/?=white',
   'https://fex.net/523406588211', '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Ethereum Works «Разработка на блокчейне Ethereum Базовый курс 2018»',
   'https://info0cean.com/wp-content/uploads/2018/10/shkola_blokcheyn_razrabotki_ethereumworks.png',
   'https://info0cean.com/ethereum-works-razrabotka-na-blokchejne-ethereum-bazovyj-kurs-2018-2/?=white', '#',
   '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Сергей Атрощенко «Манипуляции. Нарушение теханализа»',
   'https://info0cean.com/wp-content/uploads/2018/10/3.-Sergej-Atroshhenko.-Sekrety-Manipulyatsij.-Narushenie-tehanaliza.jpg',
   'https://info0cean.com/ethereum-works-razrabotka-na-blokchejne-ethereum-bazovyj-kurs-2018/?=white', '#',
   '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Форекс и инвестиции «Техника прибыли Price Action»',
   'https://info0cean.com/wp-content/uploads/2018/10/4.-Foreks-i-investitsii-Price-Action.Tehnika-pribyli-.jpg',
   'https://info0cean.com/foreks-i-investitsii-price-action-tehnika-pribyli/?=white', '#', '2018-11-01T07:52:15.978Z');
insert into course (title, image, about, download, created) values
  ('Елена Калашникова (Estrader) «Профиль рынка Price Action»',
   'https://info0cean.com/wp-content/uploads/2018/10/5.jpg',
   'https://info0cean.com/elena-kalashnikova-estrader-profil-rynka-price-action/?=white', '#',
   '2018-11-01T07:52:15.978Z');


insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/evgenij-kuznetsov-intensiv-po-upravleniyu-vremenem/?=shark'),
                                                     19);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/maksim-chernov-uspeh-cherez-svyazi-videokurs-po-netvorkingu/?=shark'),
                                                     19);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/elena-blinovskaya-marafon-zhelanij-i-rasshirenie-finansovogo-soznaniya-2018-audio/?=shark'),
                                                     19);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/pimen-kriptotrejding-dlya-nachinayushhih/?=shark'),
                                                     19);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/shkola-trejdinga-harvest/?=shark'),
                                                     19);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/artyom-srebnyj-kurs-po-trejdingu-dlya-novichka-s-nulya-bazovyj-1-0-2018/?=shark'),
                                                     19);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/radislav-gandapas-ideologiya-lidera/?=shark'),
                                                     19);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/radislav-gandapas-algoritmy-uspeha/?=shark'),
                                                     19);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/radislav-gandapas-skripty-uspeha/?=shark'),
                                                     19);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/radislav-gandapas-formula-motivatsii/?=shark'),
                                                     19);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/radislav-gandapas-polnotsennaya-zhizn-kak-biznes-proekt/?=shark'),
                                                     19);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/armen-gevorkyan-intensiv-kurs-po-profitnomu-kriptotrejdingu-2018/?=shark'),
                                                     19);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/tsi-analytics-group-trejding-na-kriptovalyutah-volnovoj-podhod-2018/?=shark'),
                                                     19);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/dmitrij-karpilovskij-kriptobiznes-3-0-2017/?=shark'),
                                                     19);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/trejding-ot-a-do-ya-s-aleksandrom-gerchikom-za-60-dnej/?=shark'),
                                                     19);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/hamaha-obuchenie-torgovle-kriptovalyutami/?=shark'),
                                                     19);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/mariya-solodar-2-h-mesyachnaya-shkola-internet-marketologov/?=shark'),
                                                     19);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/aleksej-lukyanov-denezhnyj-yutuber-paket-praktik/?=shark'),
                                                     19);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/skillbox-ingate-prakticheskij-intensivnyj-kurs-smm-menedzher-2018/?=shark'),
                                                     19);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/seoman-seo-spetsialist-s-nulya-za-2-mesyatsa-paket-premium-2018/?=shark'),
                                                     19);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/aleksej-knyazev-samyj-effektivnyj-kurs-po-nastrojke-targetingovoj-reklamy-18-potok/?=shark'),
                                                     19);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/cryptominds-kursy-po-blokchejnu-i-kriptotorgovle-2018-sbornik/?=shark'),
                                                     19);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/ethereum-works-razrabotka-na-blokchejne-ethereum-bazovyj-kurs-2018-2/?=shark'),
                                                     19);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/ethereum-works-razrabotka-na-blokchejne-ethereum-bazovyj-kurs-2018/?=shark'),
                                                     19);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/foreks-i-investitsii-price-action-tehnika-pribyli/?=shark'),
                                                     19);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/elena-kalashnikova-estrader-profil-rynka-price-action/?=shark'),
                                                     19);


insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/evgenij-kuznetsov-intensiv-po-upravleniyu-vremenem/?=white'),
                                                     17);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/maksim-chernov-uspeh-cherez-svyazi-videokurs-po-netvorkingu/?=white'),
                                                     17);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/elena-blinovskaya-marafon-zhelanij-i-rasshirenie-finansovogo-soznaniya-2018-audio/?=white'),
                                                     17);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/pimen-kriptotrejding-dlya-nachinayushhih/?=white'),
                                                     17);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/shkola-trejdinga-harvest/?=white'),
                                                     17);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/artyom-srebnyj-kurs-po-trejdingu-dlya-novichka-s-nulya-bazovyj-1-0-2018/?=white'),
                                                     17);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/radislav-gandapas-ideologiya-lidera/?=white'),
                                                     17);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/radislav-gandapas-algoritmy-uspeha/?=white'),
                                                     17);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/radislav-gandapas-skripty-uspeha/?=white'),
                                                     17);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/radislav-gandapas-formula-motivatsii/?=white'),
                                                     17);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/radislav-gandapas-polnotsennaya-zhizn-kak-biznes-proekt/?=white'),
                                                     17);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/armen-gevorkyan-intensiv-kurs-po-profitnomu-kriptotrejdingu-2018/?=white'),
                                                     17);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/tsi-analytics-group-trejding-na-kriptovalyutah-volnovoj-podhod-2018/?=white'),
                                                     17);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/dmitrij-karpilovskij-kriptobiznes-3-0-2017/?=white'),
                                                     17);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/trejding-ot-a-do-ya-s-aleksandrom-gerchikom-za-60-dnej/?=white'),
                                                     17);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/hamaha-obuchenie-torgovle-kriptovalyutami/?=white'),
                                                     17);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/mariya-solodar-2-h-mesyachnaya-shkola-internet-marketologov/?=white'),
                                                     17);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/aleksej-lukyanov-denezhnyj-yutuber-paket-praktik/?=white'),
                                                     17);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/skillbox-ingate-prakticheskij-intensivnyj-kurs-smm-menedzher-2018/?=white'),
                                                     17);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/seoman-seo-spetsialist-s-nulya-za-2-mesyatsa-paket-premium-2018/?=white'),
                                                     17);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/aleksej-knyazev-samyj-effektivnyj-kurs-po-nastrojke-targetingovoj-reklamy-18-potok/?=white'),
                                                     17);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/cryptominds-kursy-po-blokchejnu-i-kriptotorgovle-2018-sbornik/?=white'),
                                                     17);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/ethereum-works-razrabotka-na-blokchejne-ethereum-bazovyj-kurs-2018-2/?=white'),
                                                     17);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/ethereum-works-razrabotka-na-blokchejne-ethereum-bazovyj-kurs-2018/?=white'),
                                                     17);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/foreks-i-investitsii-price-action-tehnika-pribyli/?=white'),
                                                     17);
insert into course_program (course, program) values ((select id
                                                      from course
                                                      where about =
                                                            'https://info0cean.com/elena-kalashnikova-estrader-profil-rynka-price-action/?=white'),
                                                     17);
