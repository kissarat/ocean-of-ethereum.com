<?php
/**
 * Last modified: 18.06.13 16:27:19
 * Hash: 5e643d81a2296f41678e94d08256edc7758a198a
 */

$vars = [];
$filename = null;

foreach ($argv as $param) {
    $param = explode('=', $param);
    if (count($param) > 1) {
        $vars[$param[0]] = $param[1];
    }
    else {
        $filename = $param[0];
    }
}

$string = file_get_contents($filename);

foreach ($vars as $name => $value) {
    $string = str_ireplace($name, $value, $string);
}

echo $string;
