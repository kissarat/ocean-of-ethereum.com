-- Last modified: 19.01.16 09:30:10
-- Hash: 16fc5d86e7ed4e1b43d474ffa9f956c71a724954

INSERT INTO "user" (nick, email) SELECT
                                   'test' || n,
                                   'test' || n || '@mail.local'
                                 FROM generate_series(1, 100) n;

INSERT INTO node ("user", program) VALUES (1, 31);

SELECT max(id)
FROM node;

INSERT INTO node ("user", program, parent, cell)
  WITH a AS (
      SELECT
        "user",
        n.program,
        n.id
      FROM opened_node o
        JOIN node n ON n.id = o.id
      WHERE n.program = 12
      ORDER BY count DESC, id
  )
  SELECT
    7,
    a.program,
    a.id,
    s
  FROM a
    CROSS JOIN generate_series(0, 1) s;

DELETE FROM node
WHERE id > 100000 AND id IN (SELECT id
                             FROM node
                             ORDER BY id DESC);

alter table "user" add column last_activity smallint not null default 0;
