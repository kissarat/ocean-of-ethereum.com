-- Last modified: 18.11.09 01:31:33
-- Hash: f122c04bf66afe655e48562719250ae64a9c4c4a

CREATE OR REPLACE VIEW "token_user" as
  select
    t.created,
    t.id,
    t.ip,
    t.time,
    t.type,
    t.expires,
    u.email,
    u.nick,
    u.admin,
    u.avatar,
    u.parent,
    row_to_json(u.*) as "user"
  from token t
    left join "user" u on t.user = u.id;

CREATE OR REPLACE VIEW "user_about" as
  select
    id,
    nick,
    forename,
    surname,
    email,
    skype
  from "user";

CREATE OR REPLACE VIEW "transfer_user" as
  select
    tr.id,
    tr.amount,
    tr.type,
    tr.status,
    tr.wallet,
    tr.ip,
    tr.created,
    tr.time,
    tr."from",
    tr."to",
    tr.currency,
    tr.txid,
    f.nick as from_nick,
    t.nick as to_nick
  from transfer tr
    left join "user" f on f.id = tr."from"
    left join "user" t on t.id = tr."to";

CREATE OR REPLACE VIEW "user_transfer" as
  select
    *,
    case when type in ('buy', 'withdraw')
      then "from"
    else "to" end as "user"
  from transfer;

create view "balance" as
  with
      a as (
      select
        "to" as "user",
        currency,
        amount
      from transfer it
      where "to" is not null and it.status = 'success'
      union all
      select
        "from" as "user",
        currency,
        -amount
      from transfer ot
      where "from" is not null and ot.status = 'success'
    ),
      b as (
        select
          a."user",
          currency,
          sum(amount) as amount
        from a
        group by a."user", currency
    )
  select
    u.id                  as "user",
    currency,
    coalesce(b.amount, 0) as amount
  from b
    right join "user" u on b."user" = u.id;

CREATE OR REPLACE VIEW "referral" as
  with recursive r(id, "nick", parent, email, root, level, forename, surname, skype, telegram, avatar, created) as (
    select
      id,
      nick,
      parent,
      email,
      id as root,
      0  as level,
      forename,
      surname,
      skype,
      telegram,
      avatar,
      created
    from "user"
    union
    select
      u.id,
      u.nick,
      u.parent,
      u.email,
      r.root,
      r.level + 1 as level,
      u.forename,
      u.surname,
      u.skype,
      u.telegram,
      u.avatar,
      u.created
    from "user" u
      join r on u.parent = r.id
    where r.level < 3
  )
  select
    id,
    nick,
    parent,
    email,
    root,
    level,
    forename,
    surname,
    skype,
    telegram,
    avatar,
    created
  from r
  order by root, level, id;

CREATE OR REPLACE VIEW "structure" as
  with n as (
      select
        "user",
        program,
        max(id) as node
      from node
      group by "user", program
  )
  select
    id,
    nick,
    parent,
    email,
    forename,
    surname,
    skype,
    telegram,
    avatar,
    created,
    (select count(*)
     from "user" c
     where c.parent = r.id) as children,
    (select json_object_agg(n.program, n.node)
     from n
     where n."user" = r.id) as nodes
  from "user" r;

create view "finance" as
  select
    t."user",
    t.type,
    t.currency,
    sum(t.amount) as amount
  from user_transfer t
  where t.status = 'success'
  group by t."user", t.type, t.currency;

CREATE OR REPLACE VIEW "informer" as
  select
    u.id,
    (select count(*)
     from "visit" v
     where v.url = '/ref/' || u.nick)                as visit,
    (select count(*)
     from "user" r
     where r.parent = u.id)                          as invited,
    (select count(*)
     from referral
     where root = u.id and level > 0)                as referral,
    (select count(*)
     from node n
       join "user" c on n."user" = c.id
     where c.parent = u.id)                          as node,
    extract(days from CURRENT_TIMESTAMP - u.created) as day,
    u.nick,
    u.type,
    u.surname,
    u.forename,
    u.email,
    u.avatar,
    u.skype,
    u.phone,
    u.country,
    u.settlement,
    u.address,
    to_json(p.*)                                     as parent
  from "user" u
    left join user_about p on u.parent = p.id;

CREATE OR REPLACE VIEW "top_referrals" as
  select
    replace(url, '/ref/', '') as nick,
    "count"
  from (select
          url,
          count(*)
        from visit
        where url like '/ref/%'
        group by url) t
  where "count" > 1
  order by "count" desc;

CREATE OR REPLACE VIEW "consolidated_transfer" as
  select
    type,
    status,
    amount
  from transfer t;

CREATE OR REPLACE VIEW "consolidated_balance" as
  select amount
  from balance;

CREATE OR REPLACE VIEW "statistics" as
  select
    (select sum(amount)
     from consolidated_transfer
     where type = 'payment' and status = 'success')                        as payment,
    coalesce((select sum(amount)
              from consolidated_transfer
              where type = 'payment' and status =
                                         'pending'), 0)                    as pending,
    (select sum(amount)
     from consolidated_transfer
     where type = 'withdraw' and status =
                                 'success')                                as withdraw,
    (select sum(amount)
     from consolidated_transfer
     where type = 'accrue' and status =
                               'success')                                  as accrue,
    (select sum(amount)
     from consolidated_transfer
     where type = 'buy' and status =
                            'success')                                     as buy,
    (select count(*)
     from "user")                                                          as users,
    (select count(*)
     from "user" ud
     where ud.created > CURRENT_TIMESTAMP -
                        interval '1 day')                                  as users_day,
    (select count(*)
     from "token"
     where type = 'browser' and expires > CURRENT_TIMESTAMP)               as visitors,
    (select count(*)
     from "token"
     where type = 'browser' and expires > CURRENT_TIMESTAMP
           and handshake > CURRENT_TIMESTAMP -
                           interval '1 day')                               as visitors_day,
    (select count(*)
     from "token"
     where type = 'browser' and expires > CURRENT_TIMESTAMP
           and handshake > CURRENT_TIMESTAMP -
                           interval '1 day')                               as new_visitors_day,
    (select count(*)
     from log
     where entity = 'handshake' and action = 'update'
           and to_timestamp(id / (1000 * 1000 * 1000)) > CURRENT_TIMESTAMP -
                                                         interval '1 day') as activities_day,
    (select count(*)
     from visit
     where url like '/ref/%' and visit.created > CURRENT_TIMESTAMP -
                                                 interval '1 day')         as referral_day,
    (select sum(amount)
     from consolidated_balance)                                            as balance,
    NOW()                                                                  as "time";

CREATE OR REPLACE VIEW "withdrawable" as
  select
    t.id,
    (case when b.amount - t.amount >= 0
      then 'success'
     else 'canceled' end) as will,
    t.amount,
    t.wallet,
    t."from",
    u.nick                as "from_nick",
    b.amount              as balance,
    t.txid,
    b.amount - t.amount   as remain,
    t.ip,
    t.text,
    t.vars,
    t.system,
    t.created
  from transfer t
    join balance b on t."from" = b."user" and t.currency = b.currency
    join "user" u on t."from" = u.id
  where t.type = 'withdraw' and t.status = 'created' and t.wallet is not null
        and t.txid is null and t.node is null and t.amount > 0 and t.ip is not null;

create materialized view "program_view" as
  with a as (
      select
        *,
        id % 2 = 0 as finish,
        case when id % 2 = 0
          then 3
        else 4 end as level
      from program)
  select
    *,
    (select sum(power(2, s - 1))
     from generate_series(1, level) s) as size
  from a;

CREATE OR REPLACE VIEW "refund" as
  select
    p2.id,
    p2.price,
    p2.name,
    case when p2.price is null
      then
        p1.price * 4 - coalesce(p3.price, 0)
    else p2.price end                as refund,
    coalesce(p2.price, p1.price * 4) as cell
  from program p2
    left join program p1 on p1.id = p2.id - 1
    left join program p3 on p3.id = p2.id + 1;

CREATE OR REPLACE VIEW "matrix" as
  with recursive r(program, root, id, parent, "user", level, own, cell) as (
    select
      program,
      id as root,
      id,
      parent,
      "user",
      0  as "level",
      0  as own,
      cell
    from node
    union
    select
      n.program,
      r.root,
      n.id,
      n.parent,
      n."user",
      r.level + 1  as "level",
      (case when r.level >= p.level - 2
        then 1
       else 0 end) as own,
      n.cell
    from node n
      join r on r.id = n.parent
      join program_view "p" on r.program = p.id
    where r.level + 1 < p.level
  )
  select r.*
  from r;

create or replace view "opened_node" as
  with t as (
      select
        p.id,
        count(c.*) as count
      from node p
        left join node c on c.parent = p.id
      group by p.id
  )
  select *
  from t
  where count < 2;

CREATE OR REPLACE VIEW "matrix_level" as
  with a as (
      select
        root,
        level,
        program,
        count(*) as count
      from matrix
      group by root, program, level
  )
  select
    a.*,
    count < power(2, level) as active
  from a;

CREATE OR REPLACE VIEW "free_level" as
  select
    root,
    max(m.level) + 1 as level
  from matrix_level m
    join program_view "p" on m.program = p.id
  where not m.active
  group by root;

CREATE OR REPLACE VIEW "free_parent" as
  select
    m.*,
    o.count
  from matrix m
    join free_level l on m.root = l.root and l.level - 1 = m.level
    join opened_node o on m.id = o.id;

CREATE OR REPLACE VIEW "qualification" as
  select
    nq.id,
    nq.program,
    nq."user",
    nq.parent,
    nq.qualification,
    nq.cell,
    nq.time,
    o.origin,
    o.id     as offer,
    least(coalesce(nq.qualified, (select count(*)
                                  from node n
                                  where n.qualification = o.origin)),
          2) as qualified
  from node nq left join offer o on nq.id = o.used;

CREATE OR REPLACE VIEW "offer_qualification" as
  select
    o.id,
    p.id                                                                as origin,
    o.used,
    o.program,
    least(coalesce(n.qualified, (select count(*)
                                 from node n
                                 where n.qualification = o.origin)), 2) as qualified
  from node p
    join offer o on p.id = o.origin
    left join node n on o.used = n.id;

CREATE OR REPLACE VIEW "matrix_count" as
  with a as (
      select
        root            as id,
        count(*) :: int as count,
        sum(own)        as filled
      from matrix m
      group by root
  )
  select
    a.id,
    a.count,
    a.filled,
    (a.count - a.filled) >= power(2, p.level - 1) - 1 as opened,
    a.count >= power(2, p.level) - 1                  as closed,
    n.program,
    n."user",
    n.qualified,
    n.origin,
    n.offer
  from a
    join qualification n on a.id = n.id
    join program_view "p" on n.program = p.id;

CREATE OR REPLACE VIEW "matrix_node" as
  select
    m.id,
    m.count,
    m.program,
    m.qualified,
    not m.closed as active,
    m.origin,
    m.offer,
    m."user",
    u.nick
  from matrix_count m
    join "user" u on m."user" = u.id
  where m.opened;

CREATE OR REPLACE VIEW "transfer_day" AS
  SELECT
    currency,
    type,
    sum(amount) AS amount
  FROM transfer
  WHERE created > CURRENT_TIMESTAMP - INTERVAL '1 day' AND status = 'success'
  GROUP BY currency, type;

CREATE OR REPLACE VIEW "transfer_week" AS
  SELECT
    currency,
    type,
    sum(amount) AS amount
  FROM transfer
  WHERE created > CURRENT_TIMESTAMP - INTERVAL '1 week' AND status = 'success'
  GROUP BY currency, type;

CREATE OR REPLACE VIEW "transfer_statistics" AS
  SELECT
    currency,
    type,
    sum(amount) AS amount
  FROM transfer
  WHERE status = 'success'
  GROUP BY currency, type;

CREATE OR REPLACE VIEW "all_balance" AS
  WITH a AS (
    SELECT
      "to" AS "user",
      "currency",
      amount
    FROM transfer
    WHERE "to" IS NOT NULL AND status = 'success'
    UNION ALL
    SELECT
      "from"  AS "user",
      "currency",
      -amount AS amount
    FROM transfer
    WHERE "from" IS NOT NULL AND status = 'success'
  )
  SELECT
    "user",
    "currency",
    sum("amount") as "amount"
  FROM a
  GROUP BY "user", "currency";

CREATE OR REPLACE VIEW "user_log" AS
  SELECT
    l.id,
    l."user",
    u.nick,
    l.entity,
    l.action,
    l.ip,
    l.data
  FROM log l LEFT JOIN "user" u ON l."user" = u.id;

CREATE OR REPLACE VIEW "admin_user" as
  select
    u.id,
    u.nick,
    u.type,
    u.admin,
    u.surname,
    u.forename,
    u.skype,
    u.avatar,
    u.email,
    u.parent,
    u.swap_count,
    s.nick                                                    as sponsor,
    coalesce((select count(*) :: int
              from "user" c
              where c.parent = u.id), 0)                      as children,
    (SELECT json_agg(row_to_json(t.*))
     FROM (SELECT
             m.id,
             m.program,
             p.name
           FROM matrix_count m
             JOIN program p ON m.program = p.id
           WHERE m."user" = u.id AND NOT closed
           ORDER BY m.program, m.id) t)                       as nodes,
    (select count(*)
     from swap
     where approved and (from_user = u.id or to_user = u.id)) as swap_used
  --     coalesce((select b.amount
  --               from balance b
  --               where b."user" = u.id), 0) as eur
  from "user" u left join "user" s on u.parent = s.id;

create or replace view course_program_view as
  select
    c.*,
    program
  from course c
    join course_program p on c.id = p.course;

create or replace view course_program_user as
  select
    u."user",
    p.program,
    p.course
  from course_user u
    join course_program p on u.course = p.course;

CREATE OR REPLACE VIEW "table_size" as
  with
      a as (
        select
          c.oid,
          nspname                               as table_schema,
          relname                               as "table_name",
          c.reltuples                           as row_estimate,
          pg_total_relation_size(c.oid)         as total_bytes,
          pg_indexes_size(c.oid)                as index_bytes,
          pg_total_relation_size(reltoastrelid) as toast_bytes
        from pg_class c
          left join pg_namespace n on n.oid = c.relnamespace
        where relkind = 'r'
    ),
      b as (
        select
          *,
          total_bytes - index_bytes - COALESCE(toast_bytes, 0) as table_bytes
        from a
    )
  select
    "table_name",
    pg_size_pretty(total_bytes) as total,
    pg_size_pretty(index_bytes) as INDEX,
    pg_size_pretty(toast_bytes) as toast,
    pg_size_pretty(table_bytes) as "table",
    total_bytes
  from b
  where table_schema = 'public';
