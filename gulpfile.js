const gulp = require('gulp')
const scss = require('gulp-scss')

gulp.task('scss', function () {
    return gulp.src('./web/css/main.scss')
        .pipe(scss())
        .pipe(gulp.dest('./web/css'))
})

gulp.task('scss:watch', ['scss'], function () {
    gulp.watch('./web/css/*.scss', ['scss'])
})

gulp.task('default', ['scss:watch']);
