<?php
/**
 * Last modified: 18.11.20 23:51:27
 * Hash: 9f6dff93ed454386d7a76a27e702e5c21040d487
 */

namespace app\helpers;

use app\models\Transfer;
use app\models\User;
use Yii;

class Html extends \yii\bootstrap\Html
{
    public static function email($address): string
    {
        return "<a href=\"mailto:$address\">$address</a>";
    }

    public static function item($text, $url, $icon = ''): string
    {
        if (!Yii::$app->user->getIsGuest() && is_array($url)) {
            $url['nick'] = Yii::$app->user->identity->nick;
        }
        $text = Yii::t('app', $text);
        return static::a("<i class=\"zmdi zmdi-$icon\"></i> <span>$text</span>", $url);
    }

    public static function avatar($url): string
    {
        $options = ['class' => 'avatar'];
        if ($url) {
            $options['style'] = "background-image: url('$url')";
        } else {
            $options['class'] .= ' absent';
        }
        return self::tag('div', '', $options);
    }

    public static function money(int $amount, string $currency): string
    {
        return self::tag('span', $amount, [
            'class' => 'money ' . strtolower($currency)
        ]);
    }

    /**
     * @param array $state
     * @return string
     * @throws \yii\base\Exception
     */
    public static function context(?array $state = null): string
    {
        $config = [
            'id' => Yii::$app->id,
            'locale' => Yii::$app->language,
            'mode' => YII_ENV,
            'factor' => Transfer::FACTOR,
            'timezone' => date_default_timezone_get(),
//            'levels' => Transfer::LEVELS,
            'fileSize' => Yii::$app->params['fileSize'] ?? 1024 * 1024,
            'request' => [
                'id' => Utils::generateCode(24),
                'time' => Utils::timestamp()
            ],
            'server' => [
                'php' => [
                    'v' => PHP_VERSION,
                    'encoding' => mb_internal_encoding(),
//                    'extensions' => get_loaded_extensions()
                ]
            ]
        ];

        if (YII_DEBUG) {
            foreach (['display_errors', 'post_max_size', 'precision', 'max_file_uploads', 'memory_limit'] as $name) {
                $config['server']['php'][$name] = ini_get($name);
            }
        }

        if (!Yii::$app->user->getIsGuest()) {
            /**
             * @var User $user
             */
            $user = Yii::$app->user->identity;
//            $config['bonuses'] = Program::getBonuses();
            $config['user'] = [
                'id' => $user->id,
                'nick' => $user->nick,
                'created' => $user->created,
            ];
        }

        if (!empty($state)) {
            $config = array_merge($config, $state);
        }

        $vars = [
            'appConfig' => $config
        ];

        if ($state['translation'] ?? false) {
            $vars['translation'] = Utils::getLocale(Yii::$app->language);
        }

        $script = [];
        foreach ($vars as $name => $value) {
            $json = Utils::json($value);
            $script[] = "const $name=$json;";
        }
        return implode(YII_DEBUG ? "\n// -------------------------------------\n" : '', $script);
    }

    public static function extendContext($state): string
    {
        if ($state) {
            return static::tag('div', Utils::json($state), ['class' => 'extension', 'style' => 'display: none']);
        }
        return '';
    }

    public static function stamp()
    {
        $info = ['Request at ' . date('Y-m-d H:i:s')];
        foreach (['HOST', 'IP', 'USER_AGENT', 'ACCEPT', 'ACCEPT_LANGUAGE'] as $header) {
            if (isset($_SERVER['HTTP_' . $header])) {
                $info[] = "\t$header: " . $_SERVER['HTTP_' . $header];
            }
        }
        return implode("\n", $info);
    }

    public static function resource($filename)
    {
        $locale = Yii::$app->language;
        return Yii::getAlias("@web/images/pr/$locale/" . $filename);
    }

    public static function banner($filename, array $options = []): string
    {
        return static::img(static::resource('banners/' . $filename), $options);
    }

    public static function promo($filename, array $options = []): string
    {
        return static::img(static::resource('promo/' . $filename), $options);
    }
}
