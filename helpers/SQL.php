<?php
/**
 * Last modified: 18.10.05 23:34:38
 * Hash: df2253e7b5e05e1266b57ceadd26b61513079d55
 */

namespace app\helpers;


use PDO;
use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;

class SQL
{
    /**
     * @param $sql
     * @param array $params
     * @return false|null|string
     * @throws \yii\db\Exception
     */
    public static function scalar($sql, array $params = []) {
        return Yii::$app->db->createCommand($sql, $params)->queryScalar();
    }

    /**
     * @param $sql
     * @param array $params
     * @return array
     * @throws \yii\db\Exception
     */
    public static function column($sql, array $params = []): array
    {
        return Yii::$app->db->createCommand($sql, $params)->queryColumn();
    }

    /**
     * @param $sql
     * @param array $params
     * @param int $fetch_mode
     * @return array
     * @throws \yii\db\Exception
     */
    public static function queryAll($sql, array $params = [], $fetch_mode = PDO::FETCH_ASSOC): array
    {
        return Yii::$app->db->createCommand($sql, $params)->queryAll($fetch_mode);
    }

    /**
     * @param $sql
     * @param array $params
     * @param int $fetch_mode
     * @return array|false
     * @throws \yii\db\Exception
     */
    public static function queryOne($sql, array $params = [], $fetch_mode = PDO::FETCH_ASSOC) {
        return Yii::$app->db->createCommand($sql, $params)->queryOne($fetch_mode);
    }

    /**
     * @param $sql
     * @param array $params
     * @return int
     * @throws \yii\db\Exception
     */
    public static function execute($sql, array $params = []): int
    {
        return Yii::$app->db->createCommand($sql, $params)->execute();
    }

    /**
     * @param $table
     * @param $value
     * @param string $key
     * @param int $fetch_mode
     * @return array|false
     * @throws \yii\db\Exception
     */
    public static function queryByKey($table, $value, $key = 'id', $fetch_mode = PDO::FETCH_ASSOC) {
        return Yii::$app->db->createCommand("SELECT * FROM \"$table\" WHERE \"$key\" = :$key LIMIT 1",
            [':' . $key => $value])->queryOne($fetch_mode);
    }

    /**
     * @param $table
     * @param int $start
     * @return int
     * @throws \yii\db\Exception
     */
    public static function restartSequence($table, $start = 1): int
    {
        return Yii::$app->db->createCommand('ALTER SEQUENCE ' . $table . "_id_seq RESTART $start")
            ->execute();
    }

    /**
     * @param $table
     * @return false|null|string
     * @throws \yii\db\Exception
     */
    public static function getSequenceValue($table) {
        return Yii::$app->db->createCommand("SELECT currval('" . $table . "_id_seq')")
            ->queryScalar();
    }

    public static function updateAll(array $records, $call) {
        foreach ($records as $record) {
            $call($record);
            if (!$record->update(false)) {
                return false;
            }
        }
        return true;
    }
}
