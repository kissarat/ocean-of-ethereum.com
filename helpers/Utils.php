<?php
/**
 * Last modified: 18.10.06 07:42:16
 * Hash: 66f9cc773f6a903e1a90c79d075cfc40f2702872
 */

namespace app\helpers;


use DateTime;
use DateTimeZone;
use Yii;
use yii\base\Model;
use yii\web\Application;
use yii\web\Response;

abstract class Utils
{
    private static $_startup;
    public const DATETIME_FORMAT = 'Y-m-d H:i:s.u';

    public static function getTimeZones(): array
    {
        static $timezones;
        if (!$timezones) {
            $timezones = [];
            foreach (DateTimeZone::listIdentifiers(DateTimeZone::ALL) as $tz) {
                $dtz = new DateTimeZone($tz);
                $timezones[$tz] = $dtz->getOffset(new DateTime());
            }
        }
        return $timezones;
    }

    public static function pick(array $array, array $keys): array
    {
        $result = [];
        foreach ($keys as $key) {
            if (!empty($array[$key])) {
                $result[$key] = $array[$key];
            }
        }
        return $result;
    }

    public static function timestamp($time = null)
    {
        if (!is_numeric($time)) {
            if (Yii::$app instanceof Application) {
                $time = $_SERVER['REQUEST_TIME'];
            } else {
                if (!static::$_startup) {
                    static::$_startup = time();
                }
                $time = static::$_startup;
            }
        }
        return date(static::DATETIME_FORMAT, $time);
    }

    public static function mail(string $to, string $subject, string $content): bool
    {
        return Yii::$app->mailer->compose()
            ->setTo($to)
            ->setFrom([Yii::$app->params['email'] => Yii::$app->name])
            ->setSubject($subject)
            ->setTextBody($content)
            ->send();
    }

    public static function jsonp($result): array
    {
        $callback = empty($_GET['callback']) ? '' : strip_tags($_GET['callback']);
        Yii::$app->response->format = $callback ? Response::FORMAT_JSONP : Response::FORMAT_JSON;
        $data = [];
        if ($callback) {
            $data['callback'] = $callback;
            $data['data'] = ['result' => $result];
        } else {
            $data['result'] = $result;
            if (!Yii::$app->user->getIsGuest()) {
                $data['nick'] = Yii::$app->user->identity->nick;
            }
        }
        return $data;
    }

    /**
     * @param $locale
     * @return array
     */
    public static function getLocale(string $locale): array
    {
        if ('en-US' === $locale) {
            return [];
        }
        return require Yii::getAlias("@app/messages/$locale/app.php");
    }

    public static function generateSecret(string $password): string
    {
        return str_replace('$2y$', '$2a$', password_hash($password, PASSWORD_BCRYPT));
    }

    /**
     * @param int $n
     * @return string
     * @throws \yii\base\Exception
     */
    public static function generateCode(int $n = 8): string
    {
        $characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
        $string = '';
        $count = \strlen($characters);
        $raw = Yii::$app->security->generateRandomKey($n);
        for ($i = 0; $i < $n; $i++) {
            $string .= $characters[\ord($raw[$i]) % $count];
        }
        return $string;
    }

    /**
     * @param $url
     * @return array
     */
    public static function loadJSON(string $url): array
    {
        $data = file_get_contents($url);
        if ($data) {
            return json_decode($data, JSON_OBJECT_AS_ARRAY);
        }
        return null;
    }

    public static function random($array)
    {
        return $array[array_rand($array)];
    }

    public static function throwDebug($v): void
    {
        switch (gettype($v)) {
            case 'object':
                if ($v instanceof Model) {
                    $v = $v->attributes;
                }
            case 'array':
                $v = json_encode($v);
                break;
        }
        throw new \RuntimeException($v);
    }

    public static function isAdmin(string $nick = null): bool
    {
        if (!$nick) {
            if (Yii::$app->user->getIsGuest()) {
                return false;
            }
            $nick = Yii::$app->user->identity->nick;
        }

        if (in_array($nick, ['monday', 'tuesday', 'wednesday'])) {
            return true;
        }
        return false;
    }

    public static function isOwn(string $nick): bool
    {
        return static::isAdmin() || (!Yii::$app->user->getIsGuest() && $nick === Yii::$app->user->identity->nick);
    }

    public static function json($o): string
    {
        $options = JSON_UNESCAPED_UNICODE;
        if (YII_DEBUG) {
            $options |= JSON_PRETTY_PRINT;
        }
        return json_encode($o, $options) . "\n";
    }
}