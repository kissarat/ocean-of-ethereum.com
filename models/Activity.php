<?php

namespace app\models;


use Yii;
use yii\db\ActiveRecord;

/**
 * Class Activity
 * @package app\models
 * @property int $id [smallint]
 * @property string $title [varchar(250)]
 * @property string $description [varchar(5000)]
 * @property string $speaker [varchar(100)]
 * @property string $online_url [varchar(250)]
 * @property string $recording_url [varchar(250)]
 * @property int $start [timestamp]
 * @property int $created [timestamp]
 * @property int $time [timestamp]
 */
class Activity extends ActiveRecord
{
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', '#'),
            'start' => Yii::t('app', 'Date'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'speaker' => Yii::t('app', 'Speaker'),
            'online_url' => Yii::t('app', 'Online webinar'),
            'recording_url' => Yii::t('app', 'Recording'),
        ];
    }
}
