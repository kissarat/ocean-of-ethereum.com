<?php
/**
 * Last modified: 18.07.20 07:21:37
 * Hash: 4ea5463550318d953de15a8246d20c21b4b09df9
 */


namespace app\models;


use yii\db\ActiveRecord;

/**
 * Class Article
 * @package app\models
 * @property string $id [integer]
 * @property string $path [varchar(96)]
 * @property string $type [article_type]
 * @property string $price [integer]
 * @property string $name [varchar(96)]
 * @property string $short [varchar(512)]
 * @property int $time [timestamp]
 * @property int $created [timestamp]
 * @property string $text [varchar(48000)]
 * @property string $image [varchar(256)]
 * @property string $user [integer]
 * @property int $priority [smallint]
 * @property bool $active [boolean]
 */
class Article extends ActiveRecord
{

}
