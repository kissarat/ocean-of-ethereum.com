<?php
/**
 * Last modified: 18.10.14 02:39:37
 * Hash: 5709b1d9b31654e51660e164812404e15bbcb6e5
 */


namespace app\models;


use app\helpers\SQL;
use app\helpers\Utils;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class Claim
 * @package app\models
 *
 * @property User $fromObject
 * @property bool $isAccepted
 * @property User $userObject
 * @property User $toObject
 * @property bool $isDeclined
 * @property bool $from_approved
 * @property bool $to_approved
 * @property int user
 * @property int from
 * @property int to
 * @property string $id [integer]
 * @property int $created [timestamp]
 * @property int $time [timestamp]
 */
class Claim extends ActiveRecord
{
    public const MAX = 2;
    public const FROM_SUBJECT = 'from';
    public const TO_SUBJECT = 'to';

    public $to_nick;

    /**
     * @return array
     */
    public static function primaryKey(): array
    {
        return ['id'];
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            ['to_nick', 'required'],
            ['to_nick', 'exist',
                'targetAttribute' => 'nick',
                'targetClass' => User::class,
                'message' => Yii::t('app', 'Recipient not found')],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'created' => Yii::t('app', 'Created'),
            'from' => Yii::t('app', 'Old sponsor'),
            'to' => Yii::t('app', 'New sponsor'),
            'to_nick' => Yii::t('app', 'New sponsor'),
            'user' => Yii::t('app', 'Who'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getUserObject(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'user']);
    }

    /**
     * @return ActiveQuery
     */
    public function getFromObject(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'from']);
    }

    /**
     * @return ActiveQuery
     */
    public function getToObject(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'to']);
    }

    /**
     * @return bool
     */
    public function getIsAccepted(): bool
    {
        return $this->from_approved && $this->to_approved;
    }

    /**
     * @return bool
     */
    public function getIsRejected(): bool
    {
        return false === $this->from_approved || false === $this->to_approved;
    }


    /**
     * @return bool
     */
    public function getIsUsed(): bool
    {
        return null !== $this->from_approved && null !== $this->to_approved;
    }

    /**
     * @return bool
     */
    public function getIsPending(): bool
    {
        return !$this->getIsUsed() && !$this->getIsRejected();
    }

    /**
     * @param string $subject
     * @param bool $accept
     * @return bool
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function approve(string $subject, bool $accept): bool
    {
        $property = $subject . '_approved';
        $this->$property = $accept;
        $this->time = Utils::timestamp();
        if ($this->update(false)) {
            if ($this->getIsAccepted()) {
                $user = $this->userObject;
                $user->parent = $this->to;
                return $user->update(false);
            }
            return true;
        }
        return false;
    }

    /**
     * @param int $user_id
     * @param string $name
     * @return int
     */
    public static function countActive(int $user_id, string $name = 'user'): int
    {
        return static::find()
            ->andWhere([$name => $user_id])
            ->andWhere('(from_approved is null and to_approved is null)
            or (from_approved = true and to_approved is null)
            or (from_approved is null and to_approved = true)')
            ->count();
    }

    /**
     * @param int $user_id
     * @return int
     */
    public static function count(int $user_id): int
    {
        return static::find()
            ->andWhere(['user' => $user_id])
            ->andWhere('(from_approved = true and to_approved = true)
            or (from_approved is null and to_approved   = true)
            or (to_approved   is null and from_approved = true)')
            ->count();
    }

    /**
     * @param User $user
     * @param string $name
     * @return bool
     */
    public static function can(User $user, string $name = 'user'): bool
    {
        if (static::countActive($user->id, $name) > 0) {
            return false;
        }

        if (static::count($user->id) < $user->claim_count) {
            return true;
        }
        return false;
    }

    public function loadTo(): bool
    {
        if (!($this->to > 0)) {
            $to = User::findOne(['nick' => $this->to_nick]);
            if ($to) {
                $this->to = $to->id;
                return true;
            }
        }
        return false;
    }

    /**
     * @param User $user
     * @return bool
     * @throws \yii\base\InvalidArgumentException
     */
    public function create(User $user): bool
    {
        $this->user = $user->id;
        $this->from = $user->parent;
        if ($this->validate()) {
            /**
             * @var User $to
             */
            $this->loadTo();
            return $this->save();
        }
        return false;
    }
}
