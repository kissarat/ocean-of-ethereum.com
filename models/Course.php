<?php

namespace app\models;


use yii\db\ActiveRecord;

/**
 * Class Course
 * @package app\models
 * @property int $id [smallint]
 * @property string $title [varchar(250)]
 * @property string $image [varchar(250)]
 * @property string $about [varchar(250)]
 * @property string $download [varchar(250)]
 * @property int $created [timestamp]
 * @property int $time [timestamp]
 */
class Course extends ActiveRecord
{
    public $downloaded;
}
