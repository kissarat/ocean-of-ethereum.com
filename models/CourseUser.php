<?php

namespace app\models;


use app\helpers\SQL;
use yii\db\ActiveRecord;

/**
 * Class CourseUser
 * @package app\models
 * @property int $course [smallint]
 * @property string $user [integer]
 * @property int $created [timestamp]
 */
class CourseUser extends ActiveRecord
{
    public static function primaryKey()
    {
        return ['course', 'user'];
    }

    public static function tableName()
    {
        return 'course_user';
    }

    /**
     * @param int $user
     * @param int $program
     * @return array
     * @throws \yii\db\Exception
     */
    public static function getDownloaded(int $user, int $program) {
        return SQL::column('select course from course_program_user where "user" = :user and program = :program group by course', [
            ':user' => $user,
            ':program' => $program,
        ]);
    }
}
