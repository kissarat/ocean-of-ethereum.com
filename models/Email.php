<?php
/**
 * Last modified: 18.10.06 04:43:56
 * Hash: b1f427251015043eac867db69eae0cbd7da353c1
 */

namespace app\models;


use app\helpers\Utils;
use yii\db\ActiveRecord;

class Email extends ActiveRecord
{
    public static function send($to, $subject, $content)
    {
        $email = new self([
            'to' => $to,
            'subject' => $subject,
            'content' => $content,
        ]);
        if ($email->save()) {
            Utils::mail($to, $subject, $content);
            $email->sent = Utils::timestamp();
            if ($email->save(false)) {
                return $email;
            }
        }
        return false;
    }
}
