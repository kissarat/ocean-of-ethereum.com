<?php
/**
 * Last modified: 18.06.24 05:41:16
 * Hash: 5e2606d16c5b9026fff09cb3df7e3c70c92884dc
 */


namespace app\models;


class GeneratedUser extends User
{
    public $program;

    public static function tableName(): string
    {
        return 'user';
    }
}
