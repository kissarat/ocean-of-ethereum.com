<?php
/**
 * Last modified: 18.09.14 22:22:24
 * Hash: 160c0147b77171dd453b591631c5d5c30b411745
 */

namespace app\models;


use Yii;
use yii\db\ActiveRecord;

/**
 * Class Log
 * @package app\models
 * @property int $id [bigint]
 * @property string $entity [varchar(16)]
 * @property string $action [varchar(32)]
 * @property string $ip [inet]
 * @property string $user [integer]
 * @property string $token [varchar(192)]
 * @property string $data [json]
 */
class Log extends ActiveRecord
{
    /**
     * @param string $entity
     * @param string $action
     * @param null|array $data
     * @param int|null $user
     * @param null|string $ip
     * @return Log|null
     */
    public static function log(string $entity,
                               string $action,
                               ?array $data = null,
                               ?int $user = null,
                               ?string $ip = null
    ): ?Log
    {
        $id = static::nanotime();
        $u = Yii::$app->user;
        $log = new static([
            'id' => $id,
            'entity' => $entity,
            'action' => $action,
            'data' => $data,
            'token' => $_COOKIE[ini_get('session.name')] ?? null,
            'user' => $user ?: ($u->getIsGuest() ? null : $u->identity->id),
            'ip' => $ip ?: Yii::$app->request->getUserIP(),
        ]);
        if ($log->save(false)) {
            return $log;
        }
        return null;
    }

    public static function debug(string $entity,
                                 string $action,
                                 ?array $data = null,
                                 ?int $user = null,
                                 ?string $ip = null): ?Log
    {
        if (YII_DEBUG) {
            return static::log($entity, $action, $data, $user, $ip);
        }
        return null;
    }

    public static function nanotime()
    {
        return (int)floor(microtime(true) * 10000) * 100000;
    }

    public static function lastTime()
    {
        return (time() - 3600) * 1000 * 1000 * 1000;
    }
}
