<?php
/**
 * Last modified: 18.11.20 23:42:00
 * Hash: b457b3e113471711015423cf94a8668553963986
 */

namespace app\models;


use app\helpers\SQL;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class Node
 * @property int $id
 * @property int $program
 * @property int $user
 * @property int $parent
 * @property int $cell
 * @property int $qualification
 * @property int $created [timestamp]
 * @property bool $programIsFinish
 * @property bool $isMatrixActive
 * @property int $memberCount
 * @property int $matrixSize
 * @property int $time [timestamp]
 * @property Program $programObject
 * @property Node $parentObject
 * @property User $userObject
 * @property Node $qualificationObject
 * @property Offer $offerObject
 * @property array $qualifications
 * @property int $qualified [smallint]
 * @package app\models
 */
class Node extends ActiveRecord
{
    /**
     * @return ActiveQuery
     */
    public function getProgramObject(): ActiveQuery
    {
        return $this->hasOne(Program::class, ['id' => 'program']);
    }

    /**
     * @return ActiveQuery
     */
    public function getParentObject(): ActiveQuery
    {
        return $this->hasOne(static::class, ['id' => 'parent']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUserObject(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'user']);
    }

    /**
     * @return ActiveQuery
     */
    public function getQualificationObject(): ActiveQuery
    {
        return $this->hasOne(static::class, ['id' => 'qualification']);
    }

    /**
     * @param int $program_id
     * @return Offer
     * @throws \Exception
     * @throws \Throwable
     */
    public function createOffer(int $program_id): Offer
    {
        $offer = new Offer([
            'origin' => $this->id,
            'user' => $this->user,
            'program' => $program_id
        ]);
//        \assert($offer->insert(), 'Cannot create offer');
        return $offer;
    }

    /**
     * @return int
     * @throws \yii\db\Exception
     */
    public function getMemberCount(): int
    {
        return SQL::scalar('SELECT count_matrix(:id)', [':id' => $this->id]);
    }

    /**
     * @return bool
     */
    public function getProgramIsFinish(): bool
    {
        return $this->program % 2 === 0;
    }

    /**
     * @return int
     */
    public function getMatrixSize(): int
    {
        return $this->getProgramIsFinish() ? Program::FinishCount : Program::StartCount;
    }

    /**
     * @return bool
     * @throws \yii\db\Exception
     */
    public function isMatrixActive(): bool
    {
        return $this->getMemberCount() < $this->getMatrixSize();
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'amount' => Yii::t('app', 'Amount'),
            'accrues' => Yii::t('app', 'Accrues'),
            'profit' => Yii::t('app', 'Profit'),
            'currency' => Yii::t('app', 'Currency'),
            'closing' => Yii::t('app', 'Closing'),
        ];
    }

    /**
     * @param int $level
     * @return $this
     * @throws \yii\db\Exception
     */
    public function getRoot(int &$level = 0): self
    {
        if ($this->parent > 0
//            && $level < 5
            && $this->isMatrixActive()
        ) {
            $parent = $this->parentObject;
            if ($parent) {
                if (!$parent->isMatrixActive()) {
                    return $this;
                }
                ++$level;
                $root = $parent->getRoot($level);
                return $root->getRoot($level);
            }
        }
        return $this;
    }

    /**
     * @param int $id
     * @param int $level
     * @return int
     * @throws \yii\db\Exception
     */
    public static function getLevelUp(int $id, int $level = 1): int
    {
        return SQL::scalar('SELECT get_root(:id, :level)', [
            ':id' => $id,
            ':level' => $level - 1
        ]);
    }

    /**
     * @param int $id
     * @return array
     * @throws \yii\db\Exception
     */
    public static function getMatrix(int $id): array
    {
        return SQL::queryAll('WITH RECURSIVE r(id, parent, program, "user", level, cell, "left", qualification) AS (
            SELECT
              id,
              parent,
              program,
              "user",
              0 :: INT AS "level",
              cell,
              0 :: INT AS "left",
              qualification
            FROM node
            WHERE id = :id
            UNION
            SELECT
              n.id,
              n.parent,
              n.program,
              n."user",
              r.level + 1 AS "level",
              n.cell,
              r."left" * 2 + (1 - n.cell % 2) as "left",
              n.qualification
            FROM node n
              JOIN r ON r.id = n.parent
            WHERE r.level < 3 + (1 - r.program % 2)
          )
          SELECT r.*,
            u.nick,
            u.surname,
            u.forename,
            u.avatar,
            u.skype,
            (power(2, level) - 1 + "left") as number,
            qualification
          FROM r JOIN "user" u ON r.user = u.id
          ORDER BY r.parent, r.cell % 2, r.id', [
            ':id' => $id
        ]);
    }

    public function getQualifications(): ActiveQuery
    {
//        return $this->hasMany(static::class, ['id' => 'qualification']);
        return $this->hasMany(static::class, ['qualification' => 'id']);
    }

    public function getOfferObject(): ActiveQuery
    {
        return $this->hasOne(Offer::class, ['used' => 'id']);
    }
}
