<?php
/**
 * Last modified: 18.09.23 23:35:18
 * Hash: 1aa0232e12243c2b959aba5192753ea2463b52f7
 */

namespace app\models;


use app\helpers\Utils;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\ForbiddenHttpException;

/**
 * Class Offer
 * @property int $id
 * @property int $user
 * @property int $origin
 * @property int $program
 * @property int $used
 * @property string $created
 * @property Program $programObject
 * @property Node $originObject
 * @property string $time
 * @package app\models
 */
class Offer extends ActiveRecord
{

    public function attributeLabels(): array
    {
        return [
            'created' => Yii::t('app', 'Created'),
            'program' => Yii::t('app', 'Program'),
            'used' => Yii::t('app', 'Used'),
        ];
    }

    /**
     * @param $parent_id
     * @param $cell
     * @return int
     * @throws \yii\db\Exception
     * @throws \yii\base\InvalidArgumentException
     * @throws \Error
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function open($parent_id, $cell): int
    {
        $used = Program::open($parent_id, $this->user, $cell);
        if ($used > 0) {
            $this->used = $used;
            $this->time = Utils::timestamp();
            if ($this->update()) {
                return $used;
            }
        }
        return 0;
    }

    public function getProgramObject(): ActiveQuery
    {
        return $this->hasOne(Program::class, ['id' => 'program']);
    }

    /**
     * @param int|null $offer_id
     * @throws ForbiddenHttpException
     */
    public static function checkAvailability(?int $offer_id)
    {
        if ($offer_id > 0) {
            $offer = Offer::findOne($offer_id);
            if ($offer->used > 0) {
                throw new ForbiddenHttpException(Yii::t('app', 'Offer is already used'));
            }
            if ($offer->user !== Yii::$app->user->identity->id) {
                throw new ForbiddenHttpException(Yii::t('app', 'Is not your offer'));
            }
        }
    }

    public function getOriginObject(): ActiveQuery
    {
        return $this->hasOne(Node::class, ['id' => 'origin']);
    }
}
