<?php
/**
 * Last modified: 19.01.29 02:57:05
 * Hash: ed98c87f72b8417ea592042d16a0a28c0bbf16e9
 */

namespace app\models;


use app\helpers\SQL;
use app\helpers\Utils;
use yii\base\InvalidArgumentException;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\HttpException;

/**
 * Class Program
 * @property int $id
 * @property string $name
 * @property int $price
 * @property int $previous
 * @property Program $previousObject
 * @property Program $nextObject
 * @property boolean $finish
 * @property bool $active [boolean]
 * @property string $level [integer]
 * @property int $power
 * @property float $size [double precision]
 * @property string $courses [integer]
 * @package app\models
 */
class Program extends ActiveRecord
{
    public const LIST_CACHE_DURATION = 7200;
    public const DOLPHIN = 15;
    public const SHARK = 17;
    public const WHALE = 19;

    public const StartPower = 4;
    public const StartCount = (2 ** self::StartPower) - 1;
    public const FinishPower = 3;
    public const FinishCount = (2 ** self::FinishPower) - 1;

    public static function tableName(): string
    {
        return 'program_view';
    }

    /**
     * @return string[]
     */
    public static function primaryKey(): array
    {
        return ['id'];
    }

    public function getNextObject(): ActiveQuery
    {
        return $this->hasOne(static::class, ['previous' => 'id']);
    }

    public function getPreviousObject(): ActiveQuery
    {
        return $this->hasOne(static::class, ['id' => 'previous']);
    }

    /**
     * @param int $program_id
     * @param int $user_id
     * @return bool
     * @throws \yii\db\Exception
     */
    public static function canBuy(int $program_id, int $user_id = 0): bool
    {
        $balance = User::getBalance($user_id);
        if ($balance > 0) {
            $program = self::findOne($program_id);
            return $program && $balance >= $program->price;
        }
        return false;
    }

    /**
     * @param array $params
     * @return Transfer|null
     * @throws \Exception
     * @throws \Throwable
     */
    public static function transfer(array $params = []): ?Transfer
    {
        $params['status'] = 'success';
        $params['created'] = Utils::timestamp();
        $transfer = new Transfer($params);
        if ($transfer->insert(false)) {
            return $transfer;
        }
        return null;
    }

    /**
     * @param int $parent_id
     * @param int $user_id
     * @param int $cell
     * @return int
     * @throws \yii\base\InvalidArgumentException
     * @throws \yii\db\Exception
     * @throws HttpException
     * @throws \Error
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public static function buy(int $parent_id, int $user_id, int $cell): int
    {
        $parent = Node::findOne($parent_id);
        if (!$parent) {
            throw new InvalidArgumentException('parent_id');
        }
        $program = $parent->programObject;
        $balance = User::getBalance($user_id);
        if ($balance >= $program->price) {
            $buy = static::transfer([
                'from' => $user_id,
                'amount' => $program->price,
                'type' => 'buy',
                'vars' => [
                    'name' => $program->name
                ]
            ]);
            if (!$buy) {
                throw new \Exception('Cannot buy');
            }
            $node_id = static::open($parent_id, $user_id, $cell);
            if ($node_id > 0) {
                $buy->node = $node_id;
                if ($buy->update(false)) {
                    return $node_id;
                }
            }
        } else {
            throw new HttpException(402);
        }
        return 0;
    }

    /**
     * @param int $parent_id
     * @param int $user_id
     * @param int $cell
     * @return int
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
    public static function open(int $parent_id, int $user_id, int $cell): int
    {
        /**
         * @var User $user
         * @var Node $parent
         * @var Node $matrix
         * @var Program $program
         */
        $user = User::findOne($user_id);
        if (!$user) {
            throw new InvalidArgumentException('user_id');
        }
        $parent = Node::findOne($parent_id);
        if (!$parent) {
            throw new InvalidArgumentException('parent_id');
        }
        $program = $parent->programObject;
        if (!$program) {
            throw new \Exception('Invalid program');
        }

        $node = new Node([
            'program' => $program->id,
            'user' => $user->id,
            'parent' => $parent->id,
            'cell' => $cell,
            'created' => Utils::timestamp()
        ]);
        $sponsor = $user->parentObject;
        if (!$sponsor) {
            throw new \Exception('Sponsor not found');
        }
        $matrix = $parent->getRoot();
        if (!$matrix) {
            throw new \Exception('Matrix not found');
        }
        $sponsorNode = $sponsor->getActiveMatrix($program->id);
        if ($sponsorNode) {
            $node->qualification = $sponsorNode->id;
        }
        if (!$node->insert(false)) {
            throw new \Exception('Cannot create node');
        }
//        $majors = $user->getMajors();

        $nodeUser = $node->userObject;
        if ($program->finish) {
            $accrue = 0;
            $qualification = SQL::scalar('SELECT qualified FROM offer_qualification WHERE used = :used', [
                ':used' => $matrix->id
            ]);
            if ($qualification >= 0) {
                $accrue = $program->price * $qualification;
                if (YII_TEST) {
                    $ids = [];
                    Log::debug('node', 'qualifications', [
                        'node' => $node->id,
                        'root' => $matrix->id,
                        'ids' => $ids,
                        'program' => $program->id
                    ]);
                }
            } else {
                Log::debug('offer', 'absent', ['origin' => $matrix->id]);
            }

        } else {
            $accrue = floor($program->price / (2 ** ($program->getPower() - 1)));
//            $major_ids = array_map(function ($major) {
//                return (int)$major['id'];
//            }, $majors);
//            foreach ($majors as $major) {
//                static::transfer([
//                    'to' => $major['id'],
//                    'type' => 'bonus',
//                    'node' => $node->id,
//                    'amount' => (int)floor($program->price * 0.5),
//                    'vars' => [
//                        'from' => $node->user,
//                        'from_nick' => $nodeUser->nick,
//                        'name' => $program->name,
//                        'level' => $major['level'],
//                        'major' => true
//                    ]
//                ]);
//            }
            foreach ($user->getReward() as $reward) {
                $id = (int)$reward['id'];
                static::transfer([
                    'to' => $id,
                    'type' => 'bonus',
                    'node' => $node->id,
                    'amount' => (int)floor($program->price * 0.5),
                    'vars' => [
                        'from' => $node->user,
                        'from_nick' => $nodeUser->nick,
                        'name' => $program->name,
                        'level' => $reward['level']
                    ]
                ]);
            }
        }

        static::transfer([
            'to' => $matrix->user,
            'type' => 'accrue',
            'amount' => (int)$accrue,
            'node' => $node->id,
            'vars' => [
                'from' => $node->user,
                'from_nick' => $nodeUser->nick,
                'cell' => $cell,
                'program' => $program->id,
                'name' => $program->name
            ]
        ]);

        if (!$matrix->isMatrixActive()) {
            $nextProgram = $program->nextObject;
            /** @var Node $next */
            $next = null;
            if ($program->finish) {
                $matrix->createOffer($program->previous);
            }
            if ($nextProgram) {
                if ($program->finish) {
                    $next = $nextProgram->getNodeByUser($matrix->user);
                }
                if (!$next) {
                    $matrix->createOffer($nextProgram->id);
                }
            }
            if ($next || !$nextProgram) {
                static::transfer([
                    'to' => $matrix->user,
                    'type' => 'refund',
                    'amount' => $nextProgram ? $nextProgram->price : 0,
                    'node' => $node->id,
                    'vars' => [
                        'name' => $program->name
                    ]
                ]);
            }
        }

        return $node->id;
    }

    /**
     * @param int $program
     * @param User $user
     * @return Node|null
     * @throws \yii\db\Exception
     */
    public static function tryOpen(int $program, ?User $user): ?Node
    {
        if ($user && ($node = $user->getActiveMatrix($program))) {
            return $node;
        }
        return null;
    }

    public function getPower(): int
    {
        return $this->finish ? static::FinishPower : static::StartPower;
    }

    public function getSize(): int
    {
        return $this->finish ? static::FinishCount : static::StartCount;
    }

    public function getNodes()
    {
        return $this->hasMany(Node::class, ['program' => 'id']);
    }

    public function getNodeByUser(int $user_id): ?Node
    {
        return $this->getNodes()
            ->where(['user' => $user_id])
            ->one();
    }

    public static function getPrograms()
    {
        return static::find()
            ->cache(static::LIST_CACHE_DURATION)
            ->all();
    }

    public static function getStartPrograms()
    {
        return static::find()
            ->where('not finish')
            ->cache(static::LIST_CACHE_DURATION)
            ->all();
    }
}
