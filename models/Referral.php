<?php
/**
 * Last modified: 18.07.26 19:53:44
 * Hash: e17ad5a67106d3db3259a2b73eee2204b7eaa41d
 */

namespace app\models;


use app\models\traits\UserTrait;
use Yii;
use yii\db\ActiveRecord;

/**
 * Class Referral
 * @package app\models
 * @property string $id [integer]
 * @property string $nick [varchar(24)]
 * @property string $parent [integer]
 * @property string $email [varchar(48)]
 * @property string $forename [varchar(48)]
 * @property string $surname [varchar(48)]
 * @property string $skype [varchar(120)]
 * @property string $telegram [varchar(120)]
 * @property string $avatar [varchar(192)]
 * @property int $created [timestamp(0)]
 * @property int $children [bigint]
 * @property array[] $nodes [json]
 */
class Referral extends ActiveRecord
{
    use UserTrait;

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['id', 'children'], 'integer'],
            [['nick', 'surname', 'forename', 'skype', 'telegram', 'email'], 'string'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'nick' => Yii::t('app', 'Username'),
            'forename' => Yii::t('app', 'First Name'),
            'surname' => Yii::t('app', 'Last Name'),
            'children' => Yii::t('app', 'Referrals'),
        ];
    }

    public static function tableName(): string
    {
        return 'structure';
    }

    /**
     * @return string[]
     */
    public static function primaryKey(): array
    {
        return ['id'];
    }
}
