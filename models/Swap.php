<?php
/**
 * Last modified: 19.02.03 04:10:52
 * Hash: c7c56170928565c0b185322d687215e517801bf4
 */

namespace app\models;


use app\helpers\SQL;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class Swap
 * @package app\models
 * @property int $from
 * @property int $from_user
 * @property int $to
 * @property int $to_user
 * @property int $root
 * @property int $level
 * @property boolean $approved
 * @property string $created
 * @property string $time
 * @property string $id [integer]
 * @property Node $fromObject
 * @property User $fromUserObject
 * @property Node $toObject
 * @property User $toUserObject
 * @property bool $isPending
 * @property Node $matrix
 */
class Swap extends ActiveRecord
{
    public function attributeLabels(): array
    {
        return [
            'created' => Yii::t('app', 'Created'),
            'from_user' => Yii::t('app', 'Who'),
            'to_user' => Yii::t('app', 'Instead'),
            'approved' => Yii::t('app', 'Confirmation'),
        ];
    }

    public function getFromObject()
    {
        return $this->hasOne(Node::class, ['id' => 'from']);
    }

    public function getFromUserObject()
    {
        return $this->hasOne(User::class, ['id' => 'from_user']);
    }

    public function getToObject()
    {
        return $this->hasOne(Node::class, ['id' => 'to']);
    }

    public function getToUserObject()
    {
        return $this->hasOne(User::class, ['id' => 'to_user']);
    }

    public function getMatrix()
    {
        return $this->hasOne(Node::class, ['id' => 'root']);
    }

    public function getIsPending()
    {
        return null === $this->approved;
    }

    public static function forUser(int $user_id): ActiveQuery
    {
        return static::find()
            ->andWhere('("from_user" = :user_id or "to_user" = :user_id) and visible', [
                ':user_id' => $user_id
            ]);
    }

    public static function countForUser(int $user_id): int
    {
        return static::forUser($user_id)
            ->andWhere('approved = true')
            ->count();
    }

    /**
     * @param int $user
     * @param int $program
     * @return int
     * @throws \yii\db\Exception
     */
    public static function hasUnapproved(int $user, int $program)
    {
        return SQL::scalar('SELECT count(*) FROM swap WHERE approved is null AND ( from_user = :user OR to_user = :user)', [
                ':user' => $user,
//                ':program' => $program
            ]) > 0;
    }

    /**
     * @param User $user
     * @param int $program
     * @return bool
     * @throws \yii\db\Exception
     */
    public static function can(User $user, int $program): bool
    {
//        header('CC: ' . (static::countForUser($user->id) ?: '0'));
//        header('SC: ' . json_encode($user->attributes));
//        header('UN: ' . (static::hasUnapproved($user->id, $program) ? '1' : '0'));
        return static::countForUser($user->id) < $user->swap_count && !static::hasUnapproved($user->id, $program);
    }

    public static function updateParent(array $nodes, int $parent)
    {
        return SQL::updateAll($nodes, function (Node $node) use ($parent) {
            $node->parent = $parent;
        });
    }
}
