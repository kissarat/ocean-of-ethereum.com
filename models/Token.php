<?php
/**
 * Last modified: 18.07.26 02:35:24
 * Hash: 893aaf56d32009fd3e966e8efc32c46a046e6a06
 */

namespace app\models;


use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/** @noinspection LongInheritanceChainInspection */

/**
 * Class Token
 * @property string $id
 * @property int $user
 * @property string $type
 * @property string $expires
 * @property array $data
 * @property string $ip
 * @property string $created
 * @property User $userObject
 * @property string $time
 * @property int $handshake [timestamp]
 * @property string $name [varchar(240)]
 * @package app\models
 */
class Token extends ActiveRecord
{
    public static function primaryKey(): array
    {
        return ['id'];
    }

    public function rules(): array
    {
        return [
            [['id', 'type'], 'string', 'max' => 240],
            [['id', 'type'], 'required']
        ];
    }

    public function getUserObject(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'user']);
    }
}
