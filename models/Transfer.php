<?php
/**
 * Last modified: 18.11.21 00:02:30
 * Hash: 8f888fa642d98ed4fc9e94361336879b82b203a2
 */

namespace app\models;


use app\helpers\SQL;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\mssql\PDO;
use yii\db\Query;

/**
 * Class Transfer
 * @property integer $id
 * @property integer $from
 * @property integer $to
 * @property integer $amount
 * @property string $type
 * @property string $status
 * @property integer $node
 * @property string $text
 * @property string $wallet
 * @property string $txid
 * @property string $ip
 * @property string $created
 * @property string $time
 * @property string $vars [json]
 * @property string $system ["TRANSFER_SYSTEM"]
 * @property mixed $toObject
 * @property mixed $fromObject
 * @property string $currency [currency]
 * @package app\models
 */
class Transfer extends ActiveRecord
{
    public const CURRENCY = 'USD';
    public const BASE_CURRENCY = 'USD';
    public const RATE = 100;
    public const FACTOR = [
        'ETH' => 100000,
        'USD' => 100
    ];
    public const TYPE_PAYMENT = 'payment';
    public const STATUS_SUCCESS = 'success';

    public static function isCurrencyEnabled(string $name)
    {
        return true;
    }

    public static function getCurrencies()
    {
        return ['ETH', 'USD'];
    }

    public function attributes(): array
    {
        return array_merge(parent::attributes(), ['_from', '_to']);
    }

    public static $systems = [
        'perfect' => 'Perfect Money',
        'payeer' => 'Payeer',
        'advcash' => 'AdvCash',
    ];

    public static $currencies = [
//        'perfect' => 'USD',
//        'blockio' => 'BTC',
//        'ethereum' => 'ETH',
    ];

    public static $types = ['buy', 'payment', 'withdraw', 'accrue', 'bonus'];

    public function rules(): array
    {
        return [
            ['amount', 'integer'],
            ['created', 'datetime'],
            ['from', 'integer'],
            ['ip', 'string'],
            ['node', 'integer'],
            ['status', 'string'],
            ['text', 'string'],
            ['time', 'string'],
            ['to', 'integer'],
            ['txid', 'string'],
            ['type', 'string'],
            ['wallet', 'string'],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'amount' => Yii::t('app', 'Amount'),
            'created' => Yii::t('app', 'Time'),
            'status' => Yii::t('app', 'Operation'),
            'text' => Yii::t('app', 'Comment'),
            'type' => Yii::t('app', 'Type'),
            'user' => Yii::t('app', 'User'),
        ];
    }

    public static function keyBy($rows, $key = 'currency'): array
    {
        $result = [];
        foreach ($rows as $row) {
            $result[$row[$key]] = $row['amount'];
        }
        return $result;
    }

    /**
     * @param $user
     * @return array
     * @throws \yii\db\Exception
     */
    public static function getFinance($user): array
    {
        $rows = (new Query())->from('finance')
            ->where(['user' => $user])
            ->select(['type', 'currency', 'amount'])
            ->all();
        $finance = [];
        $additional = ['balance'];
//        $additional = ['balance', 'turnover', 'daily'];
        foreach (array_merge($additional, static::$types) as $type) {
            $finance[$type] = [];
            foreach (Transfer::getCurrencies() as $currency) {
                $finance[$type][$currency] = 0;
            }
        }
        foreach ($rows as $row) {
            $finance[$row['type']][$row['currency']] = $row['amount'];
        }
        foreach ($additional as $key) {
            $amount = SQL::queryAll("SELECT currency, amount FROM $key WHERE \"user\" = :user",
                [':user' => $user], PDO::FETCH_KEY_PAIR);
            if ($amount) {
                $finance[$key] = $amount;
            }
        }
        return $finance;
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public static function getExchange(): array
    {
        $exchange = [];
        foreach (SQL::queryAll('select * from exchange') as $row) {
            $exchange[$row['from']][$row['to']] = +$row['rate'];
        }
        return $exchange;
    }

    public function getFromObject(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'from']);
    }

    public function getToObject(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'to']);
    }
}
