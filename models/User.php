<?php
/**
 * Last modified: 19.01.16 09:46:45
 * Hash: eefb6ebd6f01671710f0e88a664e21991b5bbf48
 */

namespace app\models;


use app\helpers\SQL;
use app\models\traits\UserTrait;
use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

const REFERRALS = 'WITH RECURSIVE r(id, parent, level) AS (
    SELECT id, parent, 0 as level FROM "user" WHERE id = :id
    UNION
    SELECT u.id, u.parent, r.level + 1 as level FROM "user" u JOIN r ON r.parent = u.id
)
SELECT * FROM r';

/**
 * Class User
 * @property boolean $admin
 * @property integer $id
 * @property integer $parent
 * @property string $nick
 * @property string $surname
 * @property string $forename
 * @property string $email
 * @property string $skype
 * @property string $secret
 * @property string $password
 * @property string $pin
 * @property string $phone
 * @property string $country
 * @property string $settlement
 * @property string $address
 * @property string $blockio
 * @property string $type ["USER_TYPE"]
 * @property string $avatar [varchar(192)]
 * @property bool $geo [boolean]
 * @property int $created [timestamp(0)]
 * @property int $time [timestamp(0)]
 * @property int $swap_count
 * @property int $claim_count
 * @property mixed $programs
 * @property string $authKey
 * @property mixed $reward
 * @property string $auth [char(64)]
 * @property string $eth [varchar(42)]
 * @property string $telegram [varchar(32)]
 * @property User $parentObject
 * @property array $programNodes
 * @property string $site [varchar(192)]
 * @property bool $major [boolean]
 * @property int $last_activity [smallint]
 * @package app\models
 */
class User extends ActiveRecord implements IdentityInterface
{
    use UserTrait;

    public const TYPES = ['new', 'native', 'leader', 'special'];
    public const ROOT_NICK = 'ocean';
    public const SPONSOR_COOKIE = 'sponsor';
    public const SPONSOR_COOKIE_EXPIRES = 3600 * 24 * 30 * 12 * 10;

    public const LENGTH_EMAIL = 48;
    public const LENGTH_NAME = 48;
    public const LENGTH_NICK = 15;
    public const LENGTH_PIN = 4;
    public const LENGTH_PASSWORD = 100;
    public const LENGTH_TELEGRAM = 32;
    public const LENGTH_SKYPE = 32;

    public const SCENARIO_PROFILE = 'default';
    public const SCENARIO_REGISTER = 'register';
    public const SCENARIO_WALLET = 'wallet';

    public const REGEX_NICK = '/^[a-z][a-z0-9_\-]+(\.[a-z]+)?$/i';
    public const REGEX_PHONE = '/^\+?[ 0-9()\-]+$/';
    public const REGEX_SKYPE = '/^(live:)?[a-z][a-z0-9.,\-_]{5,31}$/i';
    public const REGEX_TELEGRAM = '/^[@+]?[a-z0-9_]+$/i';
    public const REGEX_PIN = '/^\d{4}$/';

    public $avatar_file;
    public $ip;
    public $level;
    public $password;
    public $repeat;
    public $sponsor;
    public $captcha;
    public $agree;

    public function rules(): array
    {
        $rules = [
            ['id', 'integer'],
            [['nick', 'email'], 'required'],
            [['password', 'repeat', 'sponsor'], 'required', 'on' => static::SCENARIO_REGISTER],
            [['surname', 'forename'], 'string', 'min' => 2, 'max' => static::LENGTH_NAME],
            ['password', 'string', 'min' => 6, 'max' => static::LENGTH_PASSWORD],
            ['nick', 'string', 'min' => 4, 'max' => static::LENGTH_NICK],
            ['email', 'string', 'min' => 4, 'max' => static::LENGTH_EMAIL],
            ['nick', 'match', 'pattern' => static::REGEX_NICK],
            ['phone', 'match', 'pattern' => static::REGEX_PHONE],
            ['skype', 'match', 'pattern' => static::REGEX_SKYPE],
            ['pin', 'match', 'pattern' => static::REGEX_PIN,
                'message' => Yii::t('app', 'Must be four digits')],
            ['telegram', 'match', 'pattern' => static::REGEX_TELEGRAM],
            ['pin', 'required', 'on' => static::SCENARIO_REGISTER, 'message' => Yii::t('app', 'Required')],
            ['email', 'email'],
            ['repeat', 'compare', 'compareAttribute' => 'password', 'message' => Yii::t('app', 'The passwords must match')],
            ['ip', 'ip'],
            ['agree', 'required', 'requiredValue' => 1,
                'message' => 'Please accept Terms and Conditions', 'on' => static::SCENARIO_REGISTER],
            ['type', 'in', 'range' => static::TYPES],
            ['telegram', 'string'],
            [['nick', 'email', 'skype', 'sponsor', 'phone', 'avatar', 'telegram', 'site', 'eth'],
                'filter', 'filter' => function ($s) {
                return strip_tags(trim($s ?: '')) ?: null;
            }],
            [['phone', 'forename', 'surname'], 'filter', 'filter' => function ($s) {
                return strip_tags(trim(preg_replace('/ {2,}/', ' ', $s ?: ''))) ?: null;
            }],
            ['telegram', 'filter', 'filter' => function ($s) {
                return empty($s) ? null : preg_replace('/^@/', '', trim($s));
            }],
            ['nick', 'unique',
                'message' => Yii::t('app', 'This login has already been taken'),
                'on' => static::SCENARIO_REGISTER],
            ['email', 'unique',
                'message' => Yii::t('app', 'This e-mail has been already taken'),
                'on' => static::SCENARIO_REGISTER],
            ['sponsor', 'exist',
                'targetAttribute' => 'nick',
                'message' => Yii::t('app', 'Sponsor not found')],

        ];
//        if (Yii::$app->has('reCaptcha')) {
//            $rules[] = ['captcha', 'required'];
//            $rules[] = ['captcha', ReCaptchaValidator::class,
//                'secret' => Yii::$app->reCaptcha->secret,
//                'uncheckedMessage' => Yii::t('app', 'Please confirm that you are not a bot.')];
//        }
        return $rules;
    }

    public function attributeLabels(): array
    {
        return [
            'agree' => Yii::t('app', 'I accept'),
            'avatar_file' => Yii::t('app', 'Avatar'),
            'email' => Yii::t('app', 'Email'),
            'forename' => Yii::t('app', 'First Name'),
            'nick' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
            'pin' => Yii::t('app', 'Financial password (4 digits)'),
            'repeat' => Yii::t('app', 'Repeat password'),
            'skype' => Yii::t('app', 'Skype'),
            'sponsor' => Yii::t('app', 'Sponsor'),
            'surname' => Yii::t('app', 'Last Name'),
        ];
    }

    public function validatePassword(string $password): bool
    {
        return password_verify($password, $this->secret);
    }

    /**
     * @return bool
     * @throws \yii\base\Exception
     */
    public function generateCode()
    {
        return $this->generateToken('code', Yii::$app->security->generateRandomString(48));
    }

    public function generateSecret()
    {
        $this->secret = str_replace('$2y$', '$2a$', password_hash($this->password, PASSWORD_BCRYPT));
    }

    public static function getIdByNick($nick): int
    {
        return static::find()
            ->where(['nick' => $nick])
            ->select(['id'])
            ->one()
            ->id;
    }

    /**
     * @param string $type
     * @param $id
     * @return null|string
     * @throws \yii\db\Exception
     */
    public function generateToken(string $type, $id): ?string
    {
        if (Yii::$app->db->createCommand()->insert('token', [
            'id' => $id,
            'type' => $type,
            'user' => $this->id,
        ])
            ->execute()
        ) {
            return $id;
        }
        return null;
    }

    public function isAdmin(): bool
    {
        return $this->admin;
    }

    public function __toString(): string
    {
        return $this->nick;
    }

    /**
     * @param string $template
     * @param array $params
     * @return bool
     */
    public function sendEmail(string $template, array $params): bool
    {
        $template = Yii::getAlias('@app') . "/mail/$template.php";
        return Yii::$app->mailer->compose()
            ->setTo($this->email)
            ->setFrom(['zbyszek@yopmail.com' => 'admin'])
            ->setSubject($params['subject'])
            ->setHtmlBody(Yii::$app->view->renderFile($template, $params))
            ->send();
    }

    /**
     * @param int $id
     * @param string $currency
     * @return int
     * @throws \yii\db\Exception
     */
    public static function getBalance($id = 0, $currency = Transfer::BASE_CURRENCY): int
    {
        if (!($id > 0)) {
            $id = Yii::$app->user->identity->id;
        }
        return SQL::scalar('select amount from balance where "user" = :id and currency = :currency', [
            ':id' => $id,
            ':currency' => $currency,
        ]);
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public function getReward(): array
    {
        return SQL::queryAll('WITH RECURSIVE r(id, parent, level) AS (
         SELECT id, parent, 0 as level FROM "user" WHERE id = :id
         UNION
         SELECT u.id, u.parent, r.level + 1 as level FROM "user" u JOIN r ON r.parent = u.id
         WHERE r.level < 150
        )
        SELECT * FROM r JOIN reward w ON r.level = w.level ORDER BY w.level',
            [':id' => $this->id]);
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public function getMajors(): array
    {
        return SQL::queryAll('WITH RECURSIVE r(id, parent, major, level) AS (
          SELECT id, parent, major, 0 as level FROM "user" WHERE id = :id
          UNION
          SELECT u.id, u.parent, u.major, r.level + 1 as level FROM "user" u JOIN r ON r.parent = u.id
          WHERE r.level < 150
        )
        SELECT id, level FROM r WHERE major = true and level > 0 ORDER BY id DESC',
            [':id' => $this->id]);
    }

    /**
     * @param int $program
     * @return Node
     * @throws \yii\db\Exception
     */
    public function getActiveMatrix(int $program): ?Node
    {
        $nodes = Node::find()
            ->where(['user' => $this->id, 'program' => $program])
            ->orderBy(['id' => SORT_DESC])
            ->limit(200)
            ->all();
        foreach ($nodes as $node) {
            if ($node->isMatrixActive()) {
                return $node;
            }
        }
        return null;
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public function getProgramNodes(): array
    {
        $array = SQL::queryAll('with a as (select
                         max(id) as node,
                         program
                       from node n
                       where n."user" = :user
                       group by program)
            select
              p.id,
              p.name,
              p.price,
              a.node,
              not c.closed as active
            from program_view p left join a on p.id = a.program
              left join matrix_count c on a.node = c.id
            order by p.id', [
            ':user' => $this->id
        ]);
        $indexed = [];
        foreach ($array as $program) {
            $indexed[$program['id']] = $program;
        }
        $nodes = [];
        foreach ($indexed as $program) {
            if (1 === $program['id'] % 2) {
                $next = $indexed[$program['id'] + 1] ?? null;
                if ($next && $next['active'] && $next['node'] > $program['node']) {
                    $program = $next;
                }
                $nodes[$program['id']] = $program;
            }
        }
//        $nodes['array'] = $array;
        return $nodes;
    }

    public function canLogin(): bool
    {
        return true;
//        return Record::find()->andWhere([
//            'object_id' => $this->id,
//            'event' => 'login_fail'
//        ])
//            ->andWhere('time > DATE_SUB(NOW(), INTERVAL 5 MINUTE)')
//            ->count() < 10;
    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return User the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null): User
    {
        $t = Token::findOne(['id' => $token]);
        if ($t && $t->user > 0) {
            $user = $t->userObject;
            if ($user && $user->admin) {
                return $user;
            }
        }
        return null;
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|int an ID that uniquely identifies a user identity.
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey(): string
    {
        return $this->auth;
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return bool whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey): bool
    {
        return $authKey === $this->auth;
    }

    /**
     * Finds an identity by the given ID.
     * @param string|int $id the ID to be looked for
     * @return IdentityInterface the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @throws \yii\base\Exception
     */
    public function generateAuthKey()
    {
        $this->auth = Yii::$app->security->generateRandomString(64);
    }

    public function getParentObject()
    {
        return $this->hasOne(static::class, ['id' => 'parent']);
    }

    public function getNodes()
    {
        return $this->hasMany(Node::class, ['user' => 'id']);
    }

    /**
     * @throws \yii\db\Exception
     */
    public function getUnseenSchools()
    {
        $seen = SQL::scalar('select count(*) from activity_seen where "user" = :user', [
            ':user' => $this->id
        ]);
        $count = SQL::scalar('select count(*) from activity');
        return $count - $seen;
    }
}
