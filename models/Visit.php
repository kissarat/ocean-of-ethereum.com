<?php
/**
 * Last modified: 18.07.03 05:38:11
 * Hash: 71e40655de2971d8af2585a0465d5490892fee75
 */

namespace app\models;


use yii\db\ActiveRecord;

/**
 * Class Visit
 * @package app\models
 * @property string $id [integer]
 * @property string $token [varchar(240)]
 * @property string $url [varchar(240)]
 * @property string $spend [integer]
 * @property string $ip [inet]
 * @property string $agent [varchar(240)]
 * @property int $created [timestamp]
 */
class Visit extends ActiveRecord
{

}
