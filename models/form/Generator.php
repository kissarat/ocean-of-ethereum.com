<?php
/**
 * Last modified: 18.10.06 04:31:52
 * Hash: 3862640b5989f285e97d5327f9ee339f2f082a0a
 */

namespace app\models\form;


use app\helpers\SQL;
use app\helpers\Utils;
use app\models\GeneratedUser;
use app\models\Program;
use app\models\Token;
use app\models\Transfer;
use app\models\User;
use Faker\Factory;
use Yii;
use yii\base\Model;

class Generator extends Model
{
    public $sponsor;
    public $count;
    public $eth;
    public $token;
    public $program;

    public function rules(): array
    {
        return [
            [['sponsor', 'count'], 'trim'],
            [['sponsor', 'count'], 'required'],
            [['program', 'eth'], 'number'],
            ['sponsor', 'exist',
                'targetAttribute' => 'nick',
                'targetClass' => User::class,
                'message' => Yii::t('app', 'Sponsor not found')],
//            ['created', 'date', 'format' => 'php:Y-m-d'],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'sponsor' => 'Спонсор',
            'count' => 'Количество',
            'eth' => 'Баланс',
            'program' => 'Программа',
        ];
    }

    /**
     * @return array
     * @throws \yii\base\InvalidArgumentException
     * @throws \yii\base\Exception
     * @throws \yii\web\HttpException
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     * @throws \RuntimeException
     * @throws \Error
     * @throws \Exception
     * @throws \Throwable
     */
    public function generate(): array
    {
        if (!$this->token) {
            $token = new Token([
                'id' => Utils::generateCode(24),
                'type' => 'browser',
                'user' => Yii::$app->user->identity->id
            ]);
            if ($token->save(false)) {
                $this->token = $token->id;
            }
        }

        $factory = Factory::create();
        $parent = User::findOne(['nick' => $this->sponsor]);
        if (!$parent) {
            $parent = 1;
        }
        $users = [];
        for ($i = 0; $i < $this->count; $i++) {
            $nick = preg_replace('/[^\w_]/', '_', substr($factory->userName, 0, 23));
            $user = new GeneratedUser([
                'nick' => $nick,
                'email' => $nick . '@mail.local',
                'parent' => $parent->id,
                'type' => 'native',
                'pin' => '1111',
                'secret' => '$2a$10$erdYxoXcL4sRTMIVUOshQOcecB.rDvi4IezX4OlNeYyr2k05rtaqG'
            ]);
            if ($user->save(false)) {
                $users[$user->id] = $user;
            } else {
                $errors = $user->getErrors();
                Yii::$app->session->addFlash('error', Utils::json($errors));
            }
        }
        $results = [];
        $program = $this->program > 0 ? Program::findOne($this->program) : null;
        foreach ($users as $id => $user) {
            if ($this->eth > 0) {
                $amount = (int)floor($this->eth * Transfer::RATE);
                $transfer = new Transfer([
                    'to' => $id,
                    'type' => 'payment',
                    'status' => 'success',
                    'amount' => $amount,
                    'currency' => Transfer::CURRENCY
                ]);
                if ($transfer->save()) {
                    if ($program) {
                        $node = Program::tryOpen($program->id, $parent);
                        if ($node && $amount >= $program->price) {
                            $free = SQL::queryOne('select id, "count" from free_parent where root = :id order by level asc, cell desc limit 1', [
                                ':id' => $node->id
                            ]);
                            $node_id = Program::buy($free['id'], $user->id, $free['count'] > 0 ? 1 : 0);
                            if ($node_id > 0) {
                                $user->program = $program;
                            }
                        }
                    }
                    $results[] = $user;
                } else {
                    throw new \RuntimeException(Utils::json($transfer->getErrors()));
                }
            }
        }
        return $results;
    }
}
