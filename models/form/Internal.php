<?php
/**
 * Last modified: 18.11.21 00:02:30
 * Hash: d4c691e24932b0f4e5d3635f1a1c1aa26f1b673a
 */


namespace app\models\form;


use app\models\Transfer;
use app\models\User;
use Yii;
use yii\base\Model;

/**
 *
 * @property int $cent
 * @property \app\models\User $recipient
 */
class Internal extends Model
{
    public $nick;
    public $amount;
    public $pin;
    public $text;

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['nick', 'pin', 'amount'], 'required'],
            ['amount', 'number', 'min' => '0.01'],
            ['pin', 'match', 'pattern' => User::REGEX_PIN,
                'message' => Yii::t('app', 'Must be four digits')],
            [['nick', 'text'], 'filter', 'filter' => function ($s) {
                return strip_tags(trim($s ?: '')) ?: null;
            }],
            ['text', 'string', 'max' => 200],
            ['nick', 'exist',
                'targetAttribute' => 'nick',
                'targetClass' => User::class,
                'message' => Yii::t('app', 'Recipient not found')],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'nick' => Yii::t('app', 'Recipient'),
            'amount' => Yii::t('app', 'Amount'),
            'pin' => Yii::t('app', 'Financial password'),
            'text' => Yii::t('app', 'Description'),
        ];
    }

    /**
     * @return int
     */
    public function getCent(): int
    {
        return $this->amount * Transfer::RATE;
    }

    /**
     * @param int $user_id
     * @return bool
     * @throws \yii\db\Exception
     */
    public function can(int $user_id): bool
    {
        return $this->getCent() <= User::getBalance($user_id);
    }

    /**
     * @return User
     */
    public function getRecipient(): User
    {
        return User::findOne(['nick' => $this->nick]);
    }

    public function checkPin(?User $user = null) {
        if (!$user) {
            $user = Yii::$app->user->identity;
        }
        if ($user->pin === $this->pin) {
            return true;
        }
        $this->addError('pin', Yii::t('app', 'Invalid financial password'));
        return false;
    }
}
