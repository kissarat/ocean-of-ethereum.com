<?php
/**
 * Last modified: 18.12.10 23:53:14
 * Hash: 49b9ae7cd89c05b1546ae489175825e7e3cc3424
 */

namespace app\models\form;

use app\models\User;
use Yii;
use yii\base\Model;

/**
 *
 * @property int $rate
 */
class Invoice extends Model
{
    public $system;
    public $amount;

    public function rules(): array
    {
        return [
            ['amount', 'number', 'min' => 1],
            ['amount', 'required'],
            ['system', 'string']
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'amount' => Yii::t('app', 'Amount'),
            'system' => Yii::t('app', 'Payment system'),
        ];
    }

    public static function getPaymentSystems() {
        return [
            'perfect' => 'Perfect Money',
//            'advcash' => 'AdvCash',
//            'payeer' => 'Payeer'
        ];
    }

    public function checkPin(?User $user = null) {
        if (!$user) {
            $user = Yii::$app->user->identity;
        }
        if ($user->pin === $this->pin) {
            return true;
        }
        $this->addError('pin', Yii::t('app', 'Invalid financial password'));
        return false;
    }
}
