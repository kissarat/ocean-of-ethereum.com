<?php
/**
 * Last modified: 18.07.26 02:27:49
 * Hash: ee5120a2df860441ddc363b5d9ff0b4ce306178b
 */

namespace app\models\form;


use app\models\User;
use Yii;
use yii\base\Model;

/**
 * @property string $login
 * @property string $password
 * @property User $user
 */
class Login extends Model
{
    public $ip;
    public $login;
    public $password;

    public function rules(): array
    {
        return [
            [['login', 'password'], 'required'],
            ['login', 'string', 'min' => 4, 'max' => 48],
            ['password', 'string', 'min' => 1],
            ['ip', 'ip']
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'login' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
        ];
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return User::findOne(filter_var($this->login, FILTER_VALIDATE_EMAIL)
            ? ['email' => $this->login]
            : ['nick' => $this->login]);
//            ->select(['id', 'secret']);
    }

    public static function checkLoginAttempt($where): bool
    {
        return true;
//        $count = Log::find()
//            ->where($where)
//            ->where(['>', 'id', Log::nanotime() - 15 * 60 * 1000000000])
//            ->where([
//                'entity' => 'auth',
//                'action' => 'invalid'
//            ])
//            ->count();
//        if ($count > 5) {
//            Yii::$app->session->addFlash('error',
//                Yii::t('app', 'You have exceeded the number of allowed login attempts. Please try again later.'));
//            return false;
//        }
    }
}
