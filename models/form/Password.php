<?php
/**
 * Last modified: 18.10.06 17:49:43
 * Hash: b9ba5ae7bdc36b2ab92794f60525bd0eed302f28
 */

namespace app\models\form;


use Yii;
use yii\base\Model;

/**
 * Class Password
 * @package app\models
 */
class Password extends Model
{
    public $current;
    public $ip;
    public $new;
    public $repeat;
    public $user_name;

    public function rules(): array
    {
        return [
            [['current', 'new', 'repeat'], 'required'],
            ['ip', 'ip'],
            ['repeat', 'compare', 'compareAttribute' => 'new', 'message' => Yii::t('app', 'The passwords must match')]
        ];
    }

    public function scenarios(): array
    {
        return [
            'default' => ['current', 'new', 'repeat'],
            'reset' => ['new', 'repeat'],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'current' => Yii::t('app', 'Current password'),
            'new' => Yii::t('app', 'New password'),
            'repeat' => Yii::t('app', 'Repeat password'),
        ];
    }
}
