<?php
/**
 * Last modified: 18.10.06 17:50:51
 * Hash: 9cb064c15bfc4cc64e8aa9736a18d54d27288a33
 */

namespace app\models\form;


use Yii;
use yii\base\Model;

class PasswordReset extends Model
{
    public $password;
    public $repeat;

    public function rules(): array
    {
        return [
            [['password', 'repeat'], 'required'],
            ['repeat', 'compare', 'compareAttribute' => 'password', 'message' => Yii::t('app', 'The passwords must match')]
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'password' => Yii::t('app', 'New password'),
            'repeat' => Yii::t('app', 'Repeat new password')
        ];
    }
}
