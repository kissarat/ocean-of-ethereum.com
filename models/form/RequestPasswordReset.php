<?php
/**
 * Last modified: 18.10.06 16:05:41
 * Hash: 7943253f613847b998ef3e7ff342306e69285643
 */

namespace app\models\form;


use app\models\User;
use Yii;
use yii\base\Model;

/**
 *
 * @property \app\models\User|bool $user
 */
class RequestPasswordReset extends Model
{
    public $login;

    public function attributeLabels(): array
    {
        return [
            'login' => Yii::t('app', 'Enter Email or Username'),
        ];
    }

    public function rules()
    {
        return [
            ['login', 'required'],
            ['login', 'trim'],
        ];
    }

    public function getUser(): ?User
    {
        return User::findOne(filter_var($this->login, FILTER_VALIDATE_EMAIL)
            ? ['email' => $this->login]
            : ['nick' => $this->login]);
    }
}
