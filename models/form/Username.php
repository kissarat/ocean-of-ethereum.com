<?php
/**
 * Last modified: 18.07.26 21:48:12
 * Hash: 042d34eb66319802c95174847f5a50489b680657
 */

namespace app\models\form;


use Yii;
use yii\base\Model;
use app\models\User;

class Username extends Model
{
    public const REGEX = User::REGEX_NICK;
    public $nick;

    public function rules(): array
    {
        return [
            ['nick', 'filter', 'filter' => 'trim'],
            ['nick', 'match', 'pattern' => static::REGEX],
            ['nick', 'exist',
                'targetClass' => User::class,
                'targetAttribute' => 'nick',
                'message' => Yii::t('app', 'User not found')]
        ];
    }
}
