<?php
/**
 * Last modified: 18.12.10 23:54:51
 * Hash: bc9c540bccb6b165e8fe00231de93608cf203e22
 */

namespace app\models\form;

use app\components\Perfect;
use app\models\User;
use Yii;

/**
 * Class Withdraw
 * @property string wallet
 * @property string text
 * @package app\models\form
 */
class Withdraw extends Invoice
{
    public $pin;
    public $text;
    public $wallet;

    public static $minimal = [
        'ETH' => 100,
    ];

    public function rules(): array
    {
        $rules = parent::rules();
        $rules[] = ['text', 'string', 'length' => [0, 190]];
        $rules[] = [['wallet', 'pin'], 'required'];
        $rules[] = ['wallet', 'string'];
        $rules[] = ['wallet', 'match', 'pattern' => Perfect::WALLET_REGEX];
        $rules[] = ['pin', 'match',
            'pattern' => User::REGEX_PIN,
            'message' => Yii::t('app', 'Must be four digits')];
        $rules[] = ['pin', 'exist',
            'targetAttribute' => 'pin',
            'targetClass' => User::class,
            'message' => Yii::t('app', 'Invalid financial password')];
        return $rules;
    }

    public function attributeLabels(): array
    {
        $labels = parent::attributeLabels();
        $labels['text'] = Yii::t('app', 'Comment');
        $labels['wallet'] = Yii::t('app', 'Wallet');
        $labels['pin'] = Yii::t('app', 'Financial password');
        return $labels;
    }
}
