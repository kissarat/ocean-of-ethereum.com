<?php
/**
 * Last modified: 18.09.15 02:43:23
 * Hash: 43619867f03160db4a7407fe1de7317802ce155a
 */


namespace app\models\traits;


use app\helpers\SQL;

trait UserTrait
{
    public function getName(bool $empty = false): string
    {
        $nick = $empty ? '' : $this->nick;
        return empty($this->forename) && empty($this->surname) ? $nick : trim($this->forename . ' ' . $this->surname);
    }

    /**
     * @return int
     * @throws \yii\db\Exception
     */
    public function getOffersCount() : int {
        return SQL::scalar('select count(*) from offer where used is null and "user" = :user', [
            ':user' => $this->id
        ]);
    }

    /**
     * @return int
     * @throws \yii\db\Exception
     */
    public function getSwapsCount() : int {
        return SQL::scalar('select count(*) from swap where approved is null and (from_user = :user or to_user = :user)', [
            ':user' => $this->id
        ]);
    }

    /**
     * @return int
     * @throws \yii\db\Exception
     */
    public function getClaimsCount() : int {
        return SQL::scalar('select count(*) from claim where ("from" = :user or "to" = :user)
             and ((from_approved is null and to_approved is null)
             or (from_approved = true and to_approved is null)
             or (from_approved is null and to_approved = true)
             )', [
            ':user' => $this->id
        ]);
    }
}
