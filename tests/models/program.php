<?php
/**
 * Last modified: 18.06.29 15:22:31
 * Hash: 8ec7d5bc62b353a53c15a1e67b6942a080f20a83
 */

require_once '../../console/environment.php';

use app\models\Program;
use app\models\User;

$node = Program::tryOpen(11, User::findOne(1));
echo $node ? 1 : 0;
