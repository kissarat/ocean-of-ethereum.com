"""
Last modified: 18.05.09 23:48:54
Hash: ba9c1aac1aa353b0ec1acc66e507e0e093705321
"""
from json import dumps
from sys import argv

from store import Subject
from yii2 import Client, user_agent, config

command = argv[1] if len(argv) >= 2 else ''

if 'id' == command:
    print(config['id'])
elif 'me' == command:
    s = Subject()
    user = s.find_one()
    print(user['nick'])
elif 'get' == command:
    c = Client()
    conn = c.request('GET', argv[2], None)
    print(conn.headers)
    print(conn.read().decode('utf-8'))
elif 'config' == command:
    print(dumps(config, ensure_ascii=False, indent=2))
else:
    print(user_agent)
