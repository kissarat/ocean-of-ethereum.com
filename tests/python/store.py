"""
Last modified: 18.05.09 23:15:24
Hash: 17f939b965a91e98ac8b7a2d42b0309d5f61c82e
"""
from urllib.parse import urlencode

from yii2 import Client


class Store:
    def __init__(self, table, client=None):
        self.client = client or Client()
        self.table = table

    def find_one(self, **where):
        return self.client.get('/%s/view?%s' % (self.table, urlencode(where)))


class Subject(Store):
    def __init__(self, client=None):
        super().__init__('subject', client)

    def find_one(self, **where):
        if 0 == len(where):
            where = {'id': self.client.user['id']}
        return super().find_one(**where)


class Money(Store):
    def __init__(self, client=None):
        super().__init__('money', client)
