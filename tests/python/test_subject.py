"""
Last modified: 18.05.09 23:15:24
Hash: 17f939b965a91e98ac8b7a2d42b0309d5f61c82e
"""
import unittest

from yii2 import Client, authorized


class SubjectTestCase(unittest.TestCase):
    def setUp(self):
        self.client = Client()

    def test_login(self):
        self.assertTrue(self.client.login(), 'Login')

    @authorized
    def test_logout(self):
        self.assertTrue(self.client.logout(), 'Logout')


if __name__ == '__main__':
    unittest.main()
