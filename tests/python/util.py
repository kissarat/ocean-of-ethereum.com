"""
Last modified: 18.05.09 18:21:09
Hash: d17a244cd8adca750cc356fa6ced29ee013f87ce
"""
from os import getenv
from os.path import dirname
from subprocess import Popen, PIPE
from time import strftime

ROOT = dirname(__file__) + '/../..'
SITE = getenv('SITE', ROOT)
TIME_FORMAT = '%y.%m.%d %H:%M:%S'
now_formatted = strftime(TIME_FORMAT)


def read_run(*args, **env):
    with Popen(args, stdout=PIPE, env=env) as p:
        return p.stdout.read().decode('utf-8')


def php(*args, **env):
    return read_run('php', *args, **env)


def git(*args, **env):
    return read_run('git', *args, **env).split("\n")
