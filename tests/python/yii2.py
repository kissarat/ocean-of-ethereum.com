"""
Last modified: 18.05.09 20:13:35
Hash: 586176de7ffd2f0eb97c4f1aa2c7e6d47997de8b
"""
import platform
from http.client import HTTPConnection
from json import loads, load
from re import compile
from urllib.parse import urlencode

from util import SITE

cookie_pattern = compile(r'^([\w]+)=([^;]+)')

config = None
with open(SITE + '/config/test.json', encoding='utf-8') as f:
    config = load(f)

user_agent = platform.node() + ' ' + platform.version() + \
             ' theJodoor1Quaof6die3Sei1='


def authorized(fun):
    def wrapper(self):
        if not self.client.authorized:
            self.assertTrue(self.client.login(), 'Cannot authorize')
        fun(self)

    return wrapper


class Client:
    def __init__(self):
        self.origin = config['params']['hostname']
        self.cookies = {}
        self.authorized = False
        self.user = {
            'id': 4,
            'nick': 'demo',
            'password': 'demo'
        }
        self.connection = None
        self.headers = {
            'Accept': 'application/json',
            'User-Agent': user_agent + config['params']['database_name']
        }

    @staticmethod
    def pack_form(name, params):
        result = {}
        for k in params:
            result['%s[%s]' % (name, k)] = params[k]
        return urlencode(result)

    @staticmethod
    def read_response(response):
        return loads(response.read().decode('utf-8'))

    def parse_cookies(self, headers):
        for k in headers:
            if 'Set-Cookie' == k:
                m = cookie_pattern.match(headers[k])
                self.cookies[m.group(1)] = m.group(2)

    def add_cookies(self, headers):
        cookies = []
        if any(self.cookies):
            for k in self.cookies:
                cookies.append(k + '=' + self.cookies[k])
            headers['Cookie'] = '; '.join(cookies)

    def get_headers(self, headers):
        for k in self.headers:
            headers[k] = self.headers[k]
        self.add_cookies(headers)
        return headers

    def connect(self):
        if self.connection:
            self.connection.close()
        self.connection = HTTPConnection(self.origin)
        return self.connection

    def request(self, method, url, data=None, headers={}):
        headers = self.get_headers(headers)
        if 'POST' == method:
            headers['Content-Type'] = 'application/x-www-form-urlencoded'
        conn = self.connect()
        conn.request(method, url, data, headers)
        res = conn.getresponse()
        self.cookies = self.cookies
        self.parse_cookies(res.headers)
        return res

    def post(self, url, data, headers={}):
        return self.request('POST', url, urlencode(data), headers)

    def get(self, url):
        res = self.request('GET', url)
        return self.read_response(res)

    def post_form(self, url, name, data):
        return self.request('POST', url, self.pack_form(name, data))

    def login(self):
        res = self.post_form('/user/login', 'Login', {
            'login': self.user['nick'],
            'password': self.user['password']
        })
        self.authorized = 302 == res.status
        return self.authorized

    def logout(self):
        res = self.request('GET', '/user/logout')
        self.authorized = not 302 == res.status
        return not self.authorized

    def __exit__(self):
        self.connection.close()

    def __del__(self):
        self.connection.close()
