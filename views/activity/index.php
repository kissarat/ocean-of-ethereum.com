<?php

use yii\grid\GridView;
use app\models\Activity;
use app\helpers\Html;

?>

<div class="activity index">
    <h1><?= Yii::t('app', 'Current activities') ?></h1>
    <?= GridView::widget([
        'dataProvider' => $provider,
        'columns' => [
            'id',
            [
                'attribute' => 'start',
                'value' => function (Activity $model) {
                    return explode(" ", $model->start)[0];
                }
//                'contentOptions' => function ($model) {
//                    return ['data-time' => strtotime($model->created)];
//                }
            ],
            'title',
            'description',
            'speaker',
            [
                'attribute' => 'online_url',
                'format' => 'html',
                'value' => function (Activity $model) {
                    if ($model->online_url) {
                        return Html::a(Yii::t('app', 'View'), $model->online_url);
                    }
                    return '';
                }
            ],
            [
                'attribute' => 'recording_url',
                'format' => 'html',
                'value' => function (Activity $model) {
                    if ($model->recording_url) {
                        return Html::a(Yii::t('app', 'View'), $model->recording_url);
                    }
                    return '';
                }
            ]
        ]
    ]) ?>
</div>
