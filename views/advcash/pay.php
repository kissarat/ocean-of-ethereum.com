<?php

/**
 * Last modified: 18.08.02 23:13:56
 * Hash: d2b556aab9e1df616d64210724578ef08cb39ccc
 * @var $amount string
 * @var $this \yii\web\View
 * @var $guid string
 * @var $to int
 */

use yarcode\advcash\RedirectForm;

echo RedirectForm::widget([
    'api' => Yii::$app->get('advcash'),
    'invoiceId' => $guid,
    'amount' => $amount
]);
