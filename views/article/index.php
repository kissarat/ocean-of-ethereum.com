<?php
/**
 * Last modified: 18.07.20 07:26:07
 * Hash: 9e80c40453ffab31dd2baa60803f152303e0868e
 */
/* @var $models \app\models\Article[] */

/* @var $this \yii\web\View */

use app\helpers\Html;

?>

<div class="article index">
    <?php foreach ($models as $model): ?>
        <article class="article" itemscope itemtype="https://schema.org/NewsArticle">
            <div itemprop="identifier"><?= $model->id ?></div>
            <h2 itemprop="headline"><?= Html::a($model->name, ['article/view', 'id' => $model->id]) ?></h2>
            <div itemprop="description"><?= $model->name ?></div>
        </article>
    <?php endforeach ?>
</div>
