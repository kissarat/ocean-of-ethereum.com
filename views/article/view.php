<?php
/**
 * Last modified: 18.07.20 07:21:37
 * Hash: 7485625af70ae824cefb6a30569a9e45e4052877
 */
/* @var $this \yii\web\View */
/* @var $model \app\models\Article */

use app\helpers\Html;

$this->title = $model->name;
$this->registerMetaTag(['name' => 'description', 'content' => $model->short]);
?>
<article class="article view">
    <div itemprop="identifier"><?= $model->id ?></div>
    <h1 itemprop="headline"><?= $model->name ?></h1>
    <div class="cover" itemscope itemtype="http://schema.org/ImageObject">
        <?= Html::img('/files/rNews-Sample-Story/libya_sample_reuters.jpg', [
            'itemprop' => 'contentURL'
        ]) ?>
    </div>
    <div>
        <!--meta itemprop="datePublished" content="<?= date('Y-m-d', strtotime($model->created)) ?>"/-->
        <div itemprop="description">
            <?= $model->short ?>
        </div>
        <div itemprop="articleBody">
            <?= $model->text ?>
        </div>
    </div>
</article>
