<?php

/**
 * Last modified: 18.10.14 02:39:38
 * Hash: ea7ee1eab8fb3c527056ed8d1f3aa15c4361ac2f
 */

use app\helpers\Html;
use app\models\Claim;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this \yii\web\View */
/* @var $can bool */
/* @var $provider \yii\data\ActiveDataProvider */
/* @var $model Claim */

function status(bool $accepted)
{
    return Html::tag('span', Yii::t('app', $accepted ? 'Accepted' : 'Rejected'),
        ['class' => $accepted ? 'status accepted' : 'status rejected']);
}

?>
<div class="claim index">
    <h1><?= Yii::t('app', 'Sponsor change request') ?></h1>
    <div class="alert alert-info">
        <?= Yii::t('app', 'Number of swaps is <span data-number="{number}">{number}</span>', [
            'number' => Yii::$app->user->identity->claim_count - $count
        ]) ?>
    </div>
    <?php
    if ($can):
        $form = ActiveForm::begin()
        ?>
        <?= $form->field($model, 'to_nick') ?>
        <button type="submit"><?= Yii::t('app', 'Change sponsor') ?></button>
        <?php
        ActiveForm::end();
    endif;
    ?>
    <ul>
        <?= /** @noinspection PhpUnhandledExceptionInspection */
        GridView::widget([
            'dataProvider' => $provider,
            'rowOptions' => function (Claim $model) {
                $class = $model->getIsPending() ? 'pending' : ($model->getIsAccepted() ? 'accepted' : 'rejected');
                return ['class' => 'status ' . $class];
            },
            'columns' => [
                [
                    'attribute' => 'created',
                    'contentOptions' => function (Claim $model) {
                        return ['data-time' => strtotime($model->created)];
                    }
                ],
                [
                    'attribute' => 'user',
                    'value' => function (Claim $model) {
                        return $model->userObject->nick;
                    }
                ],
                [
                    'attribute' => 'from',
                    'contentOptions' => ['class' => 'subject'],
                    'format' => 'html',
                    'value' => function (Claim $model) {
                        $html = $model->fromObject->nick;
                        if (is_bool($model->from_approved)) {
                            $html .= status($model->from_approved);
                        }
                        return $html;
                    }
                ],
                [
                    'attribute' => 'to',
                    'format' => 'html',
                    'contentOptions' => ['class' => 'subject'],
                    'value' => function (Claim $model) {
                        $html = $model->toObject->nick;
                        if (is_bool($model->to_approved)) {
                            $html .= status($model->to_approved);
                        }
                        return $html;
                    }
                ],
                [
                    'label' => Yii::t('app', 'Confirmation'),
                    'contentOptions' => ['class' => 'time'],
                    'format' => 'html',
                    'value' => function (Claim $model) {
                        if ($model->getIsPending()) {
                            $isReceiver = $model->to === Yii::$app->user->getId();
                            if ($model->user === Yii::$app->user->getId() || is_bool($isReceiver ? $model->to_approved : $model->from_approved)) {
                                return Html::tag('span', Yii::t('app', 'Pending'));
                            }
                            $subject = $isReceiver ? Claim::TO_SUBJECT : Claim::FROM_SUBJECT;
                            return Html::tag('span', implode("\n", [
                                Html::a(Yii::t('app', 'Accept'),
                                    ['claim/answer', 'id' => $model->id, 'subject' => $subject, 'approve' => true], ['class' => 'accept']),
                                Html::a(Yii::t('app', 'Reject'),
                                    ['claim/answer', 'id' => $model->id, 'subject' => $subject, 'approve' => false], ['class' => 'reject'])
                            ]), ['class' => 'pending']);
                        }
                        return status($model->getIsAccepted()) . Html::tag('span', strtotime($model->time), ['class' => 'unix-time']);
                    }
                ],
                [
                    'label' => Yii::t('app', 'Actions'),
                    'format' => 'html',
                    'value' => function (Claim $model) {
                        if (Yii::$app->user->identity->id === $model->user && $model->getIsPending()) {
                            return Html::a(Yii::t('app', 'Remove'), ['claim/remove', 'id' => $model->id]);
                        }
                        return '';
                    }
                ]
            ]
        ]) ?>
    </ul>
</div>
