<?php

use app\helpers\Html;

/** @var boolean $isVIP */
/** @var \app\models\Program[] $models */
?>
<div class="course index">
    <h1><?= Yii::t('app', 'Courses') ?></h1>
    <ul>
        <?php foreach ($models as $model): ?>
            <li class="course item" data-id="<?= $model->id ?>">
                <div class="image">
                    <?= Html::img($model->image, ['class' => 'field image']) ?>
                </div>
                <div class="about">
                    <?= Html::a($model->title, $model->about, ['class' => 'field title', 'target' => '_blank']) ?>
                    <?php
                    if ($model->downloaded || $can) {
                        echo Html::a('Выбрать', ['course/download', 'id' => $model->id], [
                            'class' => 'button download',
                            'target' => '_blank'
                        ]);
                    } else {
                        echo Html::tag('div', Yii::t('app', 'You cannot download this course'), ['class' => 'cannot-download']);
                    }
                    ?>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
</div>
