<?php
/**
 * Last modified: 18.10.06 04:32:33
 * Hash: d03c1615a92ae47d533d02afecd9a7b5677f99c5
 */

use app\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;

/** @var \yii\data\ArrayDataProvider $provider */
/* @var $this \yii\web\View */
/* @var $model \app\models\form\Generator */
?>
<div class="generate user">
    <?php $form = ActiveForm::begin() ?>

    <?= $form->field($model, 'sponsor')->textInput(['data-store' => true]) ?>
    <?= $form->field($model, 'count')->textInput(['data-store' => true]) ?>
    <?= $form->field($model, 'eth')->textInput(['data-store' => true]) ?>
    <?= $form->field($model, 'token')
        ->hiddenInput(['data-store' => true])
        ->label(false) ?>
    <?= $form->field($model, 'program')
        ->dropDownList([
            0 => 'Не куплять',
            11 => 'CRAB Stream',
            12 => 'CRAB Lagoon',
            13 => 'FISH Stream',
            14 => 'FISH Lagoon',
            15 => 'DOLPHIN Stream',
            16 => 'DOLPHIN Lagoon',
            17 => 'SHARK Stream',
            18 => 'SHARK Lagoon',
            19 => 'WHALE Stream',
        ], [
            'data-store' => true
        ]) ?>
    <div>Финансовый пароль: 1111</div>
    <?= Html::submitButton('Generate') ?>
    <?= Html::a('Почистить базу', ['generate/clear']) ?>
    <!--    <button type="button" id="remove" class="btn-danger">Удалить проект</button>-->
    <?php ActiveForm::end(); ?>

    <div class="list">
        <?php
        /** @noinspection PhpUnhandledExceptionInspection */
        echo GridView::widget([
            'emptyText' => 'Ничего не сгенерировано',
            'dataProvider' => $provider,
            'columns' => [
                'id',
                'nick',
                [
                    'label' => 'Программа',
                    'value' => function ($user) {
                        return $user->program ? $user->program->name : null;
                    }
                ],
                [
                    'label' => 'Вход',
                    'format' => 'html',
                    'value' => function ($user) use ($model) {
                        return Html::a($user->nick, ['/user/authorize',
                            'nick' => $user->nick,
                            'token' => $model->token
                        ]);
                    }
                ]
            ]
        ]) ?>
    </div>
</div>
