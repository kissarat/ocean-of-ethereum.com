<?php

use app\helpers\Html;
use app\models\User;

/**
 * @var User $user
 */
$user = Yii::$app->user->identity;
?>
<div class="home banner">
    <h1><?= Yii::t('app', 'Banners') ?></h1>
    <?php
    for ($i = 1; $i <= 10; $i++):
        $image = "https://office.info0cean.com/img/banners/banner$i.gif";
        $banner = Html::a(Html::img($image, ['alt' => Yii::$app->name]), Yii::$app->params['referralLinkOrigin'] . '/~' . $user->nick);
        ?>
        <div class="item">
            <?= $banner ?>
            <br/>
            <textarea cols="100"><?= Html::encode($banner) ?></textarea>
        </div>
    <?php
    endfor;
    ?>
</div>
