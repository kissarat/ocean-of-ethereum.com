<?php
/**
 * Last modified: 18.08.01 04:38:37
 * Hash: 2fe54d64a553a297770787ab0a1ffa0e1ca1ef7a
 */
/* @var $this \yii\web\View */
/* @var $error array */
/* @var $status int */

use app\helpers\Html;

?>

<div class="home error">
    <?= Yii::$app->user->getIsGuest() ? '' : Html::tag('h1', Yii::t('app', Html::encode($this->title))) ?>
    <div><?= $error['message'] ?></div>
</div>
