<?php
/**
 * Last modified: 18.07.03 05:38:12
 * Hash: 739b895e6c972fbf0cfb53b7253305a6cb664fad
 */

use yii\helpers\Html;

/**
 * @var Error $exception
 */

?>
<div class="home-exception">
    <h1><?= Html::encode($this->title) ?></h1>
    <div><?= $exception->getMessage() ?></div>
    <div><?= $exception->getFile() ?></div>
    <pre><?= $exception->xdebug_message ?></pre>
</div>
