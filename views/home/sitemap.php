<?php
/**
 * Last modified: 18.07.03 05:38:11
 * Hash: 8b776504931c82613413569123566c5aaeee65a2
 */
?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>/register</loc>
        <lastmod>2017-11-20</lastmod>
        <changefreq>monthly</changefreq>
        <priority>1.0</priority>
    </url>
    <url>
        <loc>/login</loc>
        <lastmod>2017-11-20</lastmod>
        <changefreq>monthly</changefreq>
        <priority>0.7</priority>
    </url>

    <?php foreach ($articles as $article): ?>
        <url>
            <loc>/<?= $article['path'] ?></loc>
            <lastmod><?= date('Y-m-d', strtotime($article['created'])) ?></lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.5</priority>
        </url>
    <?php endforeach ?>
</urlset>
