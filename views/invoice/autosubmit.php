<?php
/**
 * Last modified: 18.07.03 05:38:12
 * Hash: 7c34c2c3541d3a0c1a6e8c5df89a962648b46d14
 */

use app\helpers\Html;

?>

<div class="autosubmit section">
    <?= Html::tag('h1', Yii::t('app', 'Wait please...')) ?>
    <form action="<?= $url ?>" method="POST">
        <div class="fields">
            <?php
            foreach ($data as $name => $value) {
                echo Html::hiddenInput($name, $value);
            }
            ?>
        </div>
    </form>
</div>
