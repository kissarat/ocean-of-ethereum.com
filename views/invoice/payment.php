<?php

/**
 * Last modified: 18.11.21 00:26:26
 * Hash: 68358d641b32ccae1ada7c6b5380113469f96797
 */

use yii\bootstrap\ActiveForm;


/* @var $this \yii\web\View */
/* @var $model \app\models\form\Withdraw */
?>


<div class="transfer payment">
    <?php $form = ActiveForm::begin() ?>
    <?= $form->field($model, 'amount')->textInput() ?>
    <?= $form->field($model, 'system')->dropDownList(\app\models\form\Invoice::getPaymentSystems()) ?>

    <button type="submit" class="btn btn-success">
        <?= Yii::t('app', 'Pay') ?>
    </button>
    <?php ActiveForm::end() ?>
    <div class="alert alert-info innform">
        
        Если Вы желаете оплатиться криптовалютой, пишите в Skype: <a href="skype:planet-of-dream.com"><i class="fa fa-skype" aria-hidden="true" style="font-size: 24px;"></i></a> 

        <br>  или в Telegram:  <a href="https://t.me/InfoOcean"><i class="fa fa-telegram" aria-hidden="true" style="font-size: 24px;"></i></a>  


</div>
</div>
