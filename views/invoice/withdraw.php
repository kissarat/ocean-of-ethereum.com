<?php

use yii\bootstrap\ActiveForm;
use app\models\User;


/* @var $this \yii\web\View */
/* @var $model \app\models\form\Withdraw */
?>


<div class="transfer withdraw">
    <?php $form = ActiveForm::begin() ?>
    <?= $form->field($model, 'amount')->input('number') ?>
    <?= $form->field($model, 'wallet')->textInput(['pattern' => '^U\d{7,8}$']) ?>
    <?= $form->field($model, 'pin')->passwordInput(['maxlength' => User::LENGTH_PIN]) ?>
    <?= $form->field($model, 'text')->textarea() ?>

    <button type="submit" class="btn btn-success">
        <?= Yii::t('app', 'Create') ?>
    </button>
    <?php ActiveForm::end() ?>
</div>
