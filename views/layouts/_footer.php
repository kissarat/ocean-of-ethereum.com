<?php
/**
 * Last modified: 18.11.21 00:08:47
 * Hash: 7fceebf596a1127d9321c01a12b88c796bdb6a7d
 */

use app\helpers\Html;

?>
<!-- footer -->
<?= $this->endBody() ?>
<!--<script src="/js/wasm_exec.js"></script>-->
<?php
if (YII_DEBUG) {
    echo Html::tag('script', '', ['src' => '/lib/moment-with-locales.js']);
    foreach (['track.js', 'app.js', 'main.js'] as $filename) {
        echo Html::tag('script', '', ['src' => '/js/debug/' . $filename]);
    }
}
?>
<div id="developer" style="display: none">
    <a href="https://kissarat.github.io/">Developed by Taras Labiak</a>
</div>
<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'YrNwc85Snw';var d=document;var w=window;function l(){var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();
</script>
<!-- {/literal} END JIVOSITE CODE -->
</body>
</html>
<?= $this->endPage() ?>
