<?php
/**
 * Last modified: 18.09.15 07:11:23
 * Hash: e9edda138cfb5214742308e7808ff53d46b6e8ba
 */

use app\assets\MinifiedAsset;
use app\helpers\Html;

MinifiedAsset::register($this);

$bodyClass = Yii::$app->user->getIsGuest() ? 'outdoor' : 'indoor';
if (!Yii::$app->user->getIsGuest() && Yii::$app->user->identity->admin) {
    $bodyClass .= ' admin';
}
$this->beginPage()
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
   <!-- <title><?= empty($this->title) ? Yii::$app->name : Yii::$app->name . ' - ' . $this->title ?></title> -->
    <title>Info Ocean</title>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link rel="shortcut icon" href="https://info0cean.com/wp-content/uploads/2018/11/favicon.jpg" type="image/x-icon">

<script src="https://use.fontawesome.com/46afd5a11b.js"></script>

    <?= Html::csrfMetaTags() ?>
    <?= Html::script(Html::context(['translation' => true])) ?>
    <?= $this->head() ?>
</head>
<body class="<?= $bodyClass ?>" style="display: none">
<?= $this->beginBody() ?>
<div id="template">
    <?php require_once '_widgets.php' ?>
</div>
<!-- main -->
