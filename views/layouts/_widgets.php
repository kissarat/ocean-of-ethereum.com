<?php
/**
 * Last modified: 18.07.03 05:38:12
 * Hash: 6a48d73c76bef555d074c642febfe1c0c6b6b37a
 */
?>
<template class="transfer-refill">
    <h1><?= Yii::t('app', 'Refill') ?></h1>
    <form>
        <div class="fields">
            <div v-if="address" class="form-group" id="field-address">
                <label><?= Yii::t('app', 'Address') ?></label>
                <input class="form-control" name="address" v-model="address" readonly/>
            </div>
            <button v-else type="button" @click="getAddress()">
                <?= Yii::t('app', 'Get Ethereum address') ?>
            </button>
        </div>
        <div id="address-qr-code"></div>
    </form>
</template>
