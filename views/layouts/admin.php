<?php
/**
 * Last modified: 18.07.03 05:38:12
 * Hash: eb5e70d97c369049fab71fc0f6913fdd1bb781ce
 */

use app\assets\AdminAsset;
use app\helpers\Html;

$isGuest = Yii::$app->user->getIsGuest();
if (!$isGuest && Yii::$app->user->identity->admin) {
    AdminAsset::register($this);
}

$this->beginPage()
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <?= Html::script(Html::context()) ?>
    <?= $this->head() ?>
    <?php if ($isGuest): ?>
        <script>
          location.pathname = '/unauthorized'
        </script>
    <?php endif ?>
</head>
<body class="<?= $isGuest ? 'indoor' : 'outdoor' ?>">
<?= $this->beginBody() ?>
<div id="template">
    <?php require_once '_widgets.php' ?>
</div>
<div id="app">
</div>
<?= $this->endBody() ?>
</body>
</html>
<?= $this->endPage() ?>
