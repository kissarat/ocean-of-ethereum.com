<?php
/**
 * Last modified: 18.07.20 07:21:37
 * Hash: cae4b76f0b9ba81b8342ccbf638614ee1bc182ff
 */

require_once '_header.php';

use app\widgets\Alert;

?>
    <div class="container layout-article">
        <div class="row">
            <div id="app">
                <?= Alert::widget() ?>
                <main>
                    <?= $content ?>
                </main>
            </div>
        </div>
    </div>
<?php
require_once '_footer.php';
