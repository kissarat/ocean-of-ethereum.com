<?php
/**
 * Last modified: 18.07.20 07:13:30
 * Hash: 1ff7f32f4d9866b1ebdd6e570a03ed17d18e9327
 */

use app\assets\EmbedAsset;
use app\helpers\Html;

EmbedAsset::register($this);
$isGuest = Yii::$app->user->getIsGuest();

$this->beginPage()
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <?= Html::script(Html::context()) ?>
    <?= $this->head() ?>
</head>
<body class="<?= $isGuest ? 'indoor' : 'outdoor' ?>" id="olympus">
<?= $this->beginBody() ?>
<div id="app" class="layout-embed">
    <?= $content ?>
</div>
<?= $this->endBody() ?>
</body>
</html>
<?= $this->endPage() ?>
