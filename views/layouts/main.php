<?php
/**
 * Last modified: 19.01.16 09:58:38
 * Hash: 81076e755cac3e35c6a9050cb12ca45d2507e38f
 */

use app\helpers\Html;
use app\models\Transfer;
use app\models\User;
use app\widgets\Alert;
use yii\bootstrap\Nav;

require_once '_header.php';
/**
 * @var User $user
 */

/**
 * @var User $user
 */
$user = Yii::$app->user->identity;

$offersLabel = Yii::t('app', 'Reinvestments <span data-number="{number}">{number}</span>', [
    'number' => $user->getOffersCount()]);
$swapsLabel = Yii::t('app', 'Swaps <span data-number="{number}">{number}</span>', [
    'number' => $user->getSwapsCount()]);
$claimsLabel = Yii::t('app', 'Change sponsor <span data-number="{number}">{number}</span>', [
    'number' => $user->getClaimsCount()]);
$schoolLabel = Yii::t('app', 'Info-school <span data-number="{number}">{number}</span>', [
    'number' => $user->getUnseenSchools()]);

$items = [
//    [
//        'label' => Yii::t('app', 'Main page'),
//        'url' => Yii::$app->params['referralLinkOrigin'] . '/?utm_source=office',
//        'linkOptions' => ['target' => '_black']
//    ],
    [
        'label' => Yii::t('app', 'Profile'),
        'url' => ['user/profile', 'nick' => $user->nick]
    ],
    ['label' => Yii::t('app', 'Team'), 'url' => ['structure/index', 'nick' => $user->nick]],
    ['label' => Yii::t('app', 'Matrices'), 'url' => ['program/index', 'nick' => $user->nick]],
    ['label' => Yii::t('app', 'History'), 'url' => ['transfer/index', 'nick' => $user->nick]],
    ['label' => $offersLabel, 'encode' => false, 'url' => ['offer/index', 'nick' => $user->nick]],
    ['label' => $swapsLabel, 'encode' => false, 'url' => ['swap/index', 'nick' => $user->nick]],
    ['label' => $claimsLabel, 'encode' => false, 'url' => ['claim/index', 'nick' => $user->nick]],
    ['label' => Yii::t('app', 'Marketing'),
        'url' => Yii::$app->params['referralLinkOrigin'] . '/marketing?utm_source=office',
        'linkOptions' => ['target' => '_blank']],
    ['label' => Yii::t('app', 'Info-products'),
        'url' => Yii::$app->params['referralLinkOrigin'] . '/info-produkty/?utm_source=office',
        'linkOptions' => ['target' => '_blank']],
    ['label' => $schoolLabel, 'encode' => false, 'url' => ['activity/index', 'nick' => $user->nick], 'nick' => $user->nick],
    ['label' => Yii::t('app', 'Banners'), 'url' => ['home/banner']],
    ['label' => Yii::t('app', 'Settings'), 'url' => ['settings/profile', 'nick' => $user->nick]],
    ['label' => Yii::t('app', 'Logout'), 'url' => ['user/logout', 'nick' => $user->nick]],
];

$balance = User::getBalance($user->id, Transfer::BASE_CURRENCY)
?>
    <!--    <script src="/clock.js"></script>-->
    <div id="app" class="container-fluid layout-main">
        <div class="row">
            <nav class="navbar col-lg-2 col-md-3 col-sm-12">
                <div class="navbar-header">
                    <button type="button"
                            class="navbar-toggle collapsed"
                            data-toggle="collapse"
                            data-target="#menu"
                            aria-expanded="false">
                        <span class="sr-only"><?= Yii::t('app', 'Open menu') ?></span>
                    </button>
                </div>
                <a class="logo" href="//info0cean.com/" target="_blank">
                    <br>
                    <br>
                    <img src="/img/logo.png" alt="Info Ocean" style="margin: 24px 0; max-width: 100%">
                    <br>
                    <br>
                </a>
                <div class="collapse navbar-collapse" id="menu">
                    <div class="nav navbar-nav">
                        <div class="user-card">
                            <div class="info">
                                <?= Html::avatar($user->avatar) ?>
                                <div class="name">
                                    <?= $user->getName() ?>
                                    <span><?= Yii::t('app', 'Welcome') ?></span>
                                </div>
                            </div>
                            <span><q>Info Ocean!</q></span>
                        </div>
                        <?= Nav::widget([
                            'items' => $items,
                            'options' => ['class' => '']
                        ]) ?>
                    </div>
                </div>
            </nav>

            <nav class="menu-bg col-lg-2 col-md-3 hidden-sm hidden-xs"></nav>

            <div class="col-md-9 col-lg-10 col-sm-12 col-xs-12">
                <div class="row header-menu">
                    <div class="col-md-2 col-lg-2">
                        <div class="balance">
                            <i class="material-icons">account_balance_wallet</i>
                            <?= Html::tag('span', $balance, ['class' => 'money usd']) ?>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                        <?= Html::textInput('referral',
                            Yii::$app->params['referralLinkOrigin'] . '/~' . $user->nick,
                            ['class' => 'referral']) ?>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                        <div class="payments">
                            <?= Html::a(Yii::t('app', 'Pay'), [
                                'invoice/payment', 'nick' => $user->nick], ['class' => 'pay-button button']) ?>
                            <?= Html::a(Yii::t('app', 'Internal transfer'), [
                                'transfer/internal', 'nick' => $user->nick], ['class' => 'pay-button button']) ?>
                            <?= Html::a(Yii::t('app', 'Withdraw'), [
                                'invoice/withdraw', 'nick' => $user->nick], ['class' => 'pay-button button']) ?>
                        </div>
                    </div>
                    <div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
                        <div class="currencies">
                            <?php
                            //                            foreach (Transfer::getCurrencies() as $currency) {
                            //                                echo Html::a($currency, ['transfer/currency', 'currency' => $currency, 'back' => $_SERVER['REQUEST_URI']],
                            //                                    ['class' => 'currency ' . (Yii::$app->request->getCurrency() === $currency ? 'active' : '')]);
                            //                            }
                            ?>
                        </div>
                        <div class="lang">

                            <?php foreach (['RU' => 'ru-RU', 'EN' => 'en-US'] as $lang => $code) {
                                echo Html::tag('span', $lang, [
                                    'id' => $code,
                                    'class' => Yii::$app->language === $code ? 'active' : ''
                                ]);
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9 col-lg-10 col-sm-12 col-xs-12">
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>

            <div class="col-md-12 footer col-lg-12 col-sm-12 col-xs-12">
                <footer>
                    <p>
                        &copy;
                        <a href="https://info0cean.com/" title="Info Ocean">Info Ocean</a>
                        <?= date('Y') ?>.
                        <?= Yii::t('app', 'All Right Reserved') ?>
                    </p>
                    <div class="wpb_wrapper">
                        <a class="vc_icon_element-link" href="https://vk.com/info0cean" title="" target=" _blank"><i
                                    class="fa fa-vk" aria-hidden="true"></i></a>
                        <a class="vc_icon_element-link" href="https://www.facebook.com/groups/info0cean/" title=""
                           target=" _blank"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                        <a class="vc_icon_element-link" href="https://www.ok.ru/group/55608518508559" title=""
                           target=" _blank"><i class="fa fa-odnoklassniki-square" aria-hidden="true"></i></a>
                        <a class="vc_icon_element-link"
                           href="https://www.youtube.com/channel/UC6gT1CzPR8f583pwgt3S2-g/featured?view_as=subscriber"
                           title="" target=" _blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
                        <a class="vc_icon_element-link" href="https://telegram.me/info0cean" title=""
                           target=" _blank"><i class="fa fa-telegram" aria-hidden="true"></i></a>
                        <a class="vc_icon_element-link" href="https://join.skype.com/gPXbbJOD7A5i" title=""
                           target=" _blank"><i class="fa fa-skype" aria-hidden="true"></i></a>
                    </div>

                    <?php
                    if (YII_TEST) {
                        echo Html::a(Yii::t('app', 'Test'), ['generate/user', 'nick' => $user->nick], ['style' => 'color: red']);
                    }
                    ?>
                </footer>
            </div> <!--end .footer -->
        </div> <!-- end row -->
    </div> <!-- end container fluid -->

    <style>
        @keyframes anim {
            from {
                opacity: 1;
            }

            to {
                opacity: 0.2;
            }
        }

        .nav li a span[data-number] {
            color: #000000;
            border-color: black;
            animation-name: anim;
            animation-duration: .8s;
            animation-iteration-count: infinite;
            background: white;
        }

        .matrix.whale {
            background-image: url(/img/05.jpg);
            background-size: cover;
        }

        .node.matrix {
            background-size: cover !important;
        }

        table table {
            background-color: #ececec;
            border: 0 !important;
        }

        table table table {
            background-color: #d8d8d8;
            border: 0 !important;
        }

        tr:nth-child(2n+1) {
            background: #00caff1a;
        }

        footer {
            display: flex;
        }

        .wpb_wrapper {
            margin-left: 50px;
        }

        .wpb_wrapper a {
            color: #fff;
            font-size: 23px;
        }
    </style>
    <!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(52135423, "init", {
        id:52135423,
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/52135423" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<?php
require_once '_footer.php';