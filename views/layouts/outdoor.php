<?php
/**
 * Last modified: 18.07.28 06:22:54
 * Hash: a64d0f35f66384e7b55a85b60175bc5f623c3e03
 */

require_once '_header.php';

use app\widgets\Alert;

?>

    <div class="container layout-outdoor">
        <div class="row">
            <div class="col-lg-6 сol-lg-offset-3 col-md-6 col-md-offset-3">
                <div id="app">
                    <div id="main">
                        <?= Alert::widget() ?>
                        <main>
                            <div class="header-form">
                                <div class="logo">
                                    <img src="/img/logo-effect.png" alt="Ocean of Ethereum">
                                </div>
                                <h1><?= $this->title ?></h1>
                            </div>
                            <?= $content ?>
                        </main>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php
require_once '_footer.php';
