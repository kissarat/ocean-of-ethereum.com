<?php

/* @var $offer \app\models\Offer */
/* @var $nodes array */
/* @var $buy bool */
/* @var $root \app\models\Node */
/* @var $this \yii\web\View */
/* @var $program \app\models\Program */

/* @var $status int */

use app\helpers\Html;
use app\models\Swap;
use app\models\User;

/** @var User $user */
$user = Yii::$app->user->identity;

$availableLevel = -1;
$class = 'node matrix ';
$class .= $buy ? 'buy ' : 'view ';
$class .= strtolower($program->name)
?>

<div class="<?= $class ?>">
    <h1><?= $program->name ?></h1>
    <?php
    for ($i = 0; $i < $program->level; $i++):?>
        <div data-level="<?= $i ?>" class="<?= $availableLevel < 0 ? 'available' : 'unavailable' ?>">
            <?php for ($j = 2 ** $i - 1; $j < 2 ** ($i + 1) - 1; $j++) {
                if (isset($nodes[$j])) {
                    $n = $nodes[$j];
                    $my = $n['user'] === $user->id;
                    $class = 'node ' . ($my ? 'my' : 'other');
                    if ($root->id === (int)$n['qualification']) {
                        $class .= ' qualification';
                    }
                    ?>
                    <div data-number="<?= $j ?>" data-id="<?= $n['id'] ?>" class="<?= $class ?>">
                        <div class="left-matrix">
                            <?= Html::avatar($n['avatar']) ?>
                        </div>

                        <div class="center-matrix">
                            <?php
                            if (!empty($n['surname'])) {
                                echo Html::tag('div', $n['surname'] . ' ' . $n['forename'], ['class' => 'name']);
                            }
                            ?>

                            <div class="nick">
                                <strong><?= Yii::t('app', 'Login') ?></strong>:
                                <?= Html::tag('span', $n['nick']) ?>
                            </div>

                            <div class="skype-matrix">
                                <strong><?= Yii::t('app', 'Skype') ?></strong>:
                                <?= $n['skype'] ? Html::tag('span', $n['skype']) : '' ?>
                            </div>

                            <div class="bottom-matrix">
                                <?php
                                /** @noinspection PhpUnhandledExceptionInspection */
                                if ($my && $i > 0 && Swap::can($user, $program->id)) {
                                    echo Html::a(Yii::t('app', 'Change'), ['swap/create', 'from' => $n['id']]);
                                }
                                ?>
                            </div>
                        </div>
                    </div>

                    <?php
                } else {
                    if ($availableLevel < 0) {
                        $availableLevel = $i;
                    }
                    $top = (int)(($j - 1) / 2);
                    if ($buy && $i === $availableLevel && !empty($nodes[$top])) {
                        echo Html::a('', ['node/buy', 'id' => $nodes[$top]['id'], 'number' => $j, 'offer' => $offer], [
                            'class' => 'empty',
                            'data-number' => $j
                        ]);
                    } else {
                        echo Html::tag('div', '', [
                            'class' => 'empty',
                            'data-number' => $j
                        ]);
                    }
                }
            }
            ?>
        </div>
    <?php endfor ?>

    <div id="matrix-number-choice">
        <!-- matrix-number-choice -->
    </div>
</div>
