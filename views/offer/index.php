<?php
/**
 * Last modified: 18.10.06 15:25:10
 * Hash: 74399eea8f9d109190fc01afa46b797bd3768287
 *
 * @var $this \yii\web\View
 * @var $provider \yii\data\ActiveDataProvider
 */

use app\helpers\Html;
use app\models\Offer;
use yii\grid\GridView;

?>

<div class="offer index">
    <h1><?= Yii::t('app', 'Reinvestments') ?></h1>
    <?= /** @noinspection PhpUnhandledExceptionInspection */
    GridView::widget([
        'dataProvider' => $provider,
        'columns' => [
            [
                'attribute' => 'program',
                'value' => function (Offer $model) {
                    return $model->programObject->name;
                }
            ],
            [
                'attribute' => 'created',
                'contentOptions' => function (Offer $model) {
                    return ['data-time' => strtotime($model->created)];
                }
            ],
            [
                'attribute' => 'used',
                'enableSorting' => false,
                'format' => 'html',
                'label' => Yii::t('app', 'Matrix'),
                'value' => function (Offer $model) {
                    if ($model->used > 0) {
                        return Html::a(Yii::t('app', 'View'), ['node/matrix', 'id' => $model->used]);
                    }
//                    $message = 'Put into matrix';
                    $program = $model->programObject;
                    if ($program->id > $model->originObject->program) {
                        $message = $program->finish ? 'Going to Lagoon' : 'New program';
                    }
                    else {
                        $message = 'Reinvest';
                    }
                    return Html::a(Yii::t('app', $message), ['program/open',
                        'id' => $model->program, 'offer' => $model->id
                    ]);
                }
            ],
            [
                'attribute' => 'used',
                'enableSorting' => false,
                'contentOptions' => function (Offer $model) {
                    return $model->used > 0 ? ['data-time' => strtotime($model->time)] : [];
                },
                'value' => function (Offer $model) {
                    return $model->used > 0 ? $model->time : '-';
                }
            ]
        ]
    ]) ?>
</div>
