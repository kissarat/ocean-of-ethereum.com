<?php
/**
 * Last modified: 18.08.03 12:50:22
 * Hash: 3e09365359f1efd1486c81d355e5ae8a9cd623f8
 */

use \yarcode\payeer\RedirectForm;
use \yarcode\payeer\Merchant;

?>

<div class="interkassa pay">
    <span><?= Yii::t('app', 'Please wait...') ?></span>
    <?= RedirectForm::widget([
        'merchant' => Yii::$app->get('payeer'),
        'invoiceId' => $guid,
        'amount' => $amount,
        'description' => $description,
        'currency' => Merchant::CURRENCY_USD,
        'class' => 'autosubmit'
    ]);
    ?>
</div>
