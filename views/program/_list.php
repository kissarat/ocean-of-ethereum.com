<?php
/**
 * Last modified: 18.11.20 23:49:27
 * Hash: 23b0d8f396c41e939bef2d3b008c84d636d6c8ac
 *
 * @var $this \yii\web\View
 * @var $programs array
 */

use app\helpers\Html;

$user = Yii::$app->user->identity;

foreach ($programs as $program) {
    $name = str_replace(array(' Stream', ' Lagoon'), '', $program['name']);
    $lower = strtolower($name);
    ?>
    <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
        <div class="program <?= $lower ?>">
            <div class="image">
                <?= Html::img("/img/$lower.png", ['alt' => "Matrix $name"]) ?>
            </div>
            <div class="program-inform">
                <h3 class="name"><?= $name ?></h3>
                <div>
                    <span class="price money usd"><?= $program['price'] ?></span>
                </div>
                <div class="control">
                    <?php
                    if ($program['node'] > 0) {
                        echo Html::a(
                            Yii::t('app', 'Info-products'),
                            ['course/index', 'program_id' => 0 === $program['id'] % 2 ? $program['id'] - 1 : $program['id']],
                            ['class' => 'courses']
                        );
                        if (false === $program['active']) {
                            echo Html::tag('div', Yii::t('app', 'Matrix is full'), [
                                'class' => 'full'
                            ]);
                        } else {
                            echo Html::a(Yii::t('app', 'View'), ['node/matrix',
                                'nick' => $user->nick,
                                'id' => $program['node']
                            ]);
                        }
                    } else {
                        echo Html::a(Yii::t('app', 'Open'), [
                            'program/open',
                            'nick' => $user->nick,
                            'id' => $program['id']
                        ], [
                                'target' => '_blank'
                        ]);
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?php
}
