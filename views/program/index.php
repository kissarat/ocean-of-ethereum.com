<?php
/**
 * Last modified: 18.07.26 20:47:43
 * Hash: 06a701a7ce9777e4dd45d32865e6bf96fa81d24a
 */

/* @var $models array */
/* @var $this \yii\web\View */

?>
<div class="row">
    <div class="programs index program-list">
		<?= Yii::$app->view->render('_list', ['programs' => $models]) ?>
    </div>
</div>
