<?php
/**
 * Last modified: 18.10.06 15:30:47
 * Hash: 0182c8e99c218d68d92a8dd798bb097bad742758
 */
/* @var $offer int|null */
/* @var $this \yii\web\View */
/* @var $model \app\models\form\Username */
/** @var \app\models\Program $program */

/* @var $id int */

use app\helpers\Html;
use yii\widgets\ActiveForm;

?>
<article class="program sponsor">
    <p>
        <?= Yii::t('app', 'Enter the login of the member of the matrix you want to stand in or click the "Random Matrix" button', [
            'name' => $program->name
        ]) ?>
    </p>
    <?php $form = ActiveForm::begin() ?>
    <?= $form->field($model, 'nick')
        ->label(false /* Yii::t('app', 'Choose sponsor') */) ?>
    <?= Html::submitButton(Yii::t('app', 'Open')) ?>

    <?= Html::a(Yii::t('app', 'Random matrix'), ['node/random',
        'nick' => Yii::$app->user->identity->nick,
        'program' => $id,
        'buy' => !($offer > 0),
        'offer' => $offer
    ], [
        'class' => 'button'
    ]) ?>
    <?php ActiveForm::end() ?>
</article>
