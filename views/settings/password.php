<?php
/**
 * Last modified: 18.07.28 04:18:45
 * Hash: 4c3ac97766750743e24d412191201277bc3861f8
 */
/* @var $this \yii\web\View */
/* @var $model \app\models\form\Password */

use app\helpers\Html;
use app\models\User;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Password');
?>

<div class="settings password">
    <h2><?= Yii::t('app', 'Change password') ?></h2>
    <?php $form = ActiveForm::begin(); ?>

    <div class="fields">
        <?= $form->field($model, 'current')
            ->passwordInput(['maxlength' => User::LENGTH_PASSWORD]) ?>
        <?= $form->field($model, 'new')
            ->passwordInput(['maxlength' => User::LENGTH_PASSWORD]) ?>
        <?= $form->field($model, 'repeat')
            ->passwordInput(['maxlength' => User::LENGTH_PASSWORD]) ?>
        <?= $form->field($model, 'ip')->hiddenInput()->label(false) ?>
    </div>

    <div class="control">
        <?= Html::submitButton(Yii::t('app', 'Change password')) ?>
        <?= Html::a(Yii::t('app', 'Change profile'), [
            'settings/profile', 'nick' => Yii::$app->user->identity->nick]) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
