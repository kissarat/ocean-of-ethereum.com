<?php
/**
 * Last modified: 18.07.28 04:16:23
 * Hash: 2ba0c373acba3c55c3984c26b64d275c0e3437f1
 */

use app\models\User;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('app', 'Profile');
?>
<div class="row">
    <div class="settings profile">
        <h2 class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><?= Yii::t('app', 'Change profile settings') ?></h2>

        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        <div class="col-lg-3 col-md-7 col-sm-3 col-xs-12">
            <?= $form->field($model, 'forename')
                ->textInput(['maxlength' => User::LENGTH_NAME]) ?>
            <?= $form->field($model, 'surname')
                ->textInput(['maxlength' => User::LENGTH_NAME]) ?>
            <?= $form->field($model, 'email')
                ->textInput(['maxlength' => User::LENGTH_EMAIL]) ?>
            <?= $form->field($model, 'telegram')
                ->textInput(['maxlength' => User::LENGTH_TELEGRAM]) ?>
            <?= $form->field($model, 'skype')
                ->textInput(['maxlength' => User::LENGTH_SKYPE]) ?>
            <?= $form->field($model, 'avatar_file')
                ->fileInput(['accept' => 'image/*']) ?>
            <div class="controls">
                <?= Html::submitButton(Yii::t('app', 'Save')) ?>
                <?= Html::a(Yii::t('app', 'Change password'), [
                        'settings/password', 'nick' => Yii::$app->user->identity->nick]) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
