<?php
/**
 * Last modified: 18.08.22 22:25:37
 * Hash: 57db8cc5007cdb4f3d5a8f540c6ea171bc15863d
 */

/** @var int $level */

use app\helpers\Html;
use app\models\Program;
use app\models\Referral;

function configGrid($level, $config)
{
    $columns = [
        [
            'label' => Yii::t('app', 'Level'),
            'format' => 'html',
            'contentOptions' => ['class' => 'level'],
            'value' => function () use ($level) {
                $numbers = [
                    1 => 'I',
                    2 => 'II',
                    3 => 'III',
                    4 => 'IV',
                    5 => 'V',
                    6 => 'VI',
                    7 => 'VII',
                    8 => 'VIII',
                    9 => 'IX',
                    10 => 'X',
                    11 => 'XI',
                    12 => 'XII',
                    13 => 'XIII',
                    14 => 'XIV',
                    15 => 'XV',
                    16 => 'XVI',
                    17 => 'XVII',
                    18 => 'XVIII',
                    19 => 'XIX',
                    20 => 'XX',
                    21 => 'XXI',
                    22 => 'XXII',
                    23 => 'XXIII',
                    24 => 'XXIV',
                    25 => 'XXV',
                    26 => 'XXVI',
                    27 => 'XXVII',
                    28 => 'XXVIII',
                    29 => 'XXIX',
                    30 => 'XXX',
                ];
                $n = empty($numbers[$level]) ? 'Unavailable' : $numbers[$level];
                return "<span>$n</span>";
            }
        ],
        [
            'attribute' => 'surname',
            'label' => Yii::t('app', 'Name'),
            'contentOptions' => ['class' => 'name'],
            'value' => function (Referral $model) {
                return $model->getName(true);
            }
        ],
        ['attribute' => 'nick', 'contentOptions' => ['class' => 'nick']],
        ['attribute' => 'skype', 'contentOptions' => ['class' => 'skype']],
        ['attribute' => 'telegram', 'contentOptions' => ['class' => 'telegram']],
        [
            'attribute' => 'children',
            'contentOptions' => function (Referral $model) {
                return [
                    'class' => 'children',
                    'data-children' => $model->children
                ];
            }
        ]
    ];

    foreach (Program::getStartPrograms() as $program) {
        $name = str_replace(' Stream', '', $program->name);
        $columns[] = [
//            'attribute' => 'id',
            'label' => $name,
            'format' => 'html',
            'contentOptions' => [
                'class' => 'program ' . strtolower($name)
            ],
            'value' => function (Referral $model) use ($program) {
                $start = $model->nodes[$program->id] ?? 0;
                $finish = $model->nodes[$program->id + 1] ?? 0;
                if (!$finish && !$start) {
                    return '<span class="empty">-</span>';
                }
                return Html::a('view', ['node/matrix', 'id' => $finish ?: $start]);
            }
        ];
    }

    return array_merge_recursive([
        'options' => [
            'data-level' => $level
        ],
        'rowOptions' => function (Referral $model) {
            return [
                'class' => $model->children > 0 ? 'has-children' : '',
                'data-id' => $model->id,
                'data-nick' => $model->nick
            ];
        },
        'columns' => $columns
    ],
        $config);
}
