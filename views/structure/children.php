<?php
/**
 * Last modified: 18.07.26 12:45:15
 * Hash: 667d3cf50d1185aad557d43f640404610ce21941
 */

/** @var \yii\db\ActiveQuery $provider */

use yii\grid\GridView;

require '_grid.php';

?>
<div class="structure children">
    <?= GridView::widget(configGrid($level, [
        'dataProvider' => $provider,
        'showHeader' => false,
        'layout' => '{items}'
    ]));
    ?>
</div>
