<?php
/**
 * Last modified: 18.07.26 12:30:07
 * Hash: 2980b3ca0dbaeeea0bfaeaecebdb6c3ed3eaf26e
 */

/** @var \yii\db\ActiveQuery $provider */

use yii\grid\GridView;

require '_grid.php';

?>
<div class="structure index">
    <h1><?= Yii::t('app', 'Structure') ?></h1>
    <?= GridView::widget(configGrid($level, [
        'dataProvider' => $provider,
        'layout' => '{errors}{summary}{pager}{items}'
    ])) ?>
</div>
