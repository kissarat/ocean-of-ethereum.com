<?php
/**
 * Last modified: 18.10.07 06:55:01
 * Hash: 68ce81016c8361bdcf1f18817395e1f86e9a7601
 */

use app\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var \app\models\form\Username $model
 */
?>
<div class="swap create">
    <?php $form = ActiveForm::begin() ?>
    <?= $form->field($model, 'nick')
        ->label(Yii::t('app', 'Swap the cells with a user')) ?>
    <?= Html::submitButton(Yii::t('app', 'Swap ')) ?>
    <?php ActiveForm::end() ?>
</div>
