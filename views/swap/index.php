<?php
/**
 * Last modified: 18.09.15 04:35:54
 * Hash: 7f3326a391cacda7a78456a826937e2c579e3e21
 *
 * @var $provider \yii\data\ActiveDataProvider
 * @var $this \yii\web\View
 */

use app\helpers\Html;
use app\models\Swap;
use yii\grid\GridView;

?>
<div class="swap index">
    <h1><?= Yii::t('app', 'Swap') ?></h1>
    <?= /** @noinspection PhpUnhandledExceptionInspection */
    GridView::widget([
        'dataProvider' => $provider,
        'rowOptions' => function (Swap $model) {
            $class = null === $model->approved ? 'pending' : ($model->approved ? 'accepted' : 'rejected');
            return ['class' => 'status ' . $class];
        },
        'columns' => [
            [
                'attribute' => 'created',
                'contentOptions' => function (Swap $model) {
                    return ['data-time' => strtotime($model->created)];
                }
            ],
            [
                'attribute' => 'from_user',
                'enableSorting' => false,
//                'format' => 'html',
                'value' => function (Swap $model) {
                    return $model->fromUserObject->nick;
//                    return Html::a($model->fromObject->userObject->nick, ['node/matrix', 'id' => $model->from]);
                }
            ],
            [
                'attribute' => 'to_user',
                'enableSorting' => false,
//                'format' => 'html',
                'value' => function (Swap $model) {
                    return $model->toUserObject->nick;
//                    return Html::a($model->toObject->userObject->nick, ['node/matrix', 'id' => $model->to]);
                }
            ],
            [
                'attribute' => 'root',
                'enableSorting' => false,
                'format' => 'html',
                'label' => Yii::t('app', 'Program'),
                'value' => function (Swap $model) {
                    $name = $model->matrix->programObject->name;
                    return Html::tag('span', $name, ['class' => 'program ' . strtolower($name)]);
                }
            ],
            [
                'attribute' => 'approved',
                'enableSorting' => false,
                'format' => 'html',
                'value' => function (Swap $model) {
                    if (null !== $model->approved) {
                        return Html::tag('span', $model->approved
                            ? Yii::t('app', 'Approved', ['class' => 'completed approved'])
                            : Yii::t('app', 'Rejected', ['class' => 'completed rejected']));
                    }

                    if (!$model->approved && $model->to_user === Yii::$app->user->identity->id) {
                        return Html::a(Yii::t('app', 'Approve'), ['swap/approve',
                                'id' => $model->id, 'approve' => 1], ['class' => 'pending receiver approve'])
                            . ' ' .
                            Html::a(Yii::t('app', 'Reject'), ['swap/approve',
                                'id' => $model->id, 'approve' => 0], ['class' => 'pending receiver reject']);
                    }

                    return Html::tag('span', Yii::t('app', 'Pending'), ['class' => 'pending sender']);
                }
            ],
            [
                'label' => Yii::t('app', 'Actions'),
                'format' => 'html',
                'value' => function (Swap $model) {
                    if (Yii::$app->user->identity->id === $model->from_user && $model->getIsPending()) {
                        return Html::a(Yii::t('app', 'Remove'), ['swap/remove', 'id' => $model->id]);
                    }
                    return '';
                }
            ]
        ]
    ]) ?>
</div>
