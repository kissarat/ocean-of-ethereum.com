<?php
/**
 * Last modified: 19.01.29 02:29:20
 * Hash: 9037efe8589118dc3110935973581079ca6c3e8c
 */

use yii\grid\GridView;

$focus = empty($_GET['id']) ? null : +$_GET['id'];
$nick = Yii::$app->user->identity->nick;

/**
 * @var yii\data\ArrayDataProvider $provider
 * @var $this \yii\web\View
 */
?>
<router-view v-if="widgetFound"></router-view>

<div v-else class="transfer index">
    <h1><?= Yii::t('app', 'History') ?></h1>

    <?php
    /** @noinspection PhpUnhandledExceptionInspection */
    echo GridView::widget([
        'dataProvider' => $provider,
        'layout' => '{errors}{items}{summary}{pager}',
//        'tableOptions' => ['class' => 'striped bordered'],
        'emptyText' => 'Nothing found',
        'rowOptions' => function ($model) use ($focus) {
            $class = $focus === $model->id ? 'focus' : 'normal';
            return ['class' => "$class type-$model->type status-$model->status"];
        },
        'columns' => [
            'id',
            [
                'attribute' => 'created',
                'label' => Yii::t('app', 'Time'),
                'format' => 'datetime',
                'contentOptions' => function ($model) {
                    return ['data-time' => strtotime($model->created)];
                }
            ],
            [
                'attribute' => 'amount',
                'format' => 'html',
                'value' => function ($model) {
                    $sign = $model->to === Yii::$app->user->identity->id ? '+' : '-';
                    $currency = strtolower($model->currency);
                    return "<span class=\"sign\">$sign</span><span class=\"money $currency\">$model->amount</span>";
                }
            ],
            [
                'attribute' => 'text',
                'label' => Yii::t('app', 'Description'),
                'value' => function ($model) {
                    /** @var \app\models\User $user */
                    $user = Yii::$app->user->identity;
                    if ($model->text && 'internal' !== $model->type) {
                        if ($model->vars) {
                            return Yii::t('app', $model->text, $model->vars);
                        }
                        return Yii::t('app', $model->text);
                    }
                    switch ($model->type) {
                        case 'payment':
                            return Yii::t('app', 'Payment from {wallet}', ['wallet' => $model->wallet]);
                        case 'withdraw':
                            return Yii::t('app', 'Withdrawal to {wallet}', ['wallet' => $model->wallet]);
                        case 'support':
                            return Yii::t('app', 'Administrative');
                        case 'buy':
                            return Yii::t('app', 'Open {name}', $model->vars);
                        case 'bonus':
                            if (isset($model->vars['level'])) {
//                                if (!empty($model->vars['major'])) {
//                                    return Yii::t('app', 'Leader bonus from {from_nick} in {name} at {level}', $model->vars);
//                                }
                                return Yii::t('app', 'Bonus from {from_nick} in {name} at {level}', $model->vars);
                            }
                            return Yii::t('app', 'Bonus from {from_nick} in {name}', $model->vars);
                        case 'accrue':
                            if (isset($model->vars['program']) && $model->vars['program'] % 2 === 0) {
                                return Yii::t('app', 'Reward from {from_nick} in {name}', $model->vars);
                            }
                            return Yii::t('app', 'Accrue from {from_nick} in {name}', $model->vars);
                        case 'refund':
                            return Yii::t('app', '{name} refund', $model->vars);
                        case 'qualification':
                            return Yii::t('app', '{n} qualification in {name}', $model->vars);
                        case 'fee':
                            return Yii::t('app', 'Fee');
                        case 'internal':
                            $text = Yii::t('app',
                                $model->from === $user->id ? 'Transfer to {nick}' : 'Transfer from {nick}',
                                ['nick' => $model->from === $user->id ? $model->vars['to_nick'] : $model->vars['from_nick']]);
                            if ($model->text) {
                                $text .= ': ' . $model->text;
                            }
                            return $text;
                        default:
                            return $model->type;
                    }
                }
            ],
            [
                'attribute' => 'status',
                'label' => Yii::t('app', 'Status'),
                'value' => function ($model) {
                    switch ($model->status) {
                        case 'success':
                            return "\u{2713}";

                        case 'fail':
                            return "\u{274C}";

                        case 'canceled':
                            return Yii::t('app', 'Canceled');

                        case 'created':
                            return Yii::t('app', 'Created');
                        default:
                            return $model->status;
                    }
                }
            ],
        ]
    ]) ?>
</div>
