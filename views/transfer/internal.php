<?php

/**
 * Last modified: 18.10.06 04:20:02
 * Hash: 29b95e82ec64fecb6dd86f1fb82918f9cfe6d8aa
 */

use yii\widgets\ActiveForm;


/* @var $this \yii\web\View */
/* @var $model \app\models\form\Internal */
?>
<div class="transfer internal">
    <h1><?= Yii::t('app', 'Internal transfer') ?></h1>
    <?php $form = ActiveForm::begin() ?>
    <?= $form->field($model, 'nick') ?>
    <?= $form->field($model, 'amount')->input('number') ?>
    <?= $form->field($model, 'pin')->passwordInput(['maxlength' => \app\models\User::LENGTH_PIN]) ?>
    <?= $form->field($model, 'text')->textarea() ?>
    <button type="submit"><?= Yii::t('app', 'Send') ?></button>
    <?php ActiveForm::end() ?>
</div>
