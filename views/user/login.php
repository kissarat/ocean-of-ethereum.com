<?php
/**
 * Last modified: 18.09.22 16:02:09
 * Hash: 1fa5c210035bb80c12ca903a71b8ac61bfcf1acf
 */

use app\models\User;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\form\Login */
/* @var $form ActiveForm */

$this->title = Yii::t('app', 'Sign in');
?>

<div class="user login">
    <?php $form = ActiveForm::begin(); ?>
    <div class="fields">
        <?= $form->field($model, 'login')
            ->textInput(['maxlength' => User::LENGTH_EMAIL, 'placeholder' => Yii::t('app', 'Your Login')])
            ->label(false) ?>
        <?= $form->field($model, 'password')
            ->passwordInput(['maxlength' => User::LENGTH_PASSWORD, 'placeholder' => Yii::t('app', 'Your Password')])
            ->label(false) ?>
    </div>

    <div class="control">
        <?= Html::submitButton(Yii::t('app', 'Sign in')) ?>
        <?= Html::a(Yii::t('app', 'Signup'), ['user/signup']) ?>
        <?= Html::a(Yii::t('app', 'Forgot Password?'), ['user/request']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
