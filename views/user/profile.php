<?php
/**
 * Last modified: 18.11.20 23:53:29
 * Hash: c56312e58525503fa79105526a40f49938593f0c
 */

use app\helpers\Html;
use app\models\Transfer;

/* @var $program array */
/* @var $this \yii\web\View */
/* @var $model array */
/* @var $programs array */

$parent = $model['parent'];
$isMe = Yii::$app->user->getIsGuest() ? false : Yii::$app->user->identity->nick === $model['nick'];
?>
<div class="user profile">
    <div class="row">
        <ul class="informer clean">
            <li class="col-lg-3">
                <div class="icon purpure"></div>
                <div>
                    <span class="value money usd"><?=
                        $model['accrue'][Transfer::CURRENCY] +
                        (empty($model['refund']) ? 0 : $model['refund'][Transfer::CURRENCY])
                        ?></span>
                    <span class="name"><?= Yii::t('app', 'Money from the matrices') ?></span>
                </div>
            </li>
            <li class="col-lg-3">
                <div class="icon blue"></div>
                <div>
                    <span class="value money usd"><?= $model['bonus'][Transfer::CURRENCY] ?></span>
                    <span class="name"><?= Yii::t('app', 'Linear income') ?></span>
                </div>
            </li>
            <li class="col-lg-3">
                <div class="icon pink"></div>
                <div>
                    <span class="value"><?= $model['visit'] ?></span>
                    <span class="name"><?= Yii::t('app', 'Referral link transitions') ?></span>
                </div>
            </li>
            <li class="col-lg-3">
                <div class="icon green"></div>
                <div>
                    <span class="value"><?= $model['referral'] ?></span>
                    <span class="name"><?= Yii::t('app', 'The team') ?></span>
                </div>
            </li>
        </ul>
    </div>
    <div class="row program-list">
        <?php if ($parent): ?>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="widget cart-sponsor">
                    <h3 class="widget-title"><?= Yii::t('app', 'My sponsor') ?></h3>
                    <div class="widget-content">
                        <?= !empty($parent->forename) && !empty($parent->surname)
                            ? Html::tag('p', $parent->forename . ' ' . $parent->surname, ['class' => 'name'])
                            : Yii::t('app', 'Sponsor did not give name and surname') ?>
                        <ul>
                            <li>
                                <i class="material-icons">supervised_user_circle</i>
                                <span><strong>Login:</strong> <?= $parent->nick ?></span>
                            </li>
                            <?php
                            if (!empty($parent->skype)) : ?>
                                <li>
                                    <i class="material-icons">perm_phone_msg</i>
                                    <span><strong>Skype:</strong> <?= $parent->skype ?></span>
                                </li>
                            <?php else :
                                echo Yii::t('app', 'Sponsor does not have Skype');
                            endif; ?>

                        </ul>
                    </div>
                </div>
            </div>
        <?php endif ?>
        <?= Yii::$app->view->render('@app/views/program/_list', [
            'programs' => $programs
        ]) ?>
    </div>
</div>
