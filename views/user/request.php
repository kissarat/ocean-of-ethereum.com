<?php

use app\models\User;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var $this yii\web\View
 * @var $message string
 * @var $model app\models\form\RequestPasswordReset
 * @var $form ActiveForm
 */

$this->title = Yii::t('app', 'Password Recovery');
?>

<div class="user request">
    <?php $form = ActiveForm::begin(); ?>
    <h4><?= Yii::t('app', 'Password Recovery Request') ?></h4>

    <div class="fields">
        <?= $form->field($model, 'login')->textInput(['maxlength' => User::LENGTH_EMAIL]) ?>
    </div>

    <div class="control">
        <?= Html::submitButton(Yii::t('app', 'Send Email')) ?>
        <?= Html::a(Yii::t('app', 'Signup'), ['user/signup']) ?>
        <?= Html::a(Yii::t('app', 'Sign in'), ['user/login']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
