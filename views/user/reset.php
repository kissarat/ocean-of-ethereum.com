<?php
/**
 * Last modified: 18.10.06 16:16:46
 * Hash: b7a5f6530c066315ee2823271de15bb6ebf26d12
 *
 * @var $this yii\web\View
 * @var $message string
 * @var $model app\models\form\RequestPasswordReset
 * @var $form ActiveForm
 * @var $token \app\models\Token
 */

use app\models\User;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Password Recovery');

if ($token): ?>
    <div class="user reset">
        <?php $form = ActiveForm::begin(); ?>

        <div class="fields">
            <?= $form->field($model, 'password')->passwordInput(['maxlength' => User::LENGTH_PASSWORD]) ?>
            <?= $form->field($model, 'repeat')->passwordInput(['maxlength' => User::LENGTH_PASSWORD]) ?>
        </div>

        <div class="control">
            <?= Html::submitButton(Yii::t('app', 'Save')) ?>
            <?= Html::a(Yii::t('app', 'Cancel'), ['user/login']) ?>
        </div>
        <?php ActiveForm::end() ?>
    </div>
<?php else: ?>
    <div class="user reset">
        <?php $form = ActiveForm::begin(); ?>
        <div class="control">
            <?= Html::a(Yii::t('app', 'Signup'), ['user/signup']) ?>
            <?= Html::a(Yii::t('app', 'Sign in'), ['user/login']) ?>
        </div>
        <?php ActiveForm::end() ?>
    </div>
<?php endif;
