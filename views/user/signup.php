<?php
/**
 * Last modified: 18.12.01 15:22:01
 * Hash: 2fa9440463ea5af6c5f21b23e2f08aedf6bd62c0
 */

use app\models\User;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('app', 'Signup');
?>

<div class="user signup">
    <?php if (Yii::$app->params['signup']): ?>
        <?php $form = ActiveForm::begin(['enableAjaxValidation' => true]); ?>
        <h4><?= Yii::t('app', 'Create an account') ?></h4>

        <div class="fields">
            <?= $form->field($model, 'nick')->textInput(['maxlength' => User::LENGTH_NICK]) ?>
            <?= $form->field($model, 'email')->textInput(['maxlength' => User::LENGTH_EMAIL]) ?>
            <?= $form->field($model, 'password')->passwordInput(['maxlength' => User::LENGTH_PASSWORD]) ?>
            <?= $form->field($model, 'repeat')->passwordInput(['maxlength' => User::LENGTH_PASSWORD]) ?>
            <?= $form->field($model, 'pin')->passwordInput([
                'maxlength' => User::LENGTH_PIN,
                'placeholder' => Yii::t('app', 'Save and do not pass it to anyone!')
            ]) ?>
            <?= $form->field($model, 'telegram')->textInput(['maxlength' => User::LENGTH_TELEGRAM]) ?>
            <?= $form->field($model, 'skype')->textInput(['maxlength' => User::LENGTH_SKYPE]) ?>
            <?= $form->field($model, 'sponsor')->textInput(['maxlength' => User::LENGTH_NICK]) ?>
            <div class="agreement">
                <?= $form->field($model, 'agree')->checkbox() ?>
                <?= Yii::t('app', '<a href="/agreement" target="_blank">Terms and Conditions</a>') ?>
            </div>
        </div>

        <div class="control">
            <?= Html::submitButton(Yii::t('app', 'Signup')) ?>
            <?= Html::a(Yii::t('app', 'Sign in'), ['user/login']) ?>
            <?= Html::a(Yii::t('app', 'Forgot Password?'), ['user/request']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    <?php else: ?>
        <div><?= Yii::t('app', 'Signup is closed') ?></div>
    <?php endif ?>
</div>
