<?php
/**
 * Last modified: 18.11.16 01:34:47
 * Hash: 5ca7d9af4346603b604604e82def2e977f661a10
 */

if (PHP_VERSION_ID < 70200) {
    die('PHP 7.2 is required');
}
foreach (['json', 'session', 'apcu', 'intl', 'mbstring', 'redis', 'pdo_pgsql'] as $name) {
    if (!extension_loaded($name)) {
        die($name . ' extension is not loaded');
    }
}

//define('HOUR', date('H'));
//if (HOUR >= 0 && HOUR < 8) {
//    die('Site is offline');
//}

//ini_set('xdebug.remote_host', '0.0.0.0');
//ini_set('xdebug.remote_port', 8000);

$config = require '../config/web.php';

//require_once '../vendor/yiisoft/yii2/BaseYii.php';
require '../vendor/yiisoft/yii2/Yii.php';
require '../vendor/autoload.php';

//if (YII_DEBUG) {
//    require '../config/debug.php';
//}

$config['language'] = empty($_COOKIE['locale']) ? 'ru-RU' : $_COOKIE['locale'];

$app = new \yii\web\Application($config);
$classConfig = require '../config/class.php';

/** @var array $classConfig */
foreach ($classConfig as $class => $config) {
    \Yii::$container->set($class, $config);
}

$app->run();
