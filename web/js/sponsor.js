/**
 * Last modified: 18.10.05 22:22:39
 * Hash: 889624e6d3f55a04d46e95c28fd87a77b6f3ddf8
 */

document.addEventListener('DOMContentLoaded', function () {
  const cookieParam = 'sponsor'
  const office = '//office.' + location.hostname
  var sponsor = /r=([\w_]+)/.exec(location.search)
  sponsor = sponsor ? sponsor[1] : null
  var cookieSponsor = new RegExp(cookieParam + '=([\\w_]+)').exec(document.cookie)
  cookieSponsor = cookieSponsor ? cookieSponsor[1] : null
  var isNew = !!sponsor
  if (cookieSponsor) {
    if (sponsor === cookieSponsor) {
      isNew = false
    }
    else if (!sponsor) {
      sponsor = cookieSponsor
    }
  }
  const callback = 'z' + Date.now().toString(36)
  if (sponsor) {
    document.cookie = cookieParam + '=' + sponsor + '; path=/; max-age=315360000'
    if (isNew) {
      const script = document.createElement('script')
      script.src = office + '/newbie/' + sponsor + '?callback=' + callback
      document.head.appendChild(script)
    }
    [].forEach.call(document.querySelectorAll('[href$="/signup"]'), function (a) {
      a.href = a.href.replace('/signup', '/~' + sponsor + '/signup')
    });
    [].forEach.call(document.querySelectorAll('[href$="/sign-in"]'), function (a) {
      a.href = a.href.replace('/sign-in', '/~' + sponsor + '/sign-in')
    });
  }

  window[callback] = function () {
    // console.log('New visit', a)
    delete window[callback]
  }
})
