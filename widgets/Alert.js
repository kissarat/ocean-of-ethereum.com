/**
 * Alert

var alerts = null

document.addEventListener('load', function () {
  alerts = new Vue({
    el: '.widget-alert',
    data: {
      alerts: []
    },
    methods: {
      add: function (type, message) {
        this.alerts.push({
          type: type,
          message: message
        })
        return this.alerts.length - 1
      },

      success: function (message) {
        return this.add('success', message)
      },

      error: function (message) {
        return this.add('error', message)
      },

      remove: function (i) {
        this.alerts.splice(i)
      },
      clear: function () {
        this.alerts.splice(0, this.alerts.length)
      }
    }
  })
})
 */
