<?php
/**
 * Last modified: 18.06.13 16:34:59
 * Hash: 3c82c7aba7330baf65d54a663ae7b28a53590ba4
 */

namespace app\widgets;


use app\helpers\Html;
use Yii;
use yii\base\Widget;

class Alert extends Widget
{
    public $types = [
        'error' => 'alert-danger',
        'info' => 'alert-info',
        'success' => 'alert-success',
        'warning' => 'alert-warning',
    ];


    public function run() : string
    {
        $block = Html::tag('div', '{{ a.message }}
                <button type="button" class="close" aria-label="Close" v-on:click="remove(i)">
                    <span aria-hidden="true">&times;</span></button>', [
            'v-for' => '(a, i) in app.features.alerts',
            'v-bind:class' => "'alert alert-' + ('error' === a.type ? 'danger' : a.type)",
            'role' => 'alert'
        ]);
        $alerts = [
            $block
        ];
        $flashes = Yii::$app->session->getAllFlashes(true);
        if (YII_DEBUG) {
            foreach ($this->types as $type => $class) {
                $v = Yii::$app->request->get($type);
                if (!empty($v)) {
                    $flashes[$type][] = strip_tags($type) . ' message';
                }
            }
        }
        foreach ($flashes as $type => $messages) {
            $type = $this->types[$type] ?? 'alert-info';
            if (!\is_array($messages)) {
                $messages = [$messages];
            }
            foreach ($messages as $message) {
                $message .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>';
                $alerts[] = Html::tag('div', $message, [
                    'class' => 'alert ' . $type,
                    'role' => 'alert'
                ]);
            }
        }
        $tag = [
            Html::script(file_get_contents(__DIR__ . '/Alert.js')),
            $block,
            Html::tag('div', implode("\n", $alerts), [
                'class' => 'widget-alert-list',
                'v-cloak' => true,
            ])
        ];
        return Html::tag('div', implode("\n", $tag), [
            'class' => 'widget-alert',
            'v-cloak' => true,
            'data-count' => \count($alerts)
        ]);
    }
}
